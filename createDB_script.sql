CREATE SCHEMA IF NOT EXISTS `beauty_salon`;
USE `beauty_salon`;

drop table if exists work_date;
drop table if exists evaluations;
drop table if exists appointment;
drop table if exists roles_has_services;
drop table if exists users;
drop table if exists roles;
drop table if exists services;
drop table if exists `schedule`;
drop table if exists worker_schedule;
drop table if exists days;
drop view if exists clients_view;
drop view if exists admins_view;
drop view if exists masters_view;

-- -----------------------------------------------------
-- Table `beauty_salon`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beauty_salon`.`roles` (
                                                      `id` INT NOT NULL AUTO_INCREMENT,
                                                      `name` VARCHAR(40) NULL DEFAULT NULL,
                                                      PRIMARY KEY (`id`));

-- -----------------------------------------------------
-- Table `beauty_salon`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beauty_salon`.`users` (
                                                      `id` INT NOT NULL AUTO_INCREMENT,
                                                      `name` VARCHAR(150) NOT NULL,
                                                      `login` VARCHAR(40) NOT NULL,
                                                      `password` VARCHAR(32) NOT NULL,
                                                      `phone` VARCHAR(15) NOT NULL,
                                                      `role_id` INT NOT NULL,
                                                      PRIMARY KEY (`id`),
                                                      UNIQUE INDEX `login_UNIQUE` (`login`),
                                                      CONSTRAINT `fk_users_roles`
                                                          FOREIGN KEY (`role_id`)
                                                              REFERENCES `beauty_salon`.`roles` (`id`)
                                                              ON DELETE NO ACTION
                                                              ON UPDATE NO ACTION);

CREATE TABLE IF NOT EXISTS `beauty_salon`.`services` (
                                                         `id` INT NOT NULL AUTO_INCREMENT,
                                                         `name` VARCHAR(45) NOT NULL,
                                                         `price` int NOT NULL,
                                                         `estimated_time` int NOT NULL,
                                                         PRIMARY KEY (`id`),
                                                         UNIQUE INDEX `name_UNIQUE` (`name`));

-- -----------------------------------------------------
-- Table `beauty_salon`.`days`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beauty_salon`.`days` (
                                                     `name` VARCHAR(15) NOT NULL,
                                                     PRIMARY KEY (`name`));

-- -----------------------------------------------------
-- Table `beauty_salon`.`schedule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beauty_salon`.`schedule` (
                                                         `id` INT NOT NULL AUTO_INCREMENT,
                                                         `time_start` TIME NOT NULL,
                                                         `time_end` TIME NOT NULL,
                                                         `day` VARCHAR(15) NOT NULL,
                                                         `open` BOOLEAN NOT NULL,
                                                         PRIMARY KEY (`id`),
                                                         CONSTRAINT `fk_Shedual_days`
                                                             FOREIGN KEY (`day`)
                                                                 REFERENCES `beauty_salon`.`days` (`name`)
                                                                 ON DELETE NO ACTION
                                                                 ON UPDATE NO ACTION);

CREATE TABLE IF NOT EXISTS `beauty_salon`.`worker_schedule` (
                                                                `id` INT NOT NULL AUTO_INCREMENT,
                                                                `time_start` TIME NOT NULL,
                                                                `time_end` TIME NOT NULL,
                                                                `name` varchar(20),
                                                                PRIMARY KEY (`id`));

-- -----------------------------------------------------
-- Table `beauty_salon`.`work_date`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beauty_salon`.`work_date` (
                                                          `worker_id` INT NOT NULL AUTO_INCREMENT,
                                                          `worker_schedule_id` INT NOT NULL,
                                                          `date` DATE NOT NULL,
                                                          PRIMARY KEY (`worker_id`, `worker_schedule_id`, `date`),
                                                          CONSTRAINT `fk_work_date_users`
                                                              FOREIGN KEY (`worker_id`)
                                                                  REFERENCES `beauty_salon`.`users` (`id`)
                                                                  ON DELETE NO ACTION
                                                                  ON UPDATE NO ACTION,
                                                          CONSTRAINT `fk_work_date_worker_schedule`
                                                              FOREIGN KEY (`worker_schedule_id`)
                                                                  REFERENCES `beauty_salon`.`worker_schedule` (`id`)
                                                                  ON DELETE NO ACTION
                                                                  ON UPDATE NO ACTION);

CREATE TABLE IF NOT EXISTS `beauty_salon`.`roles_has_services` (
                                                                   `roles_id` INT NOT NULL,
                                                                   `services_id` INT NOT NULL,
                                                                   PRIMARY KEY (`roles_id`, `services_id`),
                                                                   CONSTRAINT `fk_roles_has_services_roles`
                                                                       FOREIGN KEY (`roles_id`)
                                                                           REFERENCES `beauty_salon`.`roles` (`id`)
                                                                           ON DELETE NO ACTION
                                                                           ON UPDATE NO ACTION,
                                                                   CONSTRAINT `fk_roles_has_services_services`
                                                                       FOREIGN KEY (`services_id`)
                                                                           REFERENCES `beauty_salon`.`services` (`id`)
                                                                           ON DELETE NO ACTION
                                                                           ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table `beauty_salon`.`appointment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `beauty_salon`.`appointment` (
                                                            `id` INT NOT NULL AUTO_INCREMENT,
                                                            `date` DATE NOT NULL,
                                                            `time_start` TIME NOT NULL,
                                                            `time_end` TIME NOT NULL,
                                                            `price` int NOT NULL check(`price` > 0),
                                                            `accepted` BOOLEAN NOT NULL default false,
                                                            `completed` BOOLEAN NOT NULL default false,
                                                            `canceled` BOOLEAN NOT NULL default false,
                                                            `services_id` INT NOT NULL,
                                                            `users_id` INT NOT NULL,
                                                            `worker_id` INT NULL,
                                                            PRIMARY KEY (`id`),
                                                            CONSTRAINT `fk_appointment_services`
                                                                FOREIGN KEY (`services_id`)
                                                                    REFERENCES `beauty_salon`.`services` (`id`)
                                                                    ON DELETE NO ACTION
                                                                    ON UPDATE NO ACTION,
                                                            CONSTRAINT `fk_appointment_users`
                                                                FOREIGN KEY (`users_id`)
                                                                    REFERENCES `beauty_salon`.`users` (`id`)
                                                                    ON DELETE NO ACTION
                                                                    ON UPDATE NO ACTION,
                                                            CONSTRAINT `fk_appointment_users2`
                                                                FOREIGN KEY (`worker_id`)
                                                                    REFERENCES `beauty_salon`.`users` (`id`)
                                                                    ON DELETE NO ACTION
                                                                    ON UPDATE NO ACTION,
                                                            CONSTRAINT time_less_end CHECK (time_start < time_end)
);

CREATE TABLE IF NOT EXISTS `beauty_salon`.`evaluations` (
                                                            `appointment_id` INT NOT NULL,
                                                            `evaluation` INT NOT NULL check(evaluation >=0 and evaluation <= 10),
                                                            PRIMARY KEY (`appointment_id`),
                                                            CONSTRAINT `fk_appointment_id`
                                                                FOREIGN KEY (`appointment_id`)
                                                                    REFERENCES `beauty_salon`.`appointment` (`id`)
                                                                    ON DELETE no action
                                                                    ON UPDATE no action
);

-- -----------------------------------------------------
-- insert roles
-- -----------------------------------------------------
insert into `roles`
(name)
values ('admin');
insert into `roles`
(name)
values ('client');
insert into `roles`
(name)
values ('master');

-- -----------------------------------------------------
-- insert services
-- -----------------------------------------------------
insert into `services`
(name, estimated_time, price)
values ('haircut', '30', '10000');
insert into `services`
(name, estimated_time, price)
values ('nail', '30', '20000');
insert into `services`
(name, estimated_time, price)
values ('peeling', '180', '50000');


create view `admins_view` as select * from `users` where `role_id` = 1;
create view `clients_view` as select * from `users` where `role_id` = 2;
create view `masters_view` as select * from `users` where `role_id` = 3;

-- SET GLOBAL event_scheduler = off; -- enable event scheduler.
SELECT @@event_scheduler;  -- check whether event scheduler is ON/OFF
SET SQL_SAFE_UPDATES = off;

drop event if exists event_cancel_appointment;
CREATE EVENT event_cancel_appointment
    ON SCHEDULE
        EVERY 24 hour
    DO
    UPDATE appointment set appointment.canceled = true where appointment.date < CURDATE();


insert into users (login, password, name, role_id, phone) VALUES('dimakomishny@gmail.com', md5('password'), 'user1', 1, '2');

insert into users (login, password, name, role_id, phone) VALUES('fakeUser', md5('password'), 'fakeUser', 2, '000');


insert into users (login, password, name, role_id, phone) VALUES('user2', md5('pas'), 'user2', 2, '2');
insert into users (login, password, name, role_id, phone) VALUES('user3', md5('pas'), 'user3', 2, '3');
insert into users (login, password, name, role_id, phone) VALUES('user4', md5('pas'), 'user4', 2, '4');
insert into users (login, password, name, role_id, phone) VALUES('user5', md5('pas'), 'user4', 2, '5');

insert into users (login, password, name, role_id, phone) VALUES('master6@gamil.com', md5('pas'), 'master5', 3, '6');
insert into users (login, password, name, role_id, phone) VALUES('master7@gmail.com', md5('	'), 'master6', 3, '7');
insert into users (login, password, name, role_id, phone) VALUES('saster6@gmail.com', md5('pas'), 'aster5', 3, '18');

insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-3-11', '15:00:00', '15:30:00', 10000, 1, 3, 7);
insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-3-12', '15:00:00', '15:30:00', 10000, 1, 3, 7);
insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-3-13', '12:00:00', '13:30:00', 10000, 1, 3, 7);
insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-3-11', '8:30:00', '10:00:00', 10000, 1, 3, 8);
insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-3-12', '13:00:00', '13:50:00', 10000, 1, 3, 8);
insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-3-13', '14:00:00', '14:30:00', 10000, 1, 3, 8);
insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-3-14', '14:00:00', '14:30:00', 10000, 1, 3, 8);

insert into worker_schedule (time_start, time_end, `name`) VALUES('9:00:00', '18:00:00', 'Weekday');
insert into worker_schedule (time_start, time_end, `name`) VALUES('10:00:00', '16:00:00', 'Weekend');

insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('6', '1', '2021-3-11');
insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('6', '1', '2021-3-12');
insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('6', '1', '2021-3-13');
insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('6', '1', '2021-3-14');
insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('8', '1', '2021-3-11');
insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('8', '1', '2021-3-12');
insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('8', '1', '2021-3-13');
insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('8', '1', '2021-3-14');


insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('8', '1', '2021-3-14');

insert into evaluations (appointment_id, evaluation) VALUES(1, 5);
insert into evaluations (appointment_id, evaluation) VALUES(2, 6);
insert into evaluations (appointment_id, evaluation) VALUES(5, 6);
insert into evaluations (appointment_id, evaluation) VALUES(3, 10);
insert into evaluations (appointment_id, evaluation) VALUES(7, 1);
