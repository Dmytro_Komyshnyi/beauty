package com.beauty.beauty.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PaginationServiceImplTest {

    private PaginationService paginationService = new PaginationServiceImpl();

    @Test
    public void testFindPageShouldReturnOneIfStrPageNumIsNull() {
        int expected = 1;
        int result = paginationService.findPage(null, 10);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testFindPageShouldReturnOneIfStrPageIsNegativeNumber() {
        int expected = 1;
        int result = paginationService.findPage("-1", 10);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testFindPageShouldReturnOneIfStrPageIsNotNumber() {
        int expected = 1;
        int result = paginationService.findPage("str", 10);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testFindPageShouldReturnLastPageIfPageMoreThanAvailablePage() {
        int expected = 10;
        int result = paginationService.findPage("15", 10);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testFindPageShouldReturnTenCaseStrPageIsTenAvailablePageIsFifteen() {
        int expected = 10;
        int result = paginationService.findPage("10", 15);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testFindPageShouldReturnOneCaseStrPageIsTwoAvailablePageIsZero() {
        int expected = 1;
        int result = paginationService.findPage("2", 0);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testCalcAvailablePageShouldReturnZeroWhenRowsZero() {
        int expected = 0;
        int rows = 0;
        int pageSize = 10;
        int result = paginationService.calcAvailablePage(rows, pageSize);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testCalcAvailablePageShouldReturnOneIfRowsLessThenPageSize() {
        int expected = 1;
        int rows = 8;
        int pageSize = 10;
        int result = paginationService.calcAvailablePage(rows, pageSize);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testCalcAvailablePageShouldReturnFifePageIfPageSizeIsFifeAndRowsIsTwentyFive() {
        int expected = 5;
        int rows = 25;
        int pageSize = 5;
        int result = paginationService.calcAvailablePage(rows, pageSize);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void testCalcAvailablePageShouldReturnSixPageIfPageSizeIsFifeAndRowsIsTwentySix() {
        int expected = 6;
        int rows = 26;
        int pageSize = 5;
        int result = paginationService.calcAvailablePage(rows, pageSize);
        Assertions.assertEquals(expected, result);
    }

}