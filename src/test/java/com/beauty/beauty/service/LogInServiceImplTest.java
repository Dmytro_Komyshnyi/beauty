package com.beauty.beauty.service;

import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.db.entity.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class LogInServiceImplTest {

    LogInService logInService = new LogInServiceImpl(null);

    @Test
    public void testCreateUserShouldCreateUser() throws ServletServiceException {
        User expected = new User("login@gmail.com", "password", Role.Client, "fullName", "0967778890");
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getParameter("login")).thenReturn(expected.getLogin());
        when(req.getParameter("password")).thenReturn(expected.getPassword());
        when(req.getParameter("fullname")).thenReturn(expected.getName());
        when(req.getParameter("phone")).thenReturn(expected.getPhone());
        User result = logInService.createUser(req, expected.getRole());
        assertEquals(expected.getRole(), result.getRole());
        assertEquals(expected.getLogin(), result.getLogin());
        assertEquals(expected.getPassword(), result.getPassword());
        assertEquals(expected.getName(), result.getName());
        assertEquals(expected.getPhone(), result.getPhone());
    }

    @Test
    public void testCreateUserShouldThrowExceptionIfPasswordIllegal() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpSession session = mock(HttpSession.class);
        when(req.getSession()).thenReturn(session);
        when(req.getParameter("login")).thenReturn("login@gmail.com");
        when(req.getParameter("password")).thenReturn("pas");
        when(req.getParameter("fullname")).thenReturn("fullName");
        when(req.getParameter("phone")).thenReturn("0967778890");
        ServletServiceException exception = Assertions.assertThrows(ServletServiceException.class,
                () -> logInService.createUser(req, Role.Client));
        String expectedMessage = "Incorrect password";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testCreateUserShouldThrowExceptionIfLoginIllegal() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpSession session = mock(HttpSession.class);
        when(req.getSession()).thenReturn(session);
        when(req.getParameter("login")).thenReturn("logingmail.com");
        when(req.getParameter("password")).thenReturn("password");
        when(req.getParameter("fullname")).thenReturn("fullName");
        when(req.getParameter("phone")).thenReturn("0967778890");
        ServletServiceException exception = Assertions.assertThrows(ServletServiceException.class,
                () -> logInService.createUser(req, Role.Client));
        String expectedMessage = "Incorrect login";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    public void testCreateUserShouldThrowExceptionIfNameIllegal() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpSession session = mock(HttpSession.class);
        when(req.getSession()).thenReturn(session);
        when(req.getParameter("login")).thenReturn("login@gmail.com");
        when(req.getParameter("password")).thenReturn("password");
        when(req.getParameter("fullname")).thenReturn("<");
        when(req.getParameter("phone")).thenReturn("0967778890");
        ServletServiceException exception = Assertions.assertThrows(ServletServiceException.class,
                () -> logInService.createUser(req, Role.Client));
        String expectedMessage = "Incorrect user name";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testCreateUserShouldThrowExceptionIfPhoneIllegal() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpSession session = mock(HttpSession.class);
        when(req.getSession()).thenReturn(session);
        when(req.getParameter("login")).thenReturn("login@gmail.com");
        when(req.getParameter("password")).thenReturn("password");
        when(req.getParameter("fullname")).thenReturn("fullName");
        when(req.getParameter("phone")).thenReturn("0967778890123");
        ServletServiceException exception = Assertions.assertThrows(ServletServiceException.class,
                () -> logInService.createUser(req, Role.Client));
        String expectedMessage = "Incorrect phone";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testCreateUserShouldThrowExceptionIfParametersIsNull() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpSession session = mock(HttpSession.class);
        when(req.getSession()).thenReturn(session);
        when(req.getParameter("login")).thenReturn("login@gmail.com");
        when(req.getParameter("password")).thenReturn("password");
        when(req.getParameter("fullname")).thenReturn(null);
        when(req.getParameter("phone")).thenReturn("0967778890");
        ServletServiceException exception = Assertions.assertThrows(ServletServiceException.class,
                () -> logInService.createUser(req, Role.Client));
        String expectedMessage = "didn't fill all fields";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

}