package com.beauty.beauty.service;

import com.beauty.beauty.db.bean.TimeSlotBean;
import com.beauty.beauty.db.dao.*;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TimeServiceImplTest {

    ServiceDAO serviceDAO;
    UserDAO userDAO;
    AppointmentDAO appointmentDAO;
    TimeService timeService = new TimeServiceImpl();

    @Test
    public void testCalcFreeSlotsSimpleWork()
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method calcFreeSlots = TimeServiceImpl.class
                .getDeclaredMethod("calcFreeSlots", TimeSlotBean.class, List.class, int.class);
        calcFreeSlots.setAccessible(true);
        int serviceTime = 30;
        TimeSlotBean workTime = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        List<TimeSlotBean> slots = new ArrayList<>();
        slots.add(new TimeSlotBean(Time.valueOf("9:30:00"), Time.valueOf("13:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("14:30:00"), Time.valueOf("15:00:00")));
        List<TimeSlotBean> list = (List<TimeSlotBean>) calcFreeSlots.invoke(null, workTime, slots, serviceTime);
        StringBuilder sb = new StringBuilder();
        for (TimeSlotBean slotBean : list) {
            sb
                    .append(slotBean.getTimeStart())
                    .append("\n")
                    .append(slotBean.getTimeEnd())
                    .append("\n");
        }
        String expected =
                "09:00:00\n" +
                        "09:00:00\n" +
                        "13:00:00\n" +
                        "14:00:00\n" +
                        "15:00:00\n" +
                        "17:30:00\n";
        String result = sb.toString();
        assertEquals(expected, result);
    }

    @Test
    public void testCalcFreeSlotsFirstAppointment()
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method calcFreeSlots = TimeServiceImpl.class
                .getDeclaredMethod("calcFreeSlots", TimeSlotBean.class, List.class, int.class);
        calcFreeSlots.setAccessible(true);
        int serviceTime = 30;
        TimeSlotBean workTime = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        List<TimeSlotBean> slots = new ArrayList<>();
        List<TimeSlotBean> list = (List<TimeSlotBean>) calcFreeSlots.invoke(null, workTime, slots, serviceTime);
        StringBuilder sb = new StringBuilder();
        for (TimeSlotBean slotBean : list) {
            sb
                    .append(slotBean.getTimeStart())
                    .append("\n")
                    .append(slotBean.getTimeEnd())
                    .append("\n");
        }
        String expected =
                "09:00:00\n" +
                        "17:30:00\n";
        String result = sb.toString();
        assertEquals(expected, result);
    }

    @Test
    public void testGetFreeTimeSlot() throws NoSuchFieldException, IllegalAccessException, ServiceDaoException,
            AppointmentDaoException, UserDaoException, ServletServiceException {
        prepareTestGetFreeTimeSlots();
        String serviceName = "service";
        int masterId = 1;
        Date date = Date.valueOf("2000-05-14");
        List<TimeSlotBean> slots = new ArrayList<>();
        slots.add(new TimeSlotBean(Time.valueOf("9:30:00"), Time.valueOf("10:30:00")));
        slots.add(new TimeSlotBean(Time.valueOf("10:30:00"), Time.valueOf("11:15:00")));
        slots.add(new TimeSlotBean(Time.valueOf("11:45:00"), Time.valueOf("12:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("14:30:00"), Time.valueOf("15:15:00")));
        TimeSlotBean workHours = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));

        when(serviceDAO.getEstimatedTime(serviceName)).thenReturn(30);
        when(appointmentDAO.getBusyTimeSlotOrderTimeStart(date, masterId)).thenReturn(slots);
        when(userDAO.getWorkTimeByDate(masterId, date)).thenReturn(workHours);

        StringBuilder sb = new StringBuilder();
        TimeService timeService = new TimeServiceImpl();
        List<TimeSlotBean> freeSlots = timeService.getFreeTimeSlots(date, masterId, serviceName);
        for (TimeSlotBean s : freeSlots) {
            sb.append(s.getTimeStart()).append("\n").append(s.getTimeEnd()).append("\n");
        }
        sb.deleteCharAt(sb.length()-1);
        String result = sb.toString();
        String expected =
                "09:00:00\n" +
                "09:00:00\n" +
                "11:15:00\n" +
                "11:15:00\n" +
                "12:00:00\n" +
                "14:00:00\n" +
                "15:15:00\n" +
                "17:30:00";
        assertEquals(expected, result);
    }

    @Test
    public void testGetFreeTimeSlotWithFreeSlot() throws NoSuchFieldException, IllegalAccessException, ServiceDaoException,
            AppointmentDaoException, UserDaoException, ServletServiceException {
        prepareTestGetFreeTimeSlots();
        String serviceName = "service";
        int masterId = 1;
        Date date = Date.valueOf("2000-05-14");
        List<TimeSlotBean> slots = new ArrayList<>();
        slots.add(new TimeSlotBean(Time.valueOf("9:30:00"), Time.valueOf("10:30:00")));
        slots.add(new TimeSlotBean(Time.valueOf("10:30:00"), Time.valueOf("11:15:00")));
        slots.add(new TimeSlotBean(Time.valueOf("11:45:00"), Time.valueOf("12:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("14:30:00"), Time.valueOf("15:15:00")));
        TimeSlotBean workHours = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        when(serviceDAO.getEstimatedTime(serviceName)).thenReturn(30);
        when(appointmentDAO.getBusyTimeSlotOrderTimeStart(date, masterId)).thenReturn(slots);
        when(userDAO.getWorkTimeByDate(masterId, date)).thenReturn(workHours);
        TimeSlotBean freeSlot = new TimeSlotBean(Time.valueOf("9:30:00"), Time.valueOf("10:30:00"));
        StringBuilder sb = new StringBuilder();
        TimeService timeService = new TimeServiceImpl();
        List<TimeSlotBean> freeSlots = timeService.getFreeTimeSlots(date, masterId, serviceName, freeSlot);
        for (TimeSlotBean s : freeSlots) {
            sb.append(s.getTimeStart()).append("\n").append(s.getTimeEnd()).append("\n");
        }
        sb.deleteCharAt(sb.length()-1);
        String result = sb.toString();
        String expected =
                "09:00:00\n" +
                        "10:00:00\n" +
                        "11:15:00\n" +
                        "11:15:00\n" +
                        "12:00:00\n" +
                        "14:00:00\n" +
                        "15:15:00\n" +
                        "17:30:00";
        assertEquals(expected, result);
    }

    @Test
    public void testGetFreeTimeSlotWorkTimesIsNull() throws NoSuchFieldException, IllegalAccessException,
            ServiceDaoException, AppointmentDaoException, UserDaoException, ServletServiceException {
        prepareTestGetFreeTimeSlots();
        String serviceName = "service";
        int masterId = 1;
        Date date = Date.valueOf("2000-05-14");
        List<TimeSlotBean> slots = new ArrayList<>();
        slots.add(new TimeSlotBean(Time.valueOf("9:30:00"), Time.valueOf("10:30:00")));
        slots.add(new TimeSlotBean(Time.valueOf("10:30:00"), Time.valueOf("11:15:00")));
        slots.add(new TimeSlotBean(Time.valueOf("11:45:00"), Time.valueOf("12:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("14:30:00"), Time.valueOf("15:15:00")));
        TimeSlotBean workHours = null;
        when(serviceDAO.getEstimatedTime(serviceName)).thenReturn(30);
        when(appointmentDAO.getBusyTimeSlotOrderTimeStart(date, masterId)).thenReturn(slots);
        when(userDAO.getWorkTimeByDate(masterId, date)).thenReturn(workHours);
        List<TimeSlotBean> freeSlots = timeService.getFreeTimeSlots(date, masterId, serviceName);
        assertNull(freeSlots);
    }

    @Test
    public void testGetFreeTimeSlotWithFreeTimeWorkTimesIsNull() throws NoSuchFieldException, IllegalAccessException,
            ServiceDaoException, AppointmentDaoException, UserDaoException, ServletServiceException {
        prepareTestGetFreeTimeSlots();
        String serviceName = "service";
        int masterId = 1;
        Date date = Date.valueOf("2000-05-14");
        List<TimeSlotBean> slots = new ArrayList<>();
        slots.add(new TimeSlotBean(Time.valueOf("9:30:00"), Time.valueOf("10:30:00")));
        slots.add(new TimeSlotBean(Time.valueOf("10:30:00"), Time.valueOf("11:15:00")));
        slots.add(new TimeSlotBean(Time.valueOf("11:45:00"), Time.valueOf("12:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("14:30:00"), Time.valueOf("15:15:00")));
        TimeSlotBean workHours = null;
        TimeSlotBean freeSlot = new TimeSlotBean(Time.valueOf("9:30:00"), Time.valueOf("10:00:00"));
        when(serviceDAO.getEstimatedTime(serviceName)).thenReturn(30);
        when(appointmentDAO.getBusyTimeSlotOrderTimeStart(date, masterId)).thenReturn(slots);
        when(userDAO.getWorkTimeByDate(masterId, date)).thenReturn(workHours);
        List<TimeSlotBean> freeSlots = timeService.getFreeTimeSlots(date, masterId, serviceName, freeSlot);
        assertNull(freeSlots);
    }

    @Test
    public void testGetFreeTimeSlotBusySlotsEmpty() throws NoSuchFieldException, IllegalAccessException,
            ServiceDaoException, AppointmentDaoException, UserDaoException, ServletServiceException {
        prepareTestGetFreeTimeSlots();
        String serviceName = "services ";
        int masterId = 1;
        Date date = Date.valueOf("2000-05-14");
        List<TimeSlotBean> slots = new ArrayList<>();
        TimeSlotBean workHours = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        when(serviceDAO.getEstimatedTime(serviceName)).thenReturn(30);
        when(appointmentDAO.getBusyTimeSlotOrderTimeStart(date, masterId)).thenReturn(slots);
        when(userDAO.getWorkTimeByDate(masterId, date)).thenReturn(workHours);
        StringBuilder sb = new StringBuilder();
        List<TimeSlotBean> freeSlots = timeService.getFreeTimeSlots(date, masterId, serviceName);
        for (TimeSlotBean s : freeSlots) {
            sb.append(s.getTimeStart()).append("\n").append(s.getTimeEnd()).append("\n");
        }
        sb.deleteCharAt(sb.length()-1);
        String result = sb.toString();
        String expected =
                "09:00:00\n" +
                "17:30:00";
        assertEquals(expected, result);
    }

    @Test
    public void testGetFreeTimeSlotWithFreeSlotShouldCatchAppointmentDAOException() throws NoSuchFieldException, IllegalAccessException,
            ServiceDaoException, AppointmentDaoException, UserDaoException {
        prepareTestGetFreeTimeSlots();
        String serviceName = "services";
        int masterId = 1;
        Date date = Date.valueOf("2000-05-14");
        TimeSlotBean workHours = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        when(serviceDAO.getEstimatedTime(serviceName)).thenReturn(30);
        when(appointmentDAO.getBusyTimeSlotOrderTimeStart(date, masterId)).thenThrow(new AppointmentDaoException("Test exception"));
        when(userDAO.getWorkTimeByDate(masterId, date)).thenReturn(workHours);
        TimeSlotBean freeSlot = new TimeSlotBean(Time.valueOf("9:30:00"), Time.valueOf("10:00:00"));
        Exception ex = assertThrows(ServletServiceException.class,
                () -> timeService.getFreeTimeSlots(date, masterId, serviceName, freeSlot));
        String expectedMessage = "can not get busy slots from db by master id["+masterId
                +"]" + " date[" +date+ "]";
        assertTrue(ex.getMessage().contains(expectedMessage));
    }

    @Test
    public void testGetFreeTimeSlotWithFreeSlotShouldCatchUserDAOException() throws NoSuchFieldException, IllegalAccessException,
            ServiceDaoException, AppointmentDaoException, UserDaoException {
        prepareTestGetFreeTimeSlots();
        String serviceName = "services";
        int masterId = 1;
        Date date = Date.valueOf("2000-05-14");
        List<TimeSlotBean> slots = new ArrayList<>();
        when(serviceDAO.getEstimatedTime(serviceName)).thenReturn(30);
        when(appointmentDAO.getBusyTimeSlotOrderTimeStart(date, masterId)).thenReturn(slots);
        when(userDAO.getWorkTimeByDate(masterId, date)).thenThrow(new UserDaoException("Test exception"));
        TimeSlotBean freeSlot = new TimeSlotBean(Time.valueOf("9:30:00"), Time.valueOf("10:00:00"));
        Exception ex = assertThrows(ServletServiceException.class,
                () -> timeService.getFreeTimeSlots(date, masterId, serviceName, freeSlot));
        String expectedMessage = "can not get master from db by master id[" +masterId+"]";
        assertTrue(ex.getMessage().contains(expectedMessage));
    }

    @Test
    public void testGetFreeTimeSlotWithFreeSlotShouldCatchServiceDAOException() throws NoSuchFieldException, IllegalAccessException,
            ServiceDaoException, AppointmentDaoException, UserDaoException {
        prepareTestGetFreeTimeSlots();
        String serviceName = "services";
        int masterId = 1;
        Date date = Date.valueOf("2000-05-14");
        List<TimeSlotBean> slots = new ArrayList<>();
        TimeSlotBean workHours = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        when(serviceDAO.getEstimatedTime(serviceName)).thenThrow(new ServiceDaoException("Test exception"));
        when(appointmentDAO.getBusyTimeSlotOrderTimeStart(date, masterId)).thenReturn(slots);
        when(userDAO.getWorkTimeByDate(masterId, date)).thenReturn(workHours);
        TimeSlotBean freeSlot = new TimeSlotBean(Time.valueOf("9:30:00"), Time.valueOf("10:00:00"));
        Exception ex = assertThrows(ServletServiceException.class,
                () -> timeService.getFreeTimeSlots(date, masterId, serviceName, freeSlot));
        String expectedMessage = "can not get service from db by serviceName["+serviceName+"]";
        assertTrue(ex.getMessage().contains(expectedMessage));
    }

    @Test
    public void testGetFreeTimeSLotShouldCatchAppointmentDAOException() throws NoSuchFieldException, IllegalAccessException,
            ServiceDaoException, AppointmentDaoException, UserDaoException {
        prepareTestGetFreeTimeSlots();
        String serviceName = "services";
        int masterId = 1;
        Date date = Date.valueOf("2000-05-14");
        TimeSlotBean workHours = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        when(serviceDAO.getEstimatedTime(serviceName)).thenReturn(30);
        when(appointmentDAO.getBusyTimeSlotOrderTimeStart(date, masterId)).thenThrow(new AppointmentDaoException("Test exception"));
        when(userDAO.getWorkTimeByDate(masterId, date)).thenReturn(workHours);
        Exception ex = assertThrows(ServletServiceException.class,
                () -> timeService.getFreeTimeSlots(date, masterId, serviceName));
        String expectedMessage = "can not get busy slots from db by master id["+masterId
                +"]" + " date[" +date+ "]";
        assertTrue(ex.getMessage().contains(expectedMessage));
    }

    @Test
    public void testGetFreeTimeSlotShouldCatchUserDAOException() throws NoSuchFieldException, IllegalAccessException,
            ServiceDaoException, AppointmentDaoException, UserDaoException {
        prepareTestGetFreeTimeSlots();
        String serviceName = "services";
        int masterId = 1;
        Date date = Date.valueOf("2000-05-14");
        List<TimeSlotBean> slots = new ArrayList<>();
        when(serviceDAO.getEstimatedTime(serviceName)).thenReturn(30);
        when(appointmentDAO.getBusyTimeSlotOrderTimeStart(date, masterId)).thenReturn(slots);
        when(userDAO.getWorkTimeByDate(masterId, date)).thenThrow(new UserDaoException("Test exception"));
        Exception ex = assertThrows(ServletServiceException.class,
                () -> timeService.getFreeTimeSlots(date, masterId, serviceName));
        String expectedMessage = "can not get master from db by master id[" +masterId+"]";
        assertTrue(ex.getMessage().contains(expectedMessage));
    }

    @Test
    public void testGetFreeTimeSlotShouldCatchServiceDAOException() throws NoSuchFieldException, IllegalAccessException,
            ServiceDaoException, AppointmentDaoException, UserDaoException {
        prepareTestGetFreeTimeSlots();
        String serviceName = "services";
        int masterId = 1;
        Date date = Date.valueOf("2000-05-14");
        List<TimeSlotBean> slots = new ArrayList<>();
        TimeSlotBean workHours = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        when(serviceDAO.getEstimatedTime(serviceName)).thenThrow(new ServiceDaoException("Test exception"));
        when(appointmentDAO.getBusyTimeSlotOrderTimeStart(date, masterId)).thenReturn(slots);
        when(userDAO.getWorkTimeByDate(masterId, date)).thenReturn(workHours);
        Exception ex = assertThrows(ServletServiceException.class,
                () -> timeService.getFreeTimeSlots(date, masterId, serviceName));
        String expectedMessage = "can not get service from db by serviceName["+serviceName+"]";
        assertTrue(ex.getMessage().contains(expectedMessage));
    }

    private void prepareTestGetFreeTimeSlots() throws NoSuchFieldException, IllegalAccessException {
        serviceDAO = mock(ServiceDAO.class);
        userDAO = mock(UserDAO.class);
        appointmentDAO = mock(AppointmentDAO.class);
        Field fieldServiceDAO = TimeServiceImpl.class.getDeclaredField("SERVICE_DAO");
        Field fieldUserDAO = TimeServiceImpl.class.getDeclaredField("USER_DAO");
        Field fieldAppointmentDAO = TimeServiceImpl.class.getDeclaredField("appointmentDAO");
        fieldServiceDAO.setAccessible(true);
        fieldUserDAO.setAccessible(true);
        fieldAppointmentDAO.setAccessible(true);
        fieldServiceDAO.set(null, serviceDAO);
        fieldUserDAO.set(null, userDAO);
        fieldAppointmentDAO.set(null, appointmentDAO);
    }

    @Test
    public void testValidTimeShouldReturnTrue() {
        List<TimeSlotBean> slots = new ArrayList<>();
        slots.add(new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("9:35:00")));
        slots.add(new TimeSlotBean(Time.valueOf("9:35:00"), Time.valueOf("11:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("12:00:00"), Time.valueOf("13:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("13:45:00"), Time.valueOf("14:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("17:00:00"), Time.valueOf("18:00:00")));
        TimeSlotBean workerTime = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        TimeSlotBean slot = new TimeSlotBean(Time.valueOf("13:00:00"), Time.valueOf("13:45:00"));
        boolean result = timeService.validateTime(slot, slots, workerTime);
        assertTrue(result);
    }

    @Test
    public void testValidTimeShouldReturnTrueIfBusySlotsEmptyAndSlotValid() {
        List<TimeSlotBean> slots = new ArrayList<>();
        TimeSlotBean workerTime = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        TimeSlotBean slot = new TimeSlotBean(Time.valueOf("13:00:00"), Time.valueOf("13:30:00"));
        TimeService timeService = new TimeServiceImpl();
        boolean result = timeService.validateTime(slot, slots, workerTime);
        assertTrue(result);
    }

    @Test
    public void testValidTimeShouldReturnFalseIfSlotTakeBusySlotPartlyEndMore() {
        List<TimeSlotBean> slots = new ArrayList<>();
        slots.add(new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("9:35:00")));
        slots.add(new TimeSlotBean(Time.valueOf("9:35:00"), Time.valueOf("11:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("12:00:00"), Time.valueOf("13:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("13:45:00"), Time.valueOf("14:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("17:00:00"), Time.valueOf("18:00:00")));
        TimeSlotBean workerTime = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        TimeSlotBean slot = new TimeSlotBean(Time.valueOf("11:00:00"), Time.valueOf("12:30:00"));
        boolean result = timeService.validateTime(slot, slots, workerTime);
        assertFalse(result);
    }

    @Test
    public void testValidTimeShouldReturnFalseIfSlotTakeBusySlotPartlyStartMore() {
        List<TimeSlotBean> slots = new ArrayList<>();
        slots.add(new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("9:35:00")));
        slots.add(new TimeSlotBean(Time.valueOf("9:35:00"), Time.valueOf("11:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("12:00:00"), Time.valueOf("13:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("13:45:00"), Time.valueOf("14:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("17:00:00"), Time.valueOf("18:00:00")));
        TimeSlotBean workerTime = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        TimeSlotBean slot = new TimeSlotBean(Time.valueOf("12:45:00"), Time.valueOf("13:30:00"));
        boolean result = timeService.validateTime(slot, slots, workerTime);
        assertFalse(result);
    }

    @Test
    public void testValidTimeShouldReturnFalseIfSlotTakeBusySlot() {
        List<TimeSlotBean> slots = new ArrayList<>();
        slots.add(new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("9:35:00")));
        slots.add(new TimeSlotBean(Time.valueOf("9:35:00"), Time.valueOf("11:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("12:00:00"), Time.valueOf("13:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("13:45:00"), Time.valueOf("14:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("17:00:00"), Time.valueOf("18:00:00")));
        TimeSlotBean workerTime = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        TimeSlotBean slot = new TimeSlotBean(Time.valueOf("12:00:00"), Time.valueOf("12:20:00"));
        boolean result = timeService.validateTime(slot, slots, workerTime);
        assertFalse(result);
    }

    @Test
    public void testValidTimeShouldReturnTrueIfSlotTakeBetweenStartWorkHourAndNextTimeSlot() {
        List<TimeSlotBean> slots = new ArrayList<>();
        slots.add(new TimeSlotBean(Time.valueOf("9:35:00"), Time.valueOf("11:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("12:00:00"), Time.valueOf("13:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("13:45:00"), Time.valueOf("14:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("17:00:00"), Time.valueOf("18:00:00")));
        TimeSlotBean workerTime = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        TimeSlotBean slot = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("9:35:00"));
        boolean result = timeService.validateTime(slot, slots, workerTime);
        assertTrue(result);
    }

    @Test
    public void testValidTimeShouldReturnTrueIfSlotTakeBetweenEndWorkHourAndNextTimeSlot() {
        List<TimeSlotBean> slots = new ArrayList<>();
        slots.add(new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("9:35:00")));
        slots.add(new TimeSlotBean(Time.valueOf("9:35:00"), Time.valueOf("11:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("12:00:00"), Time.valueOf("13:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("13:45:00"), Time.valueOf("14:00:00")));
        slots.add(new TimeSlotBean(Time.valueOf("17:00:00"), Time.valueOf("17:30:00")));
        TimeSlotBean workerTime = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        TimeSlotBean slot = new TimeSlotBean(Time.valueOf("17:30:00"), Time.valueOf("18:00:00"));
        boolean result = timeService.validateTime(slot, slots, workerTime);
        assertTrue(result);
    }

    @Test
    public void testValidTimeShouldReturnFalseIfBusySlotsEmptyAndSlotTakeTimeAfterWorkTimeRight() {
        List<TimeSlotBean> slots = new ArrayList<>();
        TimeSlotBean workerTime = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        TimeSlotBean slot = new TimeSlotBean(Time.valueOf("13:00:00"), Time.valueOf("19:00:00"));
        boolean result = timeService.validateTime(slot, slots, workerTime);
        assertFalse(result);
    }

    @Test
    public void testValidTimeShouldReturnFalseIfBusySlotsEmptyAndSlotTakeTimeAfterWorkTimeLeft() {
        List<TimeSlotBean> slots = new ArrayList<>();
        TimeSlotBean workerTime = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        TimeSlotBean slot = new TimeSlotBean(Time.valueOf("8:00:00"), Time.valueOf("13:30:00"));
        boolean result = timeService.validateTime(slot, slots, workerTime);
        assertFalse(result);
    }

    @Test
    public void testValidTimeShouldReturnFalseIfBusySlotsEmptyAndSlotTakeTimeAfterWorkTimeAllLeft() {
        List<TimeSlotBean> slots = new ArrayList<>();
        TimeSlotBean workerTime = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        TimeSlotBean slot = new TimeSlotBean(Time.valueOf("8:00:00"), Time.valueOf("9:00:00"));
        boolean result = timeService.validateTime(slot, slots, workerTime);
        assertFalse(result);
    }

    @Test
    public void testValidTimeShouldReturnFalseIfBusySlotsEmptyAndSlotTakeTimeAfterWorkTimeAllWright() {
        List<TimeSlotBean> slots = new ArrayList<>();
        TimeSlotBean workerTime = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        TimeSlotBean slot = new TimeSlotBean(Time.valueOf("18:00:00"), Time.valueOf("19:30:00"));
        boolean result = timeService.validateTime(slot, slots, workerTime);
        assertFalse(result);
    }

    @Test
    public void testValidTimeShouldReturnFalseIfTimeSlotReverse() {
        List<TimeSlotBean> slots = new ArrayList<>();
        TimeSlotBean workerTime = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        TimeSlotBean slot = new TimeSlotBean(Time.valueOf("14:00:00"), Time.valueOf("13:00:00"));
        boolean result = timeService.validateTime(slot, slots, workerTime);
        assertFalse(result);
    }

    @Test
    public void testParseShouldReturnCastableFormatStringTimeCaseTimeWithoutSeconds() {
        String input1 = "9:00";
        String expected1 = "9:00:00";
        String result = timeService.parseTime(input1);
        assertEquals(expected1, result);
        String input2 = "9:31";
        String expected2 = "9:31:00";
        result = timeService.parseTime(input2);
        assertEquals(expected2, result);
        String input3 = "09:21";
        String expected3 = "09:21:00";
        result = timeService.parseTime(input3);
        assertEquals(expected3, result);
        String input4 = "10:21";
        String expected4 = "10:21:00";
        result = timeService.parseTime(input4);
        assertEquals(expected4, result);
    }

    @Test
    public void testParseShouldReturnWithoutChangeIfTimeCorrect() {
        String input1 = "9:00:00";
        String result = timeService.parseTime(input1);
        assertEquals(input1, result);
        String input2 = "19:31:00";
        result = timeService.parseTime(input2);
        assertEquals(input2, result);
    }
}