package com.beauty.beauty.service;

import com.beauty.beauty.db.entity.Appointment;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ServletServiceImplTest {

    private ServletService servletService = new ServletServiceImpl();

    @Test
    public void testRemoveAppointmentFromSessionShouldRemoveAppointment() {
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        HttpSession session = Mockito.mock(HttpSession.class);
        List<Appointment> apps = new ArrayList<>();
        Appointment app1 = new Appointment();
        Appointment app2 = new Appointment();
        Appointment app3 = new Appointment();
        Appointment app4 = new Appointment();
        app1.setId(21);
        app2.setId(32);
        app3.setId(34);
        app4.setId(5);
        apps.add(app1);
        apps.add(app2);
        apps.add(app3);
        apps.add(app4);
        Mockito.when(session.getAttribute("appointments")).thenReturn(apps);
        Mockito.when(req.getSession()).thenReturn(session);
        servletService.removeAppointmentFromSession(req, app3.getId());
        int expected = -1;
        int result = apps.indexOf(app3);
        assertEquals(expected, result);
    }
}
