package com.beauty.beauty.util;

import com.beauty.beauty.db.bean.Email;
import com.beauty.beauty.db.entity.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpSession;
import java.sql.Date;

import static org.junit.jupiter.api.Assertions.*;

public class UtilTest {

    @Test
    public void testHasCastToStringShouldReturnFalseIfObjectCanNotCastToString() {
        Object ob = new Object();
        boolean result = Util.hasCastToString(ob);
        assertFalse(result);
    }

    @Test
    public void testHasCastToStringShouldReturnTrueIfObjectCanCastToString() {
        Object ob = "String";
        boolean result = Util.hasCastToString(ob);
        assertTrue(result);
    }

    @Test
    public void testHasCastToStringShouldReturnFalseIfFirstString() {
        Object ob1 = "string";
        Object ob2 = new Object();
        Object ob3 = new Object();
        Object ob4 = new Object();
        boolean result = Util.hasCastToString(ob1, ob2, ob3, ob4);
        assertFalse(result);
    }

    @Test
    public void testHasCastToStringShouldReturnFalseIfOneOfObjectsCanNotCastToString() {
        Object ob1 = "string";
        Object ob3 = "string";
        Object ob4 = "string";
        Object ob2 = new Object();
        Object ob5 = "string";
        boolean result = Util.hasCastToString(ob1, ob2, ob3, ob4, ob5);
        assertFalse(result);
    }

    @Test
    public void testHasCastToStringShouldReturnFalseIfOneOfObjectsIsNull() {
        String str1 = "str";
        String strNull = null;
        String str2 = "str";
        String str3 = "str";

        boolean result = Util.hasCastToString(str1, str2, strNull, str3);
        assertFalse(result);
    }

    @Test
    public void testCleanSessionShouldDeleteAllAttribute() {
        String attr1 = "attr1";
        String attr2 = "attr2";
        String attr3 = "attr3";
        String attr4 = "attr4";
        String attr5 = "attr5";
        HttpSession session = Mockito.mock(HttpSession.class);
        Util.cleanSession(session, attr1, attr2, attr3, attr5);
        Mockito.verify(session, Mockito.times(1)).removeAttribute(attr1);
        Mockito.verify(session, Mockito.times(1)).removeAttribute(attr2);
        Mockito.verify(session, Mockito.times(1)).removeAttribute(attr3);
        Mockito.verify(session, Mockito.times(1)).removeAttribute(attr5);
        Mockito.verify(session, Mockito.never()).removeAttribute(attr4);
        Mockito.verify(session, Mockito.never()).invalidate();
    }

    @Test
    public void testIsTodayShouldReturnFalseWhenDateMoreThenFourteenDay() {
        Date date = Date.valueOf(new Date(System.currentTimeMillis()).toString());
        date = new Date(date.getTime() + 15 * 24 * 60 * 60_000);
        boolean result = Util.isToday(date);
        assertFalse(result);
    }

    @Test
    public void testIsTodayShouldReturnFalseWhenDateLessThenToday() {
        Date date = Date.valueOf(new Date(System.currentTimeMillis()).toString());
        date = new Date(date.getTime() - 24 * 60 * 60_000);
        boolean result = Util.isToday(date);
        assertFalse(result);
    }

    @Test
    public void testIsTodayShouldReturnTrue() {
        Date date = Date.valueOf(new Date(System.currentTimeMillis()).toString());
        date = new Date(date.getTime() + 3 * 24 * 60 * 60_000);
        boolean result = Util.isToday(date);
        assertTrue(result);
    }

    @Test
    public void testCreateEmail() {
        User user = new User();
        user.setLogin("login");
        String expectedSubject = "subject";
        String expectedText = "text";
        Date dateLess = new Date(System.currentTimeMillis() - 3 * 60 * 1000);
        Date dateMore = new Date(System.currentTimeMillis() + 3 * 60 * 1000);
        Email email = Email.createEmail(user, expectedSubject, expectedText);
        assertEquals(user.getLogin(), email.getEmail());
        assertEquals(expectedSubject, email.getSubject());
        assertEquals(expectedText, email.getText());
        assertTrue(email.getDate().getTime() < dateMore.getTime());
        assertTrue(email.getDate().getTime() > dateLess.getTime());
    }

}
