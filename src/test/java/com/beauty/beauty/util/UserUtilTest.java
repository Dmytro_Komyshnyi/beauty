package com.beauty.beauty.util;

import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.db.entity.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpSession;
import java.lang.reflect.Field;

public class UserUtilTest {

    @Test
    public void testGetRoleShouldReturnAdmin() {
        Role role = UserUtil.getRole(1);
        Assertions.assertEquals(Role.Admin, role);
    }

    @Test
    public void testGetRoleShouldReturnMaster() {
        Role role = UserUtil.getRole(2);
        Assertions.assertEquals(Role.Client, role);
    }

    @Test
    public void testGetRoleShouldReturnWorker() {
        Role role = UserUtil.getRole(3);
        Assertions.assertEquals(Role.Master, role);
    }

    @Test()
    void testGetRoleShouldThrowIllegalArgumentExceptionIfRoleDoesNotExist() {
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> UserUtil.getRole(4));
        String expectedMessage = "Role with this id does not exist";
        String actual = exception.getMessage();
        Assertions.assertTrue(actual.contains(expectedMessage));
    }

    @Test
    void testGetRoleShouldReturnUnregisteredRole() {
        Role role = UserUtil.getRole(-1);
        Assertions.assertEquals(Role.Unregistered, role);
    }

    @Test
    public void testHasValidPasswordShouldReturnFalseIfPasswordShorterThen8() {
        String password = "a44pVa.";
        boolean result = UserUtil.hasValidPassword(password);
        Assertions.assertFalse(result);
    }

    @Test
    public void testHasValidPasswordShouldReturnFalseIfPasswordLongerThen40() {
        String password = "TkiPKj834zqX9ZH5yQvgmeSGZK9B25cLelqOuoFmL";
        boolean result = UserUtil.hasValidPassword(password);
        Assertions.assertFalse(result);
    }

    @Test
    public void testHasValidPasswordShouldReturnTrueIfPassword40() {
        String password = "TkiPKj834zqX9ZH5yQvgmeSGZK9B25cLelqOuoFm";
        boolean result = UserUtil.hasValidPassword(password);
        Assertions.assertTrue(result);
    }

    @Test
    public void testHasValidPasswordShouldReturnTrueIfPasswordContainsAtSign() {
        String password = "@T@@@@@@m@";
        boolean result = UserUtil.hasValidPassword(password);
        Assertions.assertTrue(result);
    }

    @Test
    public void testHasValidPasswordShouldReturnFalseIfPasswordContainsSQL() {
        String password = "Select from users where users id = 1";
        boolean result = UserUtil.hasValidPassword(password);
        Assertions.assertFalse(result);
    }

    @Test
    public void testHasValidPasswordShouldReturnFalseIfPasswordContainsScript() {
        String password = "<script>alert</script>";
        boolean result = UserUtil.hasValidPassword(password);
        Assertions.assertFalse(result);
    }


    @Test
    public void testHasValidPhoneShouldReturnFalseIfPhoneNumberLongerThenTwelve() {
        String phone = "0968988890123";
        boolean result = UserUtil.hasValidPhone(phone);
        Assertions.assertFalse(result);
    }

    @Test
    public void testHasValidPhoneShouldReturnFalseIfPhoneNumberShorterThenTen() {
        String phone = "096898889";
        boolean result = UserUtil.hasValidPhone(phone);
        Assertions.assertFalse(result);
    }

    @Test
    public void testHasValidPhoneShouldReturnTrueIfPhoneIsValid() {
        String phone = "0968988890";
        boolean result = UserUtil.hasValidPhone(phone);
        Assertions.assertTrue(result);
    }

    @Test
    public void testIsValidLoginShouldReturnFalseIfWithoutSignAt() {
        String email = "namesecondgmail.com";
        boolean result = UserUtil.hasValidLogin(email);
        Assertions.assertFalse(result);
    }

    @Test
    public void testIsValidLoginShouldReturnFalseIfWithoutDot() {
        String email = "namesecond@gmailcom";
        boolean result = UserUtil.hasValidLogin(email);
        Assertions.assertFalse(result);
    }


    @Test
    public void testIsValidLoginShouldReturnTrue() {
        String email = "namesecond@gmail.com";
        boolean result = UserUtil.hasValidLogin(email);
        Assertions.assertTrue(result);
    }


    @Test
    public void testGetUserFromSessionShouldReturnExistUserIfSessionAlreadyContainsUser() throws NoSuchFieldException, IllegalAccessException {
        Field staticField = UserUtil.class.getDeclaredField("ATTR_USER");
        staticField.setAccessible(true);
        String attributeUser = (String) staticField.get(null);
        HttpSession session = Mockito.mock(HttpSession.class);
        User user = new User("login", "password",
                Role.Client, "name", "0638179488");
        Mockito.when(session.getAttribute(attributeUser)).thenReturn(user);
        User result = UserUtil.getUserFromSession(session);
        Assertions.assertEquals(user, result);
    }

    @Test
    public void testGetUserFromSessionShouldReturnUnregisteredUserIfSessionNotContainsUser() throws NoSuchFieldException, IllegalAccessException {
        Field staticField = UserUtil.class.getDeclaredField("ATTR_USER");
        staticField.setAccessible(true);
        String attributeUser = (String) staticField.get(null);
        HttpSession session = Mockito.mock(HttpSession.class);
        Mockito.when(session.getAttribute(attributeUser)).thenReturn(null);
        User result = UserUtil.getUserFromSession(session);
        Assertions.assertEquals(Role.Unregistered, result.getRole());
    }

    @Test
    public void testGetUserFromSessionShouldReturnUnregisteredUserIfSessionContainsOtherObject() throws NoSuchFieldException, IllegalAccessException {
        Field staticField = UserUtil.class.getDeclaredField("ATTR_USER");
        staticField.setAccessible(true);
        String attributeUser = (String) staticField.get(null);
        HttpSession session = Mockito.mock(HttpSession.class);
        Mockito.when(session.getAttribute(attributeUser)).thenReturn(new Object());
        User result = UserUtil.getUserFromSession(session);
        Assertions.assertEquals(Role.Unregistered, result.getRole());
    }
}
