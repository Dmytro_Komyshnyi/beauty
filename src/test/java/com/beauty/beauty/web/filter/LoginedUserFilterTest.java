package com.beauty.beauty.web.filter;

import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.db.entity.User;
import org.junit.jupiter.api.Test;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;


class LoginedUserFilterTest {

    @Test
    public void testDoFilterShouldDoNothingWhenUserUnregistered() throws IOException, ServletException {
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpSession session = mock(HttpSession.class);
        ServletResponse resp = mock(ServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);
        User user = new User();
        user.setRole(Role.Unregistered);

        when(req.getSession()).thenReturn(session);
        when(session.getAttribute("current_user")).thenReturn(user);
        String expectedMessage = "successTest";
        doAnswer((a) -> {throw new Exception(expectedMessage);}).when(filterChain).doFilter(req, resp);
        Filter filter = new LoginedUserFilter();
        Exception e = assertThrows(Exception.class,() -> filter.doFilter(req, resp, filterChain));
        assertTrue(e.getMessage().contains(expectedMessage));
    }

    @Test
    public void testDoFilterShouldForwardForLogoutWhenUserRegistered() throws IOException, ServletException {
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpSession session = mock(HttpSession.class);
        ServletResponse resp = mock(ServletResponse.class);
        FilterChain filterChain = mock(FilterChain.class);
        RequestDispatcher dispatcher = mock(RequestDispatcher.class);
        User user = new User();
        user.setRole(Role.Client);

        when(req.getRequestDispatcher("/logout")).thenReturn(dispatcher);
        when(req.getSession()).thenReturn(session);
        when(session.getAttribute("current_user")).thenReturn(user);
        String expectedMessage = "successTest";
        doAnswer((a) -> {throw new Exception(expectedMessage);}).when(dispatcher).forward(req, resp);
        Filter filter = new LoginedUserFilter();
        Exception e = assertThrows(Exception.class,() -> filter.doFilter(req, resp, filterChain));
        assertTrue(e.getMessage().contains(expectedMessage));
    }

}