package com.beauty.beauty.db.dao.mySQL;

import com.beauty.beauty.db.dao.ConnectionBuilder;
import com.beauty.beauty.db.dao.ServiceDaoException;
import com.beauty.beauty.db.entity.Service;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MySQLServiceTest {

    private static MySQLService sqlService;
    private static String connectionUrl =
            "jdbc:mysql://localhost:3306/beauty_salon_test?sslMode=DISABLED&serverTimezone=UTC&user=root&password=root";

    @BeforeAll
    public static void init() {
        sqlService = MySQLService.getInstance();
        sqlService.setConnectionBuilder(() -> DriverManager.getConnection(connectionUrl));
    }

    @BeforeEach
    public void createTables() throws SQLException {
        try (Connection con = DriverManager.getConnection(connectionUrl);
             Statement stmtCreateTable = con.createStatement();
             Statement stmtInsert1 = con.createStatement();
             Statement stmtInsert2 = con.createStatement();
             Statement stmtInsert3 = con.createStatement())
        {
            stmtCreateTable.execute("CREATE TABLE IF NOT EXISTS `services` (\n" +
                    "`id` INT NOT NULL AUTO_INCREMENT,\n" +
                    "`name` VARCHAR(45) NOT NULL,\n" +
                    "`price` int NOT NULL,\n" +
                    "`estimated_time` int NOT NULL,\n" +
                    "PRIMARY KEY (`id`),\n" +
                    "UNIQUE INDEX `name_UNIQUE` (`name`));\n");
            stmtInsert1.execute(
                    "insert into `services` \n" +
                    "(name, estimated_time, price)\n" +
                    "values ('haircut', 30, 10000);\n");
            stmtInsert2.execute(
                    "insert into `services` \n" +
                    "(name, estimated_time, price)\n" +
                    "values ('nail', 30, 20000);\n");
            stmtInsert3.execute(
                    "insert into `services` \n" +
                    "(name, estimated_time, price)\n" +
                    "values ('peeling', 180, 50000);"
            );
        }
    }

    @AfterEach
    public void dropTables() throws SQLException {
        try (Connection con = DriverManager.getConnection(connectionUrl);
             Statement stmt = con.createStatement())
        {
            stmt.execute("drop table if exists services;");
        }
    }

    @Test
    public void getEstimatedTime() throws ServiceDaoException {
        int result = sqlService.getEstimatedTime("haircut");
        int expected = 30;
        assertEquals(expected, result);
    }

    @Test
    public void testGetEstimatedTimeShouldCatchSQLExceptionAndThrowServiceDaoException()
            throws NoSuchFieldException, IllegalAccessException, SQLException {
        Field field = MySQLService.class.getDeclaredField("connectionBuilder");
        field.setAccessible(true);
        ConnectionBuilder con = (ConnectionBuilder) field.get(sqlService);
        ConnectionBuilder mockCon = mock(ConnectionBuilder.class);
        when(mockCon.getConnection()).thenThrow(new SQLException());
        sqlService.setConnectionBuilder(mockCon);
        String name = "nail";
        Exception e = assertThrows(ServiceDaoException.class, () -> sqlService.getEstimatedTime(name));
        String resultMessage = e.getMessage();
        String expectedMessage = "can not get estimated time service ["+name+"]";
        sqlService.setConnectionBuilder(con);
        assertTrue(resultMessage.contains(expectedMessage));
    }

    @Test
    public void getServiceShouldCatchSQLExceptionAndThrowServiceDaoException() throws SQLException, NoSuchFieldException, IllegalAccessException {
        Field field = MySQLService.class.getDeclaredField("connectionBuilder");
        field.setAccessible(true);
        ConnectionBuilder con = (ConnectionBuilder) field.get(sqlService);
        ConnectionBuilder mockCon = mock(ConnectionBuilder.class);
        when(mockCon.getConnection()).thenThrow(new SQLException());
        sqlService.setConnectionBuilder(mockCon);
        String name = "nail";
        Exception e = assertThrows(ServiceDaoException.class, () -> sqlService.getService(name));
        String resultMessage = e.getMessage();
        String expectedMessage = "can not get service["+name+"]";
        sqlService.setConnectionBuilder(con);
        assertTrue(resultMessage.contains(expectedMessage));
    }

    @Test
    public void getAllServiceShouldCatchSQLExceptionAndThrowServiceDaoException() throws SQLException, NoSuchFieldException, IllegalAccessException {
        Field field = MySQLService.class.getDeclaredField("connectionBuilder");
        field.setAccessible(true);
        ConnectionBuilder con = (ConnectionBuilder) field.get(sqlService);
        ConnectionBuilder mockCon = mock(ConnectionBuilder.class);
        when(mockCon.getConnection()).thenThrow(new SQLException());
        sqlService.setConnectionBuilder(mockCon);
        Exception e = assertThrows(ServiceDaoException.class, () -> sqlService.getAllService());
        String resultMessage = e.getMessage();
        String expectedMessage = "can not get services";
        sqlService.setConnectionBuilder(con);
        assertTrue(resultMessage.contains(expectedMessage));
    }

    @Test
    public void getEstimatedTimeShouldThrowServiceDaoExceptionIfServiceIsNotExist() {
        String name = "illegal name";
        Exception e = assertThrows(ServiceDaoException.class, () -> sqlService.getEstimatedTime(name));
        String expectedMessage = "can not get estimated time service ["+name+"]";
        assertTrue(e.getMessage().contains(expectedMessage));
    }

    @Test
    public void getServiceShouldThrowServiceDaoExceptionIfServiceIsNotExist() {
        String name = "illegal name";
        Exception e = assertThrows(ServiceDaoException.class, () -> sqlService.getService(name));
        String expectedMessage = "can not get service["+name+"]";
        assertTrue(e.getMessage().contains(expectedMessage));
    }

    @Test
    public void testGetService() throws ServiceDaoException {
        String name = "nail";
        Service expected = new Service();
        expected.setEstimatedTime(30);
        expected.setPrice(20000);
        expected.setName(name);
        expected.setId(2);
        Service result = sqlService.getService(name);
        assertEquals(expected.getName(), result.getName());
        assertEquals(expected.getId(), result.getId());
        assertEquals(expected.getPrice(), result.getPrice());
        assertEquals(expected.getEstimatedTime(), result.getEstimatedTime());
    }

    @Test
    public void testGetAllService() throws ServiceDaoException {
        String expected = "Service{id=1, price=10000, name='haircut', estimatedTime=30}\n" +
                "Service{id=2, price=20000, name='nail', estimatedTime=30}\n" +
                "Service{id=3, price=50000, name='peeling', estimatedTime=180}\n";
        StringBuilder sb = new StringBuilder();
        List<Service> services = sqlService.getAllService();
        for (Service s : services) {
            sb.append(s.toString()).append("\n");
        }
        assertEquals(expected, sb.toString());
    }

    @Test
    public void testCloseShouldCatchException() throws Exception {
        Method method = MySQLService.class.getDeclaredMethod("close", AutoCloseable.class);
        method.setAccessible(true);
        AutoCloseable closeable = mock(AutoCloseable.class);
        doAnswer((a) -> {throw new Exception();}).when(closeable).close();
        method.invoke(sqlService, closeable);
    }

}