package com.beauty.beauty.db.dao.mySQL;

import com.beauty.beauty.db.bean.TimeSlotBean;
import com.beauty.beauty.db.dao.ConnectionBuilder;
import com.beauty.beauty.db.dao.UserDaoException;
import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.db.entity.User;
import com.beauty.beauty.db.entity.Worker;
import com.beauty.beauty.util.UserUtil;
import org.junit.jupiter.api.*;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MySQLUserTest {

    private static MySQLUser sqlUser;
    private static final String connectionUrl =
            "jdbc:mysql://localhost:3306/beauty_salon_test?allowMultiQueries=true&sslMode=DISABLED&serverTimezone=UTC&user=root&password=root";

    @BeforeAll
    public static void init() {
        sqlUser = MySQLUser.getInstance();
        sqlUser.setConnectionBuilder(() -> DriverManager.getConnection(connectionUrl));
    }

    @BeforeEach
    public void createTables() throws SQLException {
        try (Connection con = DriverManager.getConnection(connectionUrl);
             Statement stmt = con.createStatement())
        {
            stmt.execute(
                    "CREATE TABLE IF NOT EXISTS `roles` (\n" +
                            "  `id` INT NOT NULL AUTO_INCREMENT,\n" +
                            "  `name` VARCHAR(40) NULL DEFAULT NULL,\n" +
                            "  PRIMARY KEY (`id`));\n"+
                            "CREATE TABLE IF NOT EXISTS `users` (\n" +
                            "  `id` INT NOT NULL AUTO_INCREMENT,\n" +
                            "  `name` VARCHAR(150) NOT NULL,\n" +
                            "  `login` VARCHAR(40) NOT NULL,\n" +
                            "  `password` VARCHAR(32) NOT NULL,\n" +
                            "  `phone` VARCHAR(15) NOT NULL,\n" +
                            "  `role_id` INT NOT NULL,\n" +
                            "  PRIMARY KEY (`id`),\n" +
                            "  UNIQUE INDEX `login_UNIQUE` (`login`),\n" +
                            "  CONSTRAINT `fk_users_roles`\n" +
                            "    FOREIGN KEY (`role_id`)\n" +
                            "    REFERENCES `roles` (`id`)\n" +
                            "    ON DELETE NO ACTION\n" +
                            "    ON UPDATE NO ACTION);\n" +
                            "CREATE TABLE IF NOT EXISTS `services` (\n" +
                            "  `id` INT NOT NULL AUTO_INCREMENT,\n" +
                            "  `name` VARCHAR(45) NOT NULL,\n" +
                            "  `price` int NOT NULL,\n" +
                            "  `estimated_time` int NOT NULL,\n" +
                            "  PRIMARY KEY (`id`),\n" +
                            "  UNIQUE INDEX `name_UNIQUE` (`name`));\n" +
                            "CREATE TABLE IF NOT EXISTS `days` (\n" +
                            "`name` VARCHAR(15) NOT NULL,\n" +
                            "PRIMARY KEY (`name`));\n"+
                            "CREATE TABLE IF NOT EXISTS `schedule` (\n" +
                            "  `id` INT NOT NULL AUTO_INCREMENT,\n" +
                            "  `time_start` TIME NOT NULL,\n" +
                            "  `time_end` TIME NOT NULL,\n" +
                            "  `day` VARCHAR(15) NOT NULL,\n" +
                            "  `open` BOOLEAN NOT NULL,\n" +
                            "  PRIMARY KEY (`id`),\n" +
                            "  CONSTRAINT `fk_Shedual_days`\n" +
                            "    FOREIGN KEY (`day`)\n" +
                            "    REFERENCES `days` (`name`)\n" +
                            "    ON DELETE NO ACTION\n" +
                            "    ON UPDATE NO ACTION);\n"+
                            "CREATE TABLE IF NOT EXISTS `worker_schedule` (\n" +
                            "  `id` INT NOT NULL AUTO_INCREMENT,\n" +
                            "  `time_start` TIME NOT NULL,\n" +
                            "  `time_end` TIME NOT NULL,\n" +
                            "  `name` varchar(20),\n" +
                            "  PRIMARY KEY (`id`));\n" +
                            "CREATE TABLE IF NOT EXISTS `work_date` (\n" +
                            "  `worker_id` INT NOT NULL AUTO_INCREMENT,\n" +
                            "  `worker_schedule_id` INT NOT NULL,\n" +
                            "  `date` DATE NOT NULL,\n" +
                            "  PRIMARY KEY (`worker_id`, `worker_schedule_id`, `date`),\n" +
                            "  CONSTRAINT `fk_work_date_users`\n" +
                            "    FOREIGN KEY (`worker_id`)\n" +
                            "    REFERENCES `users` (`id`)\n" +
                            "    ON DELETE NO ACTION\n" +
                            "    ON UPDATE NO ACTION,\n" +
                            "  CONSTRAINT `fk_work_date_worker_schedule`\n" +
                            "    FOREIGN KEY (`worker_schedule_id`)\n" +
                            "    REFERENCES `worker_schedule` (`id`)\n" +
                            "    ON DELETE NO ACTION\n" +
                            "    ON UPDATE NO ACTION);\n"+
                            "CREATE TABLE IF NOT EXISTS `roles_has_services` (\n" +
                            "  `roles_id` INT NOT NULL,\n" +
                            "  `services_id` INT NOT NULL,\n" +
                            "  PRIMARY KEY (`roles_id`, `services_id`),\n" +
                            "  CONSTRAINT `fk_roles_has_services_roles`\n" +
                            "    FOREIGN KEY (`roles_id`)\n" +
                            "    REFERENCES `roles` (`id`)\n" +
                            "    ON DELETE NO ACTION\n" +
                            "    ON UPDATE NO ACTION,\n" +
                            "  CONSTRAINT `fk_roles_has_services_services`\n" +
                            "    FOREIGN KEY (`services_id`)\n" +
                            "    REFERENCES `services` (`id`)\n" +
                            "    ON DELETE NO ACTION\n" +
                            "    ON UPDATE NO ACTION);\n"+
                            "CREATE TABLE IF NOT EXISTS `appointment` (\n" +
                            "  `id` INT NOT NULL AUTO_INCREMENT,\n" +
                            "  `date` DATE NOT NULL,\n" +
                            "  `time_start` TIME NOT NULL,\n" +
                            "  `time_end` TIME NOT NULL,\n" +
                            "  `price` int NOT NULL check(`price` > 0),\n" +
                            "  `accepted` BOOLEAN NOT NULL default false,\n" +
                            "  `completed` BOOLEAN NOT NULL default false,\n" +
                            "  `canceled` BOOLEAN NOT NULL default false,\n" +
                            "  `services_id` INT NOT NULL,\n" +
                            "  `users_id` INT NOT NULL,\n" +
                            "  `worker_id` INT NULL,\n" +
                            "  PRIMARY KEY (`id`),\n" +
                            "  CONSTRAINT `fk_appointment_services`\n" +
                            "    FOREIGN KEY (`services_id`)\n" +
                            "    REFERENCES `services` (`id`)\n" +
                            "    ON DELETE NO ACTION\n" +
                            "    ON UPDATE NO ACTION,\n" +
                            "  CONSTRAINT `fk_appointment_users`\n" +
                            "    FOREIGN KEY (`users_id`)\n" +
                            "    REFERENCES `users` (`id`)\n" +
                            "    ON DELETE NO ACTION\n" +
                            "    ON UPDATE NO ACTION,\n" +
                            "  CONSTRAINT `fk_appointment_users2`\n" +
                            "    FOREIGN KEY (`worker_id`)\n" +
                            "    REFERENCES `users` (`id`)\n" +
                            "    ON DELETE NO ACTION\n" +
                            "    ON UPDATE NO ACTION,\n" +
                            "CONSTRAINT time_less_end CHECK (time_start < time_end));\n"+
                            "CREATE TABLE IF NOT EXISTS `evaluations` (\n" +
                            "`appointment_id` INT NOT NULL,\n" +
                            "`evaluation` INT NOT NULL check(evaluation >=0 and evaluation <= 10),\n" +
                            "PRIMARY KEY (`appointment_id`),\n" +
                            "CONSTRAINT `fk_appointment_id`\n" +
                            "FOREIGN KEY (`appointment_id`)\n" +
                            "REFERENCES `appointment` (`id`)\n" +
                            "ON DELETE no action\n" +
                            "ON UPDATE no action\n" +
                            ");\n"+
                            "CREATE TABLE IF NOT EXISTS `services` (\n" +
                            "`id` INT NOT NULL AUTO_INCREMENT,\n" +
                            "`name` VARCHAR(45) NOT NULL,\n" +
                            "`price` int NOT NULL,\n" +
                            "`estimated_time` int NOT NULL,\n" +
                            "PRIMARY KEY (`id`),\n" +
                            "UNIQUE INDEX `name_UNIQUE` (`name`));\n"+
                            "insert into `roles` \n" +
                            "(name)\n" +
                            "values ('admin');\n" +
                            "insert into `roles` \n" +
                            "(name)\n" +
                            "values ('client');\n" +
                            "insert into `roles` \n" +
                            "(name)\n" +
                            "values ('master');\n"+
                            "insert into `services` \n" +
                            "(name, estimated_time, price)\n" +
                            "values ('haircut', 30, 10000);\n"+
                            "insert into `services` \n" +
                            "(name, estimated_time, price)\n" +
                            "values ('nail', 30, 20000);\n"+
                            "insert into `services` \n" +
                            "(name, estimated_time, price)\n" +
                            "values ('peeling', 180, 50000);" +
                            "insert into users (login, password, name, role_id, phone) VALUES('dimakomishny@gmail.com', md5('password'), 'user1', 1, '2');\n"+
                            "insert into users (login, password, name, role_id, phone) VALUES('fakeUser', md5('password'), 'fakeUser', 2, '000');\n" +
                            "insert into users (login, password, name, role_id, phone) VALUES('user2', md5('pas'), 'user2', 2, '2');\n" +
                            "insert into users (login, password, name, role_id, phone) VALUES('user3', md5('pas'), 'user3', 2, '3');\n" +
                            "insert into users (login, password, name, role_id, phone) VALUES('user4', md5('pas'), 'user4', 2, '4');\n" +
                            "insert into users (login, password, name, role_id, phone) VALUES('user5', md5('pas'), 'user4', 2, '5');\n" +

                            "insert into users (login, password, name, role_id, phone) VALUES('master6', md5('pas'), 'master5', 3, '6');\n" +
                            "insert into users (login, password, name, role_id, phone) VALUES('master7@gmail.com', md5('password'), 'master6', 3, '7');\n" +
                            "insert into users (login, password, name, role_id, phone) VALUES('saster6', md5('pas'), 'aster5', 3, '18');\n" +

                            "insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-2-24', '15:00:00', '15:30:00', 10000, 1, 3, 6);\n" +
                            "insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-2-24', '15:00:00', '15:30:00', 10000, 1, 3, 6);\n" +
                            "insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-2-24', '12:00:00', '13:30:00', 10000, 1, 3, 6);\n" +
                            "insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-2-24', '8:30:00', '10:00:00', 10000, 1, 3, 7);\n" +
                            "insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-2-24', '13:00:00', '13:50:00', 10000, 1, 3, 7);\n" +
                            "insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-2-24', '14:00:00', '14:30:00', 10000, 1, 3, 7);\n" +
                            "insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-2-24', '14:00:00', '14:30:00', 10000, 1, 3, 8);\n" +

                            "insert into worker_schedule (time_start, time_end, `name`) VALUES('9:00:00', '18:00:00', 'Weekday');\n" +
                            "insert into worker_schedule (time_start, time_end, `name`) VALUES('10:00:00', '16:00:00', 'Weekend');\n" +

                            "insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('6', '1', '2021-2-24');\n" +
                            "insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('6', '1', '2021-2-25');\n" +
                            "insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('6', '1', '2021-2-26');\n" +
                            "insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('7', '1', '2021-2-24');\n" +
                            "insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('7', '1', '2021-2-25');\n" +
                            "insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('7', '1', '2021-2-26');\n" +
                            "insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('8', '1', '2021-2-24');\n" +

                            "insert into evaluations (appointment_id, evaluation) VALUES(1, 5);\n" +
                            "insert into evaluations (appointment_id, evaluation) VALUES(2, 6);\n" +
                            "insert into evaluations (appointment_id, evaluation) VALUES(5, 6);\n" +
                            "insert into evaluations (appointment_id, evaluation) VALUES(3, 10);\n" +
                            "insert into evaluations (appointment_id, evaluation) VALUES(7, 1);"
            );
        }
    }

    @AfterEach
    public void drop() throws SQLException {
        try (Connection con = DriverManager.getConnection(connectionUrl);
             Statement stmt = con.createStatement();
        ) {
            stmt.execute("drop table if exists work_date;\n" +
                    "drop table if exists evaluations;\n" +
                    "drop table if exists appointment;\n" +
                    "drop table if exists roles_has_services;\n" +
                    "drop table if exists users;\n" +
                    "drop table if exists roles;\n" +
                    "drop table if exists services;\n" +
                    "drop table if exists `schedule`;\n" +
                    "drop table if exists worker_schedule;\n" +
                    "drop table if exists days;\n" +
                    "drop view if exists clients_view; \n" +
                    "drop view if exists admins_view; \n" +
                    "drop view if exists masters_view; "
            );
        }
    }

    @Test
    public void testClose() throws Exception {
        Method method = MySQLUser.class.getDeclaredMethod("close", AutoCloseable.class);
        method.setAccessible(true);
        AutoCloseable closeable = mock(AutoCloseable.class);
        doAnswer((a) -> {throw new Exception();}).when(closeable).close();
        method.invoke(sqlUser, closeable);
    }

    @Test
    public void testGetUser() throws UserDaoException {
        User expected = new User();
        expected.setId(3);
        expected.setName("user2");
        expected.setPhone("2");
        expected.setRole(UserUtil.getRole(2));
        expected.setLogin("user2");
        User result = sqlUser.getUser(3);
        assertEquals(expected, result);
    }

    @Test
    public void testGetUserShouldThrowUserDaoException()
            throws SQLException, NoSuchFieldException, IllegalAccessException {
        Field field = MySQLUser.class.getDeclaredField("connectionBuilder");
        field.setAccessible(true);
        ConnectionBuilder oldConnBuilder = (ConnectionBuilder) field.get(sqlUser);
        ConnectionBuilder mockConnBuilder = mock(ConnectionBuilder.class);
        field.set(sqlUser, mockConnBuilder);
        when(mockConnBuilder.getConnection()).thenThrow(new SQLException("getConnection"));
        int id = 3;
        Exception e = assertThrows(UserDaoException.class, () -> sqlUser.getUser(id));
        String message = e.getMessage();
        String expectedMessage = "can not get user id["+id+"]";
        assertTrue(message.contains(expectedMessage));
        field.set(sqlUser, oldConnBuilder);
    }

    @Test
    public void testGetUserShouldThrowUserDaoExceptionIfNotFoundUser() {
        int id = 0;
        Exception e = assertThrows(UserDaoException.class, () -> sqlUser.getUser(id));
        String message = e.getMessage();
        String expectedMessage = "can not find user id["+id+"]";
        assertTrue(message.contains(expectedMessage));
    }

    @Test
    public void testGetUsersByRole() throws UserDaoException {
        List<User> list = sqlUser.getUsersByRole(Role.Client);
        String result = list.toString();
        String expected = "[User{id=2, login='fakeUser', password='null', role=Client," +
                " name='fakeUser', phone='000'}, User{id=3, login='user2', " +
                "password='null', role=Client, name='user2', phone='2'}, " +
                "User{id=4, login='user3', password='null', role=Client, " +
                "name='user3', phone='3'}, User{id=5, login='user4', password='null'," +
                " role=Client, name='user4', phone='4'}, User{id=6, login='user5'," +
                " password='null', role=Client, name='user4', phone='5'}]";
        assertEquals(expected, result);
    }

    @Test
    public void testGetMasters() throws UserDaoException {
        List<Worker> list = sqlUser.getMasters();
        String expected = "[User{id=9, login='saster6', password='null', " +
                "role=Master, name='aster5', phone='18'}, User{id=7, login='master6', " +
                "password='null', role=Master, name='master5', phone='6'}, User{id=8, login='master7@gmail.com', " +
                "password='null', role=Master, name='master6', phone='7'}]";
        String result = list.toString();
        assertEquals(expected, result);
    }

    @Test
    public void testGetUserShouldReturnUserIfPasswordAndLoginCorrect() throws UserDaoException {
        User user = sqlUser.getUser("fakeUser", "password");
        String expected = "User{id=2, login='fakeUser', password='null', " +
                "role=Client, name='fakeUser', phone='000'}";
        String result = user.toString();
        assertEquals(expected, result);
    }

    @Test
    public void testGetUserShouldThrowUserDaoExceptionIfWrongPassword() throws UserDaoException {
        Exception e = assertThrows(UserDaoException.class, () -> sqlUser.getUser("fakeUser", "wrongPassword"));
        String message = e.getMessage();
        String expectedMessage = "can not get user";
        assertTrue(message.contains(expectedMessage));
    }

    @Test
    public void testGetUserByLoginAndPasswordShouldThrowUserDaoException() throws NoSuchFieldException, SQLException, IllegalAccessException {
        Field field = MySQLUser.class.getDeclaredField("connectionBuilder");
        field.setAccessible(true);
        ConnectionBuilder oldConnBuilder = (ConnectionBuilder) field.get(sqlUser);
        ConnectionBuilder mockConnBuilder = mock(ConnectionBuilder.class);
        field.set(sqlUser, mockConnBuilder);
        when(mockConnBuilder.getConnection()).thenThrow(new SQLException("getConnection"));
        Exception e = assertThrows(UserDaoException.class, () -> sqlUser.getUser("fakeUser", "wrongPassword"));
        String message = e.getMessage();
        String expectedMessage = "can not get user";
        assertTrue(message.contains(expectedMessage));
        field.set(sqlUser, oldConnBuilder);
    }

    @Test
    public void testGetUsersByRoleShouldThrowUserDaoException() throws NoSuchFieldException, SQLException, IllegalAccessException {
        Field field = MySQLUser.class.getDeclaredField("connectionBuilder");
        field.setAccessible(true);
        ConnectionBuilder oldConnBuilder = (ConnectionBuilder) field.get(sqlUser);
        ConnectionBuilder mockConnBuilder = mock(ConnectionBuilder.class);
        field.set(sqlUser, mockConnBuilder);
        when(mockConnBuilder.getConnection()).thenThrow(new SQLException("getConnection"));
        Exception e = assertThrows(UserDaoException.class, () -> sqlUser.getUsersByRole(Role.Client));
        String message = e.getMessage();
        String expectedMessage = "can not get user";
        assertTrue(message.contains(expectedMessage));
        field.set(sqlUser, oldConnBuilder);
    }

    @Test
    public void testGetWorkTimeByDate() throws UserDaoException {
        int id = 6;
        Date date = Date.valueOf("2021-2-24");
        TimeSlotBean result = sqlUser.getWorkTimeByDate(id, date);
        TimeSlotBean expected = new TimeSlotBean(Time.valueOf("9:00:00"), Time.valueOf("18:00:00"));
        assertEquals(result, expected);
    }

    @Test
    public void testGetWorkTimeByDateShouldReturnNullIfUserDoNotExist() throws UserDaoException {
        int id = 0;
        Date date = Date.valueOf("2021-2-24");
        TimeSlotBean result = sqlUser.getWorkTimeByDate(id, date);
        assertNull(result);
    }

    @Test
    public void testGetWorkTimeByDateShouldReturnNullIfUserDoesNotHaveWorkTime() throws UserDaoException {
        int id = 6;
        Date date = Date.valueOf("1999-2-20");
        TimeSlotBean result = sqlUser.getWorkTimeByDate(id, date);
        assertNull(result);
    }

    @Test
    public void testGetWorkTimeByDateShouldThrowUserDaoException() throws NoSuchFieldException, SQLException, IllegalAccessException {
        Field field = MySQLUser.class.getDeclaredField("connectionBuilder");
        field.setAccessible(true);
        ConnectionBuilder oldConnBuilder = (ConnectionBuilder) field.get(sqlUser);
        ConnectionBuilder mockConnBuilder = mock(ConnectionBuilder.class);
        field.set(sqlUser, mockConnBuilder);
        when(mockConnBuilder.getConnection()).thenThrow(new SQLException("getConnection"));
        int id = 6;
        Date date = Date.valueOf("2021-2-24");
        Exception e = assertThrows(UserDaoException.class, () -> sqlUser.getWorkTimeByDate(id, date));
        String message = e.getMessage();
        String expectedMessage = "can not get worker time workerId["+id+"] date["+date+"]";
        assertTrue(message.contains(expectedMessage));
        field.set(sqlUser, oldConnBuilder);
    }

    @Test
    public void testGetMastersShouldThrowUserDaoException() throws NoSuchFieldException, SQLException, IllegalAccessException {
        Field field = MySQLUser.class.getDeclaredField("connectionBuilder");
        field.setAccessible(true);
        ConnectionBuilder oldConnBuilder = (ConnectionBuilder) field.get(sqlUser);
        ConnectionBuilder mockConnBuilder = mock(ConnectionBuilder.class);
        field.set(sqlUser, mockConnBuilder);
        when(mockConnBuilder.getConnection()).thenThrow(new SQLException("getConnection"));
        Exception e = assertThrows(UserDaoException.class, () -> sqlUser.getMasters());
        String message = e.getMessage();
        String expectedMessage = "can not get masters";
        assertTrue(message.contains(expectedMessage));
        field.set(sqlUser, oldConnBuilder);
    }

    @Test
    void addWorkerDate() throws UserDaoException {
        sqlUser.addWorkerDate(7, Date.valueOf("2021-3-14"));
    }
}