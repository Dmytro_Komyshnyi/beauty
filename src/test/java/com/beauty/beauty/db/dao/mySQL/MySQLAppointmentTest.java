package com.beauty.beauty.db.dao.mySQL;

import com.beauty.beauty.db.bean.TimeSlotBean;
import com.beauty.beauty.db.dao.AppointmentDaoException;
import com.beauty.beauty.db.dao.ConnectionBuilder;
import com.beauty.beauty.db.entity.*;
import com.beauty.beauty.util.UserUtil;
import org.junit.jupiter.api.*;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MySQLAppointmentTest {

    private static MySQLAppointment sqlAppointment;
    private static final String connectionUrl =
            "jdbc:mysql://localhost:3306/beauty_salon_test?allowMultiQueries=true&sslMode=DISABLED&serverTimezone=UTC&user=root&password=root";

    @BeforeAll
    public static void init() {
        sqlAppointment = new MySQLAppointment();
        sqlAppointment.setConnectionBuilder(() -> DriverManager.getConnection(connectionUrl));
    }

    @BeforeEach
    public void createTables() throws SQLException {
        try (Connection con = DriverManager.getConnection(connectionUrl);
             Statement stmt = con.createStatement())
        {
            stmt.execute(
                    "CREATE TABLE IF NOT EXISTS `roles` (\n" +
                        "  `id` INT NOT NULL AUTO_INCREMENT,\n" +
                        "  `name` VARCHAR(40) NULL DEFAULT NULL,\n" +
                        "  PRIMARY KEY (`id`));\n"+
                        "CREATE TABLE IF NOT EXISTS `users` (\n" +
                        "  `id` INT NOT NULL AUTO_INCREMENT,\n" +
                        "  `name` VARCHAR(150) NOT NULL,\n" +
                        "  `login` VARCHAR(40) NOT NULL,\n" +
                        "  `password` VARCHAR(32) NOT NULL,\n" +
                        "  `phone` VARCHAR(15) NOT NULL,\n" +
                        "  `role_id` INT NOT NULL,\n" +
                        "  PRIMARY KEY (`id`),\n" +
                        "  UNIQUE INDEX `login_UNIQUE` (`login`),\n" +
                        "  CONSTRAINT `fk_users_roles`\n" +
                        "    FOREIGN KEY (`role_id`)\n" +
                        "    REFERENCES `roles` (`id`)\n" +
                        "    ON DELETE NO ACTION\n" +
                        "    ON UPDATE NO ACTION);\n" +
                        "CREATE TABLE IF NOT EXISTS `services` (\n" +
                        "  `id` INT NOT NULL AUTO_INCREMENT,\n" +
                        "  `name` VARCHAR(45) NOT NULL,\n" +
                        "  `price` int NOT NULL,\n" +
                        "  `estimated_time` int NOT NULL,\n" +
                        "  PRIMARY KEY (`id`),\n" +
                        "  UNIQUE INDEX `name_UNIQUE` (`name`));\n" +
                        "CREATE TABLE IF NOT EXISTS `days` (\n" +
                        "`name` VARCHAR(15) NOT NULL,\n" +
                        "PRIMARY KEY (`name`));\n"+
                        "CREATE TABLE IF NOT EXISTS `schedule` (\n" +
                        "  `id` INT NOT NULL AUTO_INCREMENT,\n" +
                        "  `time_start` TIME NOT NULL,\n" +
                        "  `time_end` TIME NOT NULL,\n" +
                        "  `day` VARCHAR(15) NOT NULL,\n" +
                        "  `open` BOOLEAN NOT NULL,\n" +
                        "  PRIMARY KEY (`id`),\n" +
                        "  CONSTRAINT `fk_Shedual_days`\n" +
                        "    FOREIGN KEY (`day`)\n" +
                        "    REFERENCES `days` (`name`)\n" +
                        "    ON DELETE NO ACTION\n" +
                        "    ON UPDATE NO ACTION);\n"+
                        "CREATE TABLE IF NOT EXISTS `worker_schedule` (\n" +
                        "  `id` INT NOT NULL AUTO_INCREMENT,\n" +
                        "  `time_start` TIME NOT NULL,\n" +
                        "  `time_end` TIME NOT NULL,\n" +
                        "  `name` varchar(20),\n" +
                        "  PRIMARY KEY (`id`));\n" +
                        "CREATE TABLE IF NOT EXISTS `work_date` (\n" +
                        "  `worker_id` INT NOT NULL AUTO_INCREMENT,\n" +
                        "  `worker_schedule_id` INT NOT NULL,\n" +
                        "  `date` DATE NOT NULL,\n" +
                        "  PRIMARY KEY (`worker_id`, `worker_schedule_id`, `date`),\n" +
                        "  CONSTRAINT `fk_work_date_users`\n" +
                        "    FOREIGN KEY (`worker_id`)\n" +
                        "    REFERENCES `users` (`id`)\n" +
                        "    ON DELETE NO ACTION\n" +
                        "    ON UPDATE NO ACTION,\n" +
                        "  CONSTRAINT `fk_work_date_worker_schedule`\n" +
                        "    FOREIGN KEY (`worker_schedule_id`)\n" +
                        "    REFERENCES `worker_schedule` (`id`)\n" +
                        "    ON DELETE NO ACTION\n" +
                        "    ON UPDATE NO ACTION);\n"+
                        "CREATE TABLE IF NOT EXISTS `roles_has_services` (\n" +
                        "  `roles_id` INT NOT NULL,\n" +
                        "  `services_id` INT NOT NULL,\n" +
                        "  PRIMARY KEY (`roles_id`, `services_id`),\n" +
                        "  CONSTRAINT `fk_roles_has_services_roles`\n" +
                        "    FOREIGN KEY (`roles_id`)\n" +
                        "    REFERENCES `roles` (`id`)\n" +
                        "    ON DELETE NO ACTION\n" +
                        "    ON UPDATE NO ACTION,\n" +
                        "  CONSTRAINT `fk_roles_has_services_services`\n" +
                        "    FOREIGN KEY (`services_id`)\n" +
                        "    REFERENCES `services` (`id`)\n" +
                        "    ON DELETE NO ACTION\n" +
                        "    ON UPDATE NO ACTION);\n"+
                        "CREATE TABLE IF NOT EXISTS `appointment` (\n" +
                        "  `id` INT NOT NULL AUTO_INCREMENT,\n" +
                        "  `date` DATE NOT NULL,\n" +
                        "  `time_start` TIME NOT NULL,\n" +
                        "  `time_end` TIME NOT NULL,\n" +
                        "  `price` int NOT NULL check(`price` > 0),\n" +
                        "  `accepted` BOOLEAN NOT NULL default false,\n" +
                        "  `completed` BOOLEAN NOT NULL default false,\n" +
                        "  `canceled` BOOLEAN NOT NULL default false,\n" +
                        "  `services_id` INT NOT NULL,\n" +
                        "  `users_id` INT NOT NULL,\n" +
                        "  `worker_id` INT NULL,\n" +
                        "  PRIMARY KEY (`id`),\n" +
                        "  CONSTRAINT `fk_appointment_services`\n" +
                        "    FOREIGN KEY (`services_id`)\n" +
                        "    REFERENCES `services` (`id`)\n" +
                        "    ON DELETE NO ACTION\n" +
                        "    ON UPDATE NO ACTION,\n" +
                        "  CONSTRAINT `fk_appointment_users`\n" +
                        "    FOREIGN KEY (`users_id`)\n" +
                        "    REFERENCES `users` (`id`)\n" +
                        "    ON DELETE NO ACTION\n" +
                        "    ON UPDATE NO ACTION,\n" +
                        "  CONSTRAINT `fk_appointment_users2`\n" +
                        "    FOREIGN KEY (`worker_id`)\n" +
                        "    REFERENCES `users` (`id`)\n" +
                        "    ON DELETE NO ACTION\n" +
                        "    ON UPDATE NO ACTION,\n" +
                        "CONSTRAINT time_less_end CHECK (time_start < time_end));\n"+
                        "CREATE TABLE IF NOT EXISTS `evaluations` (\n" +
                        "`appointment_id` INT NOT NULL,\n" +
                        "`evaluation` INT NOT NULL check(evaluation >=0 and evaluation <= 10),\n" +
                        "PRIMARY KEY (`appointment_id`),\n" +
                        "CONSTRAINT `fk_appointment_id`\n" +
                        "FOREIGN KEY (`appointment_id`)\n" +
                        "REFERENCES `appointment` (`id`)\n" +
                        "ON DELETE no action\n" +
                        "ON UPDATE no action\n" +
                        ");\n"+
                        "CREATE TABLE IF NOT EXISTS `services` (\n" +
                        "`id` INT NOT NULL AUTO_INCREMENT,\n" +
                        "`name` VARCHAR(45) NOT NULL,\n" +
                        "`price` int NOT NULL,\n" +
                        "`estimated_time` int NOT NULL,\n" +
                        "PRIMARY KEY (`id`),\n" +
                        "UNIQUE INDEX `name_UNIQUE` (`name`));\n"+
                         "insert into `roles` \n" +
                            "(name)\n" +
                            "values ('admin');\n" +
                            "insert into `roles` \n" +
                            "(name)\n" +
                            "values ('client');\n" +
                            "insert into `roles` \n" +
                            "(name)\n" +
                            "values ('master');\n"+
                        "insert into `services` \n" +
                                "(name, estimated_time, price)\n" +
                                "values ('haircut', 30, 10000);\n"+
                        "insert into `services` \n" +
                                "(name, estimated_time, price)\n" +
                                "values ('nail', 30, 20000);\n"+
                        "insert into `services` \n" +
                                "(name, estimated_time, price)\n" +
                                "values ('peeling', 180, 50000);" +
                            "insert into users (login, password, name, role_id, phone) VALUES('dimakomishny@gmail.com', md5('password'), 'user1', 1, '2');\n"+
                            "insert into users (login, password, name, role_id, phone) VALUES('fakeUser', md5('password'), 'fakeUser', 2, '000');\n" +
                            "insert into users (login, password, name, role_id, phone) VALUES('user2', md5('pas'), 'user2', 2, '2');\n" +
                            "insert into users (login, password, name, role_id, phone) VALUES('user3', md5('pas'), 'user3', 2, '3');\n" +
                            "insert into users (login, password, name, role_id, phone) VALUES('user4', md5('pas'), 'user4', 2, '4');\n" +
                            "insert into users (login, password, name, role_id, phone) VALUES('user5', md5('pas'), 'user4', 2, '5');\n" +

                            "insert into users (login, password, name, role_id, phone) VALUES('master6', md5('pas'), 'master5', 3, '6');\n" +
                            "insert into users (login, password, name, role_id, phone) VALUES('master7@gmail.com', md5('password'), 'master6', 3, '7');\n" +
                            "insert into users (login, password, name, role_id, phone) VALUES('saster6', md5('pas'), 'aster5', 3, '18');\n" +

                            "insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-2-24', '15:00:00', '15:30:00', 10000, 1, 3, 6);\n" +
                            "insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-2-24', '15:00:00', '15:30:00', 10000, 1, 3, 6);\n" +
                            "insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-2-24', '12:00:00', '13:30:00', 10000, 1, 3, 6);\n" +
                            "insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-2-24', '8:30:00', '10:00:00', 10000, 1, 3, 7);\n" +
                            "insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-2-24', '13:00:00', '13:50:00', 10000, 1, 3, 7);\n" +
                            "insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-2-24', '14:00:00', '14:30:00', 10000, 1, 3, 7);\n" +
                            "insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) VALUES('2021-2-24', '14:00:00', '14:30:00', 10000, 1, 3, 8);\n" +

                            "insert into worker_schedule (time_start, time_end, `name`) VALUES('9:00:00', '18:00:00', 'Weekday');\n" +
                            "insert into worker_schedule (time_start, time_end, `name`) VALUES('10:00:00', '16:00:00', 'Weekend');\n" +

                            "insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('6', '1', '2021-2-24');\n" +
                            "insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('6', '1', '2021-2-25');\n" +
                            "insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('6', '1', '2021-2-26');\n" +
                            "insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('7', '1', '2021-2-24');\n" +
                            "insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('7', '1', '2021-2-25');\n" +
                            "insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('7', '1', '2021-2-26');\n" +
                            "insert into work_date (worker_id, worker_schedule_id, `date`) VALUES('8', '1', '2021-2-24');\n" +

                            "insert into evaluations (appointment_id, evaluation) VALUES(1, 5);\n" +
                            "insert into evaluations (appointment_id, evaluation) VALUES(2, 6);\n" +
                            "insert into evaluations (appointment_id, evaluation) VALUES(5, 6);\n" +
                            "insert into evaluations (appointment_id, evaluation) VALUES(3, 10);\n" +
                            "insert into evaluations (appointment_id, evaluation) VALUES(7, 1);"
            );
        }
    }

    @AfterEach
    public void drop() throws SQLException {
        try (Connection con = DriverManager.getConnection(connectionUrl);
             Statement stmt = con.createStatement();
        ) {
            stmt.execute("drop table if exists work_date;\n" +
                            "drop table if exists evaluations;\n" +
                            "drop table if exists appointment;\n" +
                            "drop table if exists roles_has_services;\n" +
                            "drop table if exists users;\n" +
                            "drop table if exists roles;\n" +
                            "drop table if exists services;\n" +
                            "drop table if exists `schedule`;\n" +
                            "drop table if exists worker_schedule;\n" +
                            "drop table if exists days;\n" +
                            "drop view if exists clients_view; \n" +
                            "drop view if exists admins_view; \n" +
                            "drop view if exists masters_view; "
            );
        }
    }

    @Test
    public void testGetAppointmentsShouldReturnFistFiveAppointments() throws AppointmentDaoException {
        User user = new User();
        user.setId(3);
        List<Appointment> list = sqlAppointment.getAppointments(0, 5, user);
        String expected = "[Appointment{id=4, date=2021-02-24, timeStart=08:30:00, " +
                "timeEnd=10:00:00, price=10000, accepted=false, completed=false, " +
                "canceled=false, service=Service{id=1, price=10000, name='haircut'," +
                " estimatedTime=30}, user=User{id=3, login='user2', password='null', " +
                "role=Client, name='user2', phone='2'}, worker=User{id=7, login='master6'," +
                " password='null', role=Master, name='master5', phone='6'}}, Appointment{id=3," +
                " date=2021-02-24, timeStart=12:00:00, timeEnd=13:30:00, price=10000, accepted=false," +
                " completed=false, canceled=false, service=Service{id=1, price=10000, " +
                "name='haircut', estimatedTime=30}, user=User{id=3, login='user2', password='null'," +
                " role=Client, name='user2', phone='2'}, worker=User{id=6, login='user5'," +
                " password='null', role=Client, name='user4', phone='5'}}, Appointment{id=5," +
                " date=2021-02-24, timeStart=13:00:00, timeEnd=13:50:00, price=10000, accepted=false," +
                " completed=false, canceled=false, service=Service{id=1, price=10000, name='haircut', " +
                "estimatedTime=30}, user=User{id=3, login='user2', password='null', role=Client, " +
                "name='user2', phone='2'}, worker=User{id=7, login='master6', password='null', " +
                "role=Master, name='master5', phone='6'}}, Appointment{id=7, date=2021-02-24," +
                " timeStart=14:00:00, timeEnd=14:30:00, price=10000, accepted=false, completed=false, " +
                "canceled=false, service=Service{id=1, price=10000, name='haircut', estimatedTime=30}," +
                " user=User{id=3, login='user2', password='null', role=Client, name='user2', phone='2'}," +
                " worker=User{id=8, login='master7@gmail.com', password='null', role=Master, " +
                "name='master6', phone='7'}}, Appointment{id=6, date=2021-02-24, timeStart=14:00:00, " +
                "timeEnd=14:30:00, price=10000, accepted=false, completed=false, canceled=false," +
                " service=Service{id=1, price=10000, name='haircut', estimatedTime=30}, user=User{id=3," +
                " login='user2', password='null', role=Client, name='user2', phone='2'}, " +
                "worker=User{id=7, login='master6', password='null', role=Master, name='master5', phone='6'}}]";
        String result = list.toString();
        assertEquals(expected, result);
        assertEquals(5, list.size());
    }

    @Test
    public void testGetBusyTimeSlotOrderTimeStart() throws AppointmentDaoException {
        Date date = Date.valueOf("2021-2-24");
        int id = 7;
        List<TimeSlotBean> result = sqlAppointment.getBusyTimeSlotOrderTimeStart(date, id);
        String expected = "[TimeSlotBean{timeStart=08:30:00, timeEnd=10:00:00}, " +
                "TimeSlotBean{timeStart=13:00:00, timeEnd=13:50:00}, " +
                "TimeSlotBean{timeStart=14:00:00, timeEnd=14:30:00}]";
        assertEquals(expected, result.toString());
    }

    @Test
    public void testGetNumberOfUserAppointment() throws AppointmentDaoException {
        User user = new User();
        user.setId(3);
        int result = sqlAppointment.getNumberOfUserAppointment(user);
        int expected = 7;
        assertEquals(expected, result);
    }

    @Test
    public void testAddEvaluation() throws AppointmentDaoException, SQLException {
        Evaluation evaluation = new Evaluation();
        evaluation.setEvaluation(2);
        Appointment app = new Appointment();
        app.setId(4);
        evaluation.setAppointment(app);
        sqlAppointment.addEvaluation(evaluation);
        try (Connection con = DriverManager.getConnection(connectionUrl);
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT appointment_id, evaluation FROM evaluations " +
                        "WHERE appointment_id = "+evaluation.getAppointment().getId()+";"))
        {
            rs.next();
            int id = rs.getInt(1);
            int value = rs.getInt(2);
            assertEquals(evaluation.getAppointment().getId(), id);
            assertEquals(evaluation.getEvaluation(), value);
        }
    }

    @Test
    public void testAddEvaluationShouldCatchSQlExceptionAndThrowAppointmentException() {
        Evaluation evaluation = new Evaluation();
        evaluation.setEvaluation(0);
        Appointment app = new Appointment();
        app.setId(1);
        evaluation.setAppointment(app);
        assertThrows(AppointmentDaoException.class, () -> sqlAppointment.addEvaluation(evaluation));
    }

    @Test
    public void testUpdateEvaluationShouldChangeEvaluation() throws AppointmentDaoException {
        Evaluation evaluation = new Evaluation();
        Appointment appointment = new Appointment();
        appointment.setId(5);
        evaluation.setAppointment(appointment);
        evaluation.setEvaluation(1);
        sqlAppointment.updateEvaluation(evaluation);
        try (Connection con = DriverManager.getConnection(connectionUrl);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT appointment_id, evaluation FROM evaluations " +
                     "WHERE appointment_id = "+evaluation.getAppointment().getId()+";"))
        {
            rs.next();
            int id = rs.getInt(1);
            int value = rs.getInt(2);
            assertEquals(evaluation.getAppointment().getId(), id);
            assertEquals(evaluation.getEvaluation(), value);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    @Test
    public void testUpdateEvaluationShouldCatchSQlExceptionAndThrowAppointmentException() {
        Evaluation evaluation = new Evaluation();
        Appointment appointment = new Appointment();
        appointment.setId(0);
        evaluation.setAppointment(appointment);
        evaluation.setEvaluation(1);
        assertThrows(AppointmentDaoException.class, () -> sqlAppointment.updateEvaluation(evaluation));
    }

    @Test
    public void testGetEvaluation() throws AppointmentDaoException {
        int id = 3;
        int expectedEvaluation = 10;
        Evaluation evaluation = sqlAppointment.getEvaluation(id);
        assertEquals(expectedEvaluation, evaluation.getEvaluation());
        assertEquals(id, evaluation.getAppointment().getId());
    }

    @Test
    public void testGetEvaluationShouldCatchSQlExceptionAndThrowAppointmentExceptionIfSQLException() {
        int id = 0;
        Exception e = assertThrows(AppointmentDaoException.class, () -> sqlAppointment.getEvaluation(id));
        String expectedMessage = "can not find evaluation id["+id+"]";
        String message = e.getMessage();
        assertTrue(message.contains(expectedMessage));
    }

    @Test
    public void testGetEvaluationShouldCatchSQlExceptionAndThrowAppointmentExceptionIfEvaluationNotFound() throws NoSuchFieldException, SQLException, IllegalAccessException {
        ConnectionBuilder connectionBuilder = mock(ConnectionBuilder.class);
        doAnswer((a)-> {throw new SQLException("getConnection");})
                .when(connectionBuilder).getConnection();
        Field field = sqlAppointment.getClass().getDeclaredField("connectionBuilder");
        field.setAccessible(true);
        Object oldConnectionBuilder = field.get(sqlAppointment);
        field.set(sqlAppointment, connectionBuilder);
        Exception e = assertThrows(AppointmentDaoException.class, () -> sqlAppointment.getEvaluation(0));
        String expectedMessage = "can not get evaluation id["+0+"]";
        String message = e.getMessage();
        assertTrue(message.contains(expectedMessage));
        field.set(sqlAppointment, oldConnectionBuilder);
    }

    @Test
    public void testGetNumberOfMasterAppointmentShouldReturnZero() throws AppointmentDaoException {
        User master = new User();
        master.setId(0);
        int result = sqlAppointment.getNumberOfMasterAppointment(master);
        int expected = 0;
        assertEquals(expected, result);
    }

    @Test
    public void testGetNumberOfMasterAppointment() throws AppointmentDaoException {
        User master = new User();
        master.setId(6);
        int result = sqlAppointment.getNumberOfMasterAppointment(master);
        int expected = 3;
        assertEquals(expected, result);
    }

    @Test
    public void testGetNumberOfMasterAppointmentShouldCatchSQLExceptionAndThrowAppointmentDaoException() throws NoSuchFieldException, SQLException, IllegalAccessException {
        ConnectionBuilder mockConnectionBuilder = mock(ConnectionBuilder.class);
        when(mockConnectionBuilder.getConnection()).thenThrow(new SQLException("getConnection"));
        Field field = sqlAppointment.getClass().getDeclaredField("connectionBuilder");
        field.setAccessible(true);
        ConnectionBuilder oldConnection = (ConnectionBuilder) field.get(sqlAppointment);
        field.set(sqlAppointment, mockConnectionBuilder);
        User master = new User();
        master.setId(6);
        Exception e = assertThrows(AppointmentDaoException.class,
                () -> sqlAppointment.getNumberOfMasterAppointment(master));
        String expectedMessage = "can not select appointments";
        String message = e.getMessage();
        assertTrue(message.contains(expectedMessage));
        field.set(sqlAppointment, oldConnection);
    }

    @Test
    public void testAppAppointment() throws AppointmentDaoException, SQLException {
        Appointment app = new Appointment();
        app.setTimeStart(Time.valueOf("10:00:00"));
        app.setTimeEnd(Time.valueOf("12:00:00"));
        app.setDate(Date.valueOf("2021-2-25"));
        User worker = new User();
        worker.setId(6);
        app.setWorker(worker);
        User user = new User();
        user.setId(2);
        app.setUser(user);
        app.setPrice(200);
        Service service = new Service();
        service.setId(1);
        app.setService(service);
        app.setAccepted(true);
        app.setCompleted(false);
        app.setCanceled(false);
        sqlAppointment.addAppointment(app);
        try (Connection con = DriverManager.getConnection(connectionUrl);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(
                     "select \n" +
                     "appointment.id, appointment.date, appointment.time_start, appointment.time_end, " +
                     "appointment.price, appointment.accepted, appointment.completed, appointment.canceled,\n" +
                     "worker.id, worker.name, worker.phone, worker.login, worker.role_id,\n" +
                     "_client.id, _client.name, _client.phone, _client.login, _client.role_id,\n" +
                     "ser.*\n" +
                     "from appointment\n" +
                     "inner join users as worker on worker.id = worker_id\n" +
                     "inner join users as _client on _client.id = users_id\n" +
                     "inner join services as ser on ser.id = services_id\n" +
                     "where appointment.id = 8"))
        {
            rs.next();
            Appointment result = buildAppointment(rs);
            assertEquals(8, result.getId());
            assertEquals(app.getDate(), result.getDate());
            assertEquals(app.getPrice(), result.getPrice());
            assertEquals(app.getTimeStart(), result.getTimeStart());
            assertEquals(app.getTimeEnd(), result.getTimeEnd());
            assertEquals(app.getWorker().getId(), result.getWorker().getId());
            assertEquals(app.getUser().getId(), result.getUser().getId());
            assertEquals(app.getService().getId(), result.getService().getId());
            assertEquals(app.getService().getId(), result.getService().getId());
        }
    }

    private Appointment buildAppointment(ResultSet rs) throws SQLException {
        Appointment app = new Appointment();
        app.setId(rs.getInt(1));
        app.setDate(rs.getDate(2));
        app.setTimeStart(rs.getTime(3));
        app.setTimeEnd(rs.getTime(4));
        app.setPrice(rs.getInt(5));
        app.setAccepted(rs.getBoolean(6));
        app.setCompleted(rs.getBoolean(7));
        app.setCanceled(rs.getBoolean(8));
        User worker = new User();
        worker.setId(rs.getInt(9));
        worker.setName(rs.getString(10));
        worker.setPhone(rs.getString(11));
        worker.setLogin(rs.getString(12));
        worker.setRole(UserUtil.getRole(rs.getInt(13)));
        User user = new User();
        user.setId(rs.getInt(14));
        user.setName(rs.getString(15));
        user.setPhone(rs.getString(16));
        user.setLogin(rs.getString(17));
        user.setRole(UserUtil.getRole(rs.getInt(18)));
        Service service = new Service();
        service.setId(rs.getInt(19));
        service.setName(rs.getString(20));
        service.setPrice(rs.getInt(21));
        service.setEstimatedTime(rs.getInt(22));
        app.setWorker(worker);
        app.setUser(user);
        app.setService(service);
        return app;
    }

    @Test
    public void testAppAppointmentShouldThrowAppointmentDaoException() throws AppointmentDaoException, IllegalAccessException, SQLException, NoSuchFieldException {
        Appointment app = new Appointment();
        app.setTimeStart(Time.valueOf("10:00:00"));
        app.setTimeEnd(Time.valueOf("12:00:00"));
        app.setDate(Date.valueOf("2021-2-25"));
        User worker = new User();
        worker.setId(6);
        app.setWorker(worker);
        User user = new User();
        user.setId(2);
        app.setUser(user);
        app.setPrice(200);
        Service service = new Service();
        service.setId(1);
        app.setService(service);
        app.setAccepted(true);
        app.setCompleted(false);
        app.setCanceled(false);
        ConnectionBuilder mockConnectionBuilder = mock(ConnectionBuilder.class);
        when(mockConnectionBuilder.getConnection()).thenThrow(new SQLException("getConnection"));
        Field field = sqlAppointment.getClass().getDeclaredField("connectionBuilder");
        field.setAccessible(true);
        ConnectionBuilder oldConnection = (ConnectionBuilder) field.get(sqlAppointment);
        field.set(sqlAppointment, mockConnectionBuilder);
        Exception e = assertThrows(AppointmentDaoException.class,
                () -> sqlAppointment.addAppointment(app));
        String expectedMessage = "can not insert appointment["+app+"]";
        String message = e.getMessage();
        assertTrue(message.contains(expectedMessage));
        field.set(sqlAppointment, oldConnection);
    }

    @Test
    public void testClose() throws Exception {
        Method method = MySQLAppointment.class.getDeclaredMethod("close", AutoCloseable.class);
        method.setAccessible(true);
        AutoCloseable closeable = mock(AutoCloseable.class);
        doAnswer((a) -> {throw new Exception();}).when(closeable).close();
        method.invoke(sqlAppointment, closeable);
    }

    @Test
    public void testUpdateAppointment() throws AppointmentDaoException, SQLException {
        Appointment app = new Appointment();
        app.setId(6);
        app.setTimeStart(Time.valueOf("10:00:00"));
        app.setTimeEnd(Time.valueOf("12:00:00"));
        app.setDate(Date.valueOf("2021-2-25"));
        User worker = new User();
        worker.setId(6);
        app.setWorker(worker);
        User user = new User();
        user.setId(2);
        app.setUser(user);
        app.setPrice(200);
        Service service = new Service();
        service.setId(1);
        app.setService(service);
        app.setAccepted(true);
        app.setCompleted(false);
        app.setCanceled(false);
        sqlAppointment.updateAppointment(app);
        try (Connection con = DriverManager.getConnection(connectionUrl);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(
                     "select \n" +
                             "appointment.id, appointment.date, appointment.time_start, appointment.time_end, " +
                             "appointment.price, appointment.accepted, appointment.completed, appointment.canceled,\n" +
                             "worker.id, worker.name, worker.phone, worker.login, worker.role_id,\n" +
                             "_client.id, _client.name, _client.phone, _client.login, _client.role_id,\n" +
                             "ser.*\n" +
                             "from appointment\n" +
                             "inner join users as worker on worker.id = worker_id\n" +
                             "inner join users as _client on _client.id = users_id\n" +
                             "inner join services as ser on ser.id = services_id\n" +
                             "where appointment.id =" + app.getId()))
        {
            rs.next();
            Appointment result = buildAppointment(rs);
            assertEquals(app.getId(), result.getId());
            assertEquals(app.getDate(), result.getDate());
            assertEquals(app.getPrice(), result.getPrice());
            assertEquals(app.getTimeStart(), result.getTimeStart());
            assertEquals(app.getTimeEnd(), result.getTimeEnd());
            assertEquals(app.getWorker().getId(), result.getWorker().getId());
            assertEquals(app.getUser().getId(), result.getUser().getId());
            assertEquals(app.getService().getId(), result.getService().getId());
            assertEquals(app.getService().getId(), result.getService().getId());
        }
    }

    @Test
    public void testUpdateAppointmentShouldThrowAppointmentDaoException() throws SQLException, NoSuchFieldException, IllegalAccessException {
        Appointment app = new Appointment();
        app.setId(6);
        app.setTimeStart(Time.valueOf("10:00:00"));
        app.setTimeEnd(Time.valueOf("12:00:00"));
        app.setDate(Date.valueOf("2021-2-25"));
        User worker = new User();
        worker.setId(6);
        app.setWorker(worker);
        User user = new User();
        user.setId(2);
        app.setUser(user);
        app.setPrice(200);
        Service service = new Service();
        service.setId(1);
        app.setService(service);
        app.setAccepted(true);
        app.setCompleted(false);
        app.setCanceled(false);
        ConnectionBuilder mockConnectionBuilder = mock(ConnectionBuilder.class);
        when(mockConnectionBuilder.getConnection()).thenThrow(new SQLException("getConnection"));
        Field field = sqlAppointment.getClass().getDeclaredField("connectionBuilder");
        field.setAccessible(true);
        ConnectionBuilder oldConnection = (ConnectionBuilder) field.get(sqlAppointment);
        field.set(sqlAppointment, mockConnectionBuilder);
        Exception e = assertThrows(AppointmentDaoException.class,
                () -> sqlAppointment.updateAppointment(app));
        String expectedMessage = "can not update appointment+["+app+"]";
        String message = e.getMessage();
        assertTrue(message.contains(expectedMessage));
        field.set(sqlAppointment, oldConnection);
    }

    @Test
    public void testChangeAcceptStatus() throws SQLException, AppointmentDaoException {
        int id = 3;
        sqlAppointment.changeAcceptStatus(3, true);
        try (Connection con = DriverManager.getConnection(connectionUrl);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(
                     "select appointment.accepted\n"+
                             "from appointment\n" +
                             "where appointment.id =" + id))
        {
            rs.next();
            boolean result = rs.getBoolean(1);
            assertTrue(result);
        }
    }

    @Test
    public void testChangeAcceptStatusShouldThrowAppointmentDaoException() throws SQLException, NoSuchFieldException, IllegalAccessException {
        ConnectionBuilder mockConnectionBuilder = mock(ConnectionBuilder.class);
        when(mockConnectionBuilder.getConnection()).thenThrow(new SQLException("getConnection"));
        Field field = sqlAppointment.getClass().getDeclaredField("connectionBuilder");
        field.setAccessible(true);
        ConnectionBuilder oldConnection = (ConnectionBuilder) field.get(sqlAppointment);
        field.set(sqlAppointment, mockConnectionBuilder);
        int id = 1;
        boolean accept = false;
        Exception e = assertThrows(AppointmentDaoException.class,
                () -> sqlAppointment.changeAcceptStatus(id, accept));
        String expectedMessage = "can not change accepted status id["+id+"] accepted[" +accept+ "]";
        String message = e.getMessage();
        assertTrue(message.contains(expectedMessage));
        field.set(sqlAppointment, oldConnection);
    }

}