package com.beauty.beauty.db.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    public void testEqualsHashCodeShouldEquals() {
        int id = 1;
        String login = "login";
        String password1 = "password1";
        String password2 = "password2";
        String name = "name";
        String phone = "771";
        Role role = Role.Client;
        User user1 = new User(login, password1, role, name, phone);
        User user2 = new User(login, password2, role, name, phone);
        assertTrue(user1.equals(user2));
        int hash1 = user1.hashCode();
        int hash2 = user2.hashCode();
        assertEquals(hash1, hash2);
    }


    @Test
    public void testEqualsHashCodeShouldDifferent() {
        int id = 1;
        String login1 = "login1";
        String login2 = "login2";
        String password1 = "password1";
        String password2 = "password2";
        String name = "name";
        String phone = "771";
        Role role = Role.Client;
        User user1 = new User(login1, password1, role, name, phone);
        User user2 = new User(login2, password2, role, name, phone);
        assertFalse(user1.equals(user2));
        int hash1 = user1.hashCode();
        int hash2 = user2.hashCode();
        assertNotEquals(hash1, hash2);
    }
}