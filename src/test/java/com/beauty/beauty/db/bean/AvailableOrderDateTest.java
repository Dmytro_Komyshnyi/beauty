package com.beauty.beauty.db.bean;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class AvailableOrderDateTest {

    @Test
    public void testCtorShouldNowWithCurrentDateAndMaxWithCurrentPlusFourteenDays() {
        Date nowExpected = new Date();
        Date maxExpected = new Date(nowExpected.getTime() + 1209600000);
        AvailableOrderDate orderDate = new AvailableOrderDate();
        boolean minResult = (orderDate.getNow().getTime() >= nowExpected.getTime()
                && orderDate.getNow().getTime() <= nowExpected.getTime() + 1000);
        boolean maxResult = (orderDate.getMax().getTime() >= maxExpected.getTime()
                && orderDate.getMax().getTime() <= maxExpected.getTime() + 1000);
        assertTrue(minResult);
        assertTrue(maxResult);
    }
}