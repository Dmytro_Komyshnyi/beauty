package com.beauty.beauty.db.dao;

import com.beauty.beauty.db.bean.TimeSlotBean;
import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.db.entity.User;
import com.beauty.beauty.db.entity.WorkDate;
import com.beauty.beauty.db.entity.Worker;

import java.sql.Date;
import java.util.List;

public interface UserDAO extends DAO {

    User getUser(String login, String password) throws UserDaoException;
    User getUser(int id) throws UserDaoException;
    void addUser(User user)  throws UserDaoException;
    TimeSlotBean getWorkTimeByDate(int workerId, Date date)  throws UserDaoException;
    List<User> getUsersByRole(Role role) throws UserDaoException;
    List<Worker> getMasters() throws UserDaoException;
    List<WorkDate> getSchedule(User user) throws UserDaoException;
    List<WorkDate> getSchedule(int id) throws UserDaoException;
    void addWorkerDate(int id, Date date) throws UserDaoException;

}
