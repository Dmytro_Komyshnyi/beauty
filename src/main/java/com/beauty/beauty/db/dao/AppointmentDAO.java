package com.beauty.beauty.db.dao;

import com.beauty.beauty.db.bean.TimeSlotBean;
import com.beauty.beauty.db.entity.Appointment;
import com.beauty.beauty.db.entity.Evaluation;
import com.beauty.beauty.db.entity.User;

import java.sql.Date;
import java.util.List;

public interface AppointmentDAO extends DAO{

    void addAppointment(Appointment appointment) throws AppointmentDaoException;

    int getNumberOfActualAppointment() throws AppointmentDaoException;

    int getNumberOfUserAppointment(User user) throws AppointmentDaoException;

    int getNumberOfMasterAppointment(User master) throws AppointmentDaoException;

    void updateEvaluation(Evaluation evaluation) throws AppointmentDaoException;

    void changeCancelStatus(int id, boolean cancel) throws AppointmentDaoException;

    void changeCancelStatus(int userId, int appointmentId, boolean cancel) throws AppointmentDaoException;

    void changeAcceptStatus(int id, boolean accept) throws AppointmentDaoException;

    void changeCompletedStatus(int masterId, int appointmentId, boolean completed) throws AppointmentDaoException;

    void addEvaluation(Evaluation evaluation) throws AppointmentDaoException;

    void updateAppointment(Appointment app) throws AppointmentDaoException;

    List<TimeSlotBean> getBusyTimeSlotOrderTimeStart(Date date, int masterId) throws AppointmentDaoException;

    List<Appointment> getActualAppointments(int start, int limit) throws AppointmentDaoException;

    List<Appointment> getAppointments(int start, int limit, User user) throws AppointmentDaoException;

    List<Appointment> getMasterAppointment(int start, int limit, User master) throws AppointmentDaoException;

    Appointment getAppointment(int appId) throws AppointmentDaoException;

    Evaluation getEvaluation(int appointmentId) throws AppointmentDaoException;
}
