package com.beauty.beauty.db.dao;

public class ServiceDaoException extends Exception {

    public ServiceDaoException(String message, Throwable cause) {
        super(message, cause);
    }
    public ServiceDaoException(String message) {
        super(message);
    }
}
