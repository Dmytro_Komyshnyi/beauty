package com.beauty.beauty.db.dao.mySQL;

import com.beauty.beauty.db.bean.TimeSlotBean;
import com.beauty.beauty.db.dao.AppointmentDAO;
import com.beauty.beauty.db.dao.AppointmentDaoException;
import com.beauty.beauty.db.dao.ConnectionBuilder;
import com.beauty.beauty.db.entity.Appointment;
import com.beauty.beauty.db.entity.Evaluation;
import com.beauty.beauty.db.entity.Service;
import com.beauty.beauty.db.entity.User;
import com.beauty.beauty.util.UserUtil;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class MySQLAppointment implements AppointmentDAO {

    private static ConnectionBuilder connectionBuilder;
    private static final Logger LOG = Logger.getLogger(MySQLAppointment.class);

    /**
     * add evaluation to DB
     * @throws AppointmentDaoException if evaluation less then 1 or DB's problem
     */
    @Override
    public void addEvaluation(Evaluation evaluation) throws AppointmentDaoException {
        try {
            update("insert into evaluations (appointment_id, evaluation) VALUES("
                    +evaluation.getAppointment().getId()+", "+evaluation.getEvaluation()+");");
        } catch (SQLException e) {
            LOG.warn("#addEvaluation: can not insert evaluation["+evaluation+"]", e);
            throw new AppointmentDaoException("can not insert new evaluation["+evaluation+"]", e);
        }
    }

    /**
     * update exist evaluation
     * @param evaluation exist evaluations with correct id
     */
    @Override
    public void updateEvaluation(Evaluation evaluation) throws AppointmentDaoException {
        try {
            update("update evaluations set evaluation="
                    +evaluation.getEvaluation()+" where appointment_id="+evaluation.getAppointment().getId()+";");
        } catch (SQLException e) {
            throw new AppointmentDaoException("can not update evaluation["+evaluation+"]", e);
        }
    }

    /**
     * @param appointmentId evaluation id
     * @return evaluation
     * @throws AppointmentDaoException can be thrown if request finds nothing
     */
    @Override
    public Evaluation getEvaluation(int appointmentId) throws AppointmentDaoException {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionBuilder.getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(
                    "select \n" +
                            "appointment.id, appointment.date, appointment.time_start, appointment.time_end, " +
                            "appointment.price, appointment.accepted, appointment.completed, appointment.canceled,\n" +
                            "worker.id, worker.name, worker.phone, worker.login, worker.role_id,\n" +
                            "_client.id, _client.name, _client.phone, _client.login, _client.role_id,\n" +
                            "ser.*, e.evaluation\n" +
                            "from appointment\n" +
                            "inner join users as worker on worker.id = worker_id\n" +
                            "inner join users as _client on _client.id = users_id\n" +
                            "inner join services as ser on ser.id = services_id\n" +
                            "inner join evaluations e on appointment.id = e.appointment_id\n" +
                            "where appointment.id = "+appointmentId+";");
            if (rs.next()) {
                Evaluation evaluation = new Evaluation();
                evaluation.setAppointment(buildAppointment(rs));
                evaluation.setEvaluation(rs.getInt(23));
                return evaluation;
            }
            LOG.warn("#getEvaluation: can not find evaluation id["+appointmentId+"]");
            throw new AppointmentDaoException("can not find evaluation id["+appointmentId+"]");
        } catch (SQLException e) {
            LOG.warn("#getEvaluation: can not get evaluation id["+appointmentId+"]", e);
            throw new AppointmentDaoException("can not get evaluation id["+appointmentId+"]", e);
        } finally {
            close(rs);
            close(stmt);
            close(con);
        }
    }

    /**
     * update exist appointment
     * @param app exist appointment
     */
    @Override
    public void updateAppointment(Appointment app) throws AppointmentDaoException {
        try {
            update("update appointment\n" +
                    "set\n" +
                    "    accepted = "+app.isAccepted()+",\n" +
                    "    canceled = "+app.isCanceled()+",\n" +
                    "    completed = "+app.isCompleted()+",\n" +
                    "    price = "+app.getPrice()+",\n" +
                    "    services_id="+app.getService().getId()+",\n" +
                    "    users_id="+app.getUser().getId()+",\n" +
                    "    worker_id="+app.getWorker().getId()+",\n" +
                    "    time_start='"+app.getTimeStart()+"',\n" +
                    "    time_end='"+app.getTimeEnd()+"',\n" +
                    "    date = '"+app.getDate()+"'\n" +
                    "    where id = "+app.getId()+";");
        } catch (SQLException e) {
            throw new AppointmentDaoException("can not update appointment+["+app+"]", e);
        }
    }

    /**
     * update column accepted
     * @param id appointment id
     * @param accept accepted
     */
    @Override
    public void changeAcceptStatus(int id, boolean accept) throws AppointmentDaoException {
        try {
            update(
                    "UPDATE appointment\n" +
                            "SET accepted = "+accept+"\n" +
                            "WHERE id = "+id+";");
        } catch (SQLException e) {
            LOG.warn("#changeAcceptStatus: can not change accepted status id["+id+"] accepted[" +accept+ "]");
            throw new AppointmentDaoException("can not change accepted status id["+id+"] accepted[" +accept+ "]", e);
        }
    }

    /**
     * update column canceled
     * @param id appointment id
     * @param cancel canceled
     * @throws AppointmentDaoException
     */
    @Override
    public void changeCancelStatus(int id, boolean cancel) throws AppointmentDaoException {
        try {
            update(
                    "UPDATE appointment\n" +
                    "SET canceled = "+cancel+"\n" +
                    "WHERE id = "+id+";");
        } catch (SQLException e) {
            LOG.warn("#changeCancelStatus: can not change canceled status id["+id+"] canceled[" +cancel+ "]");
            throw new AppointmentDaoException("can not change canceled status id["+id+"] canceled[" +cancel+ "]", e);
        }
    }

    /**
     * update canceled column if appointment.users_id = userId
     * @param userId user id
     * @param appointmentId appointment id
     * @param cancel canceled
     */
    @Override
    public void changeCancelStatus(int userId, int appointmentId, boolean cancel) throws AppointmentDaoException {
        try {
            update(
                    "UPDATE appointment\n" +
                            "SET canceled = "+cancel+"\n" +
                            "WHERE id = "+appointmentId+" AND users_id = "+userId+";");
        } catch (SQLException e) {
            LOG.warn("#changeCancelStatus: can not change canceled status userId["+userId+"] " +
                    "appointmentId["+appointmentId+"] canceled[" +cancel+ "]");
            throw new AppointmentDaoException("can not change canceled status userId["+userId+"] " +
                    "appointmentId["+appointmentId+"] canceled[" +cancel+ "]", e);
        }
    }

    /**
     * update completed column if appointment.worker_id = masterId;
     * @param masterId master id
     * @param appointmentId appointment id
     * @param completed completed status
     * @throws AppointmentDaoException
     */
    @Override
    public void changeCompletedStatus(int masterId, int appointmentId, boolean completed)
            throws AppointmentDaoException {
        try {
            update("UPDATE appointment\n" +
                    "SET completed = "+completed+"\n" +
                    "WHERE id = "+appointmentId+" AND worker_id = "+masterId+";");
        } catch (SQLException e) {
            LOG.warn("#changeCancelStatusByMaster: can not update column[canceled] of appointment cancel=["
                    +false+"] masterId["+masterId+"] appointmentId["+appointmentId+"]", e);
            throw new AppointmentDaoException("Can not update column[canceled] of appointment", e);
        }
    }

    private void update(String sql) throws SQLException {
        Connection con = null;
        Statement stmt = null;
        try {
            con = connectionBuilder.getConnection();
            stmt = con.createStatement();
            stmt.executeUpdate(sql);
            if (stmt.getUpdateCount() <= 0) {
                throw new SQLException("changed 0 rows");
            }
        } finally {
            close(stmt);
            close(con);
        }
    }

    /**
     * @param start limit start
     * @param limit limit number
     * @return return not cancel and not completed and not accepted appointments
     */
    @Override
    public List<Appointment> getActualAppointments(int start, int limit) throws AppointmentDaoException {
        try {
            return getAppointments(
                    "WHERE appointment.accepted = false AND appointment.completed = false AND appointment.canceled = false\n" +
                            "ORDER BY appointment.date, appointment.time_start\n",
                    start, limit);
        } catch (SQLException e) {
            LOG.error("#getActualAppointments: can not select appointments");
            throw new AppointmentDaoException("can not select appointments", e);
        }
    }

    /**
     * Select appointments
     * @param start limit start
     * @param limit limit quantity
     * @param user user should contains id
     * @return user's appointments
     */
    @Override
    public List<Appointment>getAppointments(int start, int limit, User user) throws AppointmentDaoException {
        try {
            return getAppointments("where users_id = "+user.getId()+
                    "\norder by appointment.completed ASC, appointment.canceled ASC, " +
                    "appointment.date, appointment.time_start\n", start, limit);
        } catch (SQLException e) {
            LOG.error("#getAppointments: can not select appointments start["+start+"] limit["+limit+"] user["+user+"]");
            throw new AppointmentDaoException("can not select appointments start["+start+"] limit["
                    +limit+"] user["+user+"]", e);
        }
    }

    /**
     * Select master's appointment
     * @param start limit start
     * @param limit limit quantity
     * @param master master should contains id
     * @return master's appointment
     */
    @Override
    public List<Appointment> getMasterAppointment(int start, int limit, User master) throws AppointmentDaoException {
        try {
            return getAppointments("where worker_id = " + master.getId() +
                    " order by appointment.completed DESC, appointment.canceled DESC, appointment.time_start\n", start, limit);
        } catch (SQLException e) {
            LOG.warn("#getMasterAppointment: can not get appointments master["+
                    master+"] start["+start+"] limit["+limit+"]", e);
            throw new AppointmentDaoException("can not get appointments master["+
                    master+"] start["+start+"] limit["+limit+"]", e);
        }
    }

    /**
     * Select appointment by id
     * @param appId appointment id
     * @return appointment
     * @throws AppointmentDaoException can throw if appointment doesn't exist
     */
    @Override
    public Appointment getAppointment(int appId) throws AppointmentDaoException {
        try {
            return getAppointments("where appointment.id =" + appId, 0,1 ).get(0);
        } catch (SQLException e) {
            LOG.warn("#getAppointment: can not get appointment with id["+appId+"]");
            throw new AppointmentDaoException("can not get appointment with id["+appId+"]", e);
        }
    }

    private List<Appointment> getAppointments(String where, int start, int limit) throws SQLException {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionBuilder.getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(
                    "select \n" +
                            "appointment.id, appointment.date, appointment.time_start, appointment.time_end, " +
                            "appointment.price, appointment.accepted, appointment.completed, appointment.canceled,\n" +
                            "worker.id, worker.name, worker.phone, worker.login, worker.role_id,\n" +
                            "_client.id, _client.name, _client.phone, _client.login, _client.role_id,\n" +
                            "ser.*\n" +
                            "from appointment\n" +
                            "inner join users as worker on worker.id = worker_id\n" +
                            "inner join users as _client on _client.id = users_id\n" +
                            "inner join services as ser on ser.id = services_id\n" +
                            where +
                            "\nlimit "+start+", "+limit+";");
            ArrayList<Appointment> list = new ArrayList<>();
            while (rs.next()) {
                list.add(buildAppointment(rs));
            }
            return list;
        } finally {
            close(rs);
            close(stmt);
            close(con);
        }
    }

    private Appointment buildAppointment(ResultSet rs) throws SQLException {
        Appointment app = new Appointment();
        app.setId(rs.getInt(1));
        app.setDate(rs.getDate(2));
        app.setTimeStart(rs.getTime(3));
        app.setTimeEnd(rs.getTime(4));
        app.setPrice(rs.getInt(5));
        app.setAccepted(rs.getBoolean(6));
        app.setCompleted(rs.getBoolean(7));
        app.setCanceled(rs.getBoolean(8));
        User worker = new User();
        worker.setId(rs.getInt(9));
        worker.setName(rs.getString(10));
        worker.setPhone(rs.getString(11));
        worker.setLogin(rs.getString(12));
        worker.setRole(UserUtil.getRole(rs.getInt(13)));
        User user = new User();
        user.setId(rs.getInt(14));
        user.setName(rs.getString(15));
        user.setPhone(rs.getString(16));
        user.setLogin(rs.getString(17));
        user.setRole(UserUtil.getRole(rs.getInt(18)));
        Service service = new Service();
        service.setId(rs.getInt(19));
        service.setName(rs.getString(20));
        service.setPrice(rs.getInt(21));
        service.setEstimatedTime(rs.getInt(22));
        app.setWorker(worker);
        app.setUser(user);
        app.setService(service);
        return app;
    }

    /**
     * @param master should contain id
     * @return number of master's appointment
     */
    @Override
    public int getNumberOfMasterAppointment(User master) throws AppointmentDaoException {
        try {
            return getRowNumber(
                    "SELECT count(id) FROM appointment\n" +
                            "WHERE worker_id = "+master.getId()+";");
        } catch (SQLException e) {
            LOG.error("#getNumberOfMasterAppointment: can not select appointments", e);
            throw new AppointmentDaoException("can not select appointments", e);
        }
    }


    /**
     * @return number of not canceled, not accepted, not completed appointments
     */
    @Override
    public int getNumberOfActualAppointment() throws AppointmentDaoException {
        try {
            return getRowNumber(
                    "SELECT count(id) FROM appointment\n" +
                            "WHERE appointment.accepted = false " +
                            "AND appointment.completed = false " +
                            "AND appointment.canceled = false\n");
        } catch (SQLException e) {
            LOG.error("#getActualAppointments: can not select appointments", e);
            throw new AppointmentDaoException("can not select appointments", e);
        }
    }

    /**
     * Select number of user's appointment
     * @param user user with id
     * @return return number of user's appointments
     */
    @Override
    public int getNumberOfUserAppointment(User user) throws AppointmentDaoException {
        try {
            return getRowNumber(
                    "select count(id) from appointment\n" +
                            "where users_id = "+user.getId()+";");
        } catch (SQLException e) {
            LOG.error("#getNumberOfUserAppointment: can not select appointments user["+user+"]");
            throw new AppointmentDaoException("can not select appointments user["+user+"]", e);
        }
    }

    private int getRowNumber(String sql) throws SQLException {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionBuilder.getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);
            rs.next();
            return rs.getInt(1);
        } finally {
            close(rs);
            close(stmt);
            close(con);
        }
    }

    /**
     * Select all master's busy slots
     * @param date date
     * @param masterId master's id non negative number and non zero
     * @return busy time slots
     */
    @Override
    public List<TimeSlotBean> getBusyTimeSlotOrderTimeStart(Date date, int masterId) throws AppointmentDaoException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionBuilder.getConnection();
            stmt = con.prepareStatement(
                    "SELECT time_start, time_end " +
                        "FROM appointment " +
                        "WHERE worker_id = "+masterId+" and canceled = false AND `date` = '"+date+"' order by time_start;");
            rs = stmt.executeQuery();
            ArrayList<TimeSlotBean> list = new ArrayList<>();
            while (rs.next()) {
                TimeSlotBean slot = new TimeSlotBean();
                slot.setTimeStart(rs.getTime(1));
                slot.setTimeEnd(rs.getTime(2));
                list.add(slot);
            }
            return list;
        } catch (SQLException e) {
            LOG.error("#getBusyTimeSlotByDate: can not get busy time slots date["+date+"]");
            throw new AppointmentDaoException("can not busy time slots date["+date+"]", e);
        } finally {
            close(rs);
            close(stmt);
            close(con);
        }
    }

    /**
     * Insert new appointment
     * @param appointment appointment
     */
    @Override
    public void addAppointment(Appointment appointment) throws AppointmentDaoException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionBuilder.getConnection();
            stmt = con.prepareStatement(
                    "insert into appointment (`date`, time_start, time_end, price, services_id, users_id, worker_id) " +
                            "VALUES(?, ?, ?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            stmt.setDate(1, appointment.getDate());
            stmt.setTime(2, appointment.getTimeStart());
            stmt.setTime(3, appointment.getTimeEnd());
            stmt.setInt(4, appointment.getPrice());
            stmt.setInt(5, appointment.getService().getId());
            stmt.setInt(6, appointment.getUser().getId());
            stmt.setInt(7, appointment.getWorker().getId());
            stmt.executeUpdate();
            rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                appointment.setId((rs.getInt(1)));
            }
        } catch (SQLException e) {
            LOG.warn("#addAppointment: \"can not insert appointment["+appointment+"]");
            throw new AppointmentDaoException("can not insert appointment["+appointment+"]", e);
        } finally {
            close(rs);
            close(stmt);
            close(con);
        }
    }

    private void close(AutoCloseable resource) {
        try {
            if (resource != null) {
                resource.close();
            }
        } catch (Exception e) {
            LOG.error("can not close resource[" + resource + "]", e);
        }
    }

    @Override
    public void setConnectionBuilder(ConnectionBuilder connectionBuilder) {
        MySQLAppointment.connectionBuilder = connectionBuilder;
    }
}
