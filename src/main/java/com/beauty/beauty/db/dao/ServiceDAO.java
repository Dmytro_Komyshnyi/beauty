package com.beauty.beauty.db.dao;

import com.beauty.beauty.db.entity.Service;

import java.util.List;

public interface ServiceDAO extends DAO {

    Service getService(String name) throws ServiceDaoException;
    List<Service> getAllService() throws ServiceDaoException;
    int getEstimatedTime(String name) throws ServiceDaoException;
}
