package com.beauty.beauty.db.dao;

public class AppointmentDaoException extends Exception {

    public AppointmentDaoException(String message, Throwable cause) {
        super(message, cause);
    }
    public AppointmentDaoException(String message) {
        super(message);
    }
}
