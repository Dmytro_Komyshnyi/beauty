package com.beauty.beauty.db.dao.mySQL;

import com.beauty.beauty.db.dao.AppointmentDAO;
import com.beauty.beauty.db.dao.PullConnectionBuilder;
import com.beauty.beauty.db.dao.ServiceDAO;
import com.beauty.beauty.db.dao.UserDAO;
import org.apache.log4j.Logger;

import javax.naming.NamingException;

public class DAOFactory {

    private static Logger LOG = Logger.getLogger(DAOFactory.class);

    public static AppointmentDAO buildAppointmentDAO() throws NamingException {
        try {
            MySQLAppointment mySQLAppointment = new MySQLAppointment();
            mySQLAppointment.setConnectionBuilder(PullConnectionBuilder.getInstance());
            LOG.info("success initialize AppointmentDAO");
            return mySQLAppointment;
        } catch (NamingException e) {
            LOG.error("#init: can not init DAOes", e);
            throw e;
        }
    }

    public static UserDAO buildUserDAO() throws NamingException {
        try {
            UserDAO userDAO = MySQLUser.getInstance();
            userDAO.setConnectionBuilder(PullConnectionBuilder.getInstance());
            LOG.info("success initialize AppointmentDAO");
            return userDAO;
        } catch (NamingException e) {
            LOG.error("#init: can not init DAOes", e);
            throw e;
        }
    }

    public static ServiceDAO buildServiceDAO() throws NamingException {
        try {
            ServiceDAO serviceDAO = MySQLService.getInstance();
            serviceDAO.setConnectionBuilder(PullConnectionBuilder.getInstance());
            LOG.info("success initialize AppointmentDAO");
            return serviceDAO;
        } catch (NamingException e) {
            LOG.error("#init: can not init DAOes", e);
            throw e;
        }
    }
}
