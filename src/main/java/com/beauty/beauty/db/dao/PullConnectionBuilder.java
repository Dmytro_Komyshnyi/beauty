package com.beauty.beauty.db.dao;

import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class PullConnectionBuilder implements ConnectionBuilder {

    private static final Logger LOG = Logger.getLogger(PullConnectionBuilder.class);

    private static PullConnectionBuilder connectionBuilder;
    private DataSource dataSource;

    /**
     * Gets DataSource
     */
    private PullConnectionBuilder() throws NamingException {
        try {
            Context ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:comp/env/jdbc/beauty_salon");
        } catch (NamingException e) {
            LOG.warn("can not DataSource", e);
            throw e;
        }
    }

    /**
     * @return {@link PullConnectionBuilder#connectionBuilder}
     */
    public static PullConnectionBuilder getInstance() throws NamingException {
        if (connectionBuilder == null) {
            connectionBuilder = new PullConnectionBuilder();
        }
        return connectionBuilder;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
