package com.beauty.beauty.db.dao.mySQL;

import com.beauty.beauty.db.dao.ConnectionBuilder;
import com.beauty.beauty.db.dao.ServiceDAO;
import com.beauty.beauty.db.dao.ServiceDaoException;
import com.beauty.beauty.db.entity.Service;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class MySQLService implements ServiceDAO {

    private static final Logger LOG = Logger.getLogger(MySQLService.class);
    private static ConnectionBuilder connectionBuilder;
    private static MySQLService dbService;

    private MySQLService() {
    }

    public static MySQLService getInstance() {
        if (dbService == null) {
            dbService = new MySQLService();
        }
        return dbService;
    }

    @Override
    public int getEstimatedTime(String name) throws ServiceDaoException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionBuilder.getConnection();
            stmt = con.prepareStatement(
                    "SELECT estimated_time FROM services WHERE name = ?;");
            stmt.setString(1, name);
            rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            throw new ServiceDaoException("can not get estimated time service ["+name+"]");
        } catch (SQLException e) {
            LOG.warn("#getEstimatedTime: name[" + name + "]", e);
            throw new ServiceDaoException("can not get estimated time service ["+name+"]", e);
        } finally {
            close(rs);
            close(stmt);
            close(con);
        }
    }

    @Override
    public Service getService(String name) throws ServiceDaoException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionBuilder.getConnection();
            stmt = con.prepareStatement(
                    "SELECT id, name, price, estimated_time FROM services WHERE name = ?;");
            stmt.setString(1, name);
            rs = stmt.executeQuery();
            if (rs.next()) {
                Service service = new Service();
                service.setId(rs.getInt(1));
                service.setName(rs.getString(2));
                service.setPrice(rs.getInt(3));
                service.setEstimatedTime(rs.getInt(4));
                return service;
            }
            throw new ServiceDaoException("can not get service["+name+"]");
        } catch (SQLException e) {
            LOG.warn("#getService: name[" + name + "]", e);
            throw new ServiceDaoException("can not get service["+name+"]", e);
        } finally {
            close(rs);
            close(stmt);
            close(con);
        }
    }

    @Override
    public List<Service> getAllService() throws ServiceDaoException {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionBuilder.getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT id, name, price, estimated_time FROM services;");
            List<Service> list = new ArrayList<>(5);
            while (rs.next()) {
                Service service = new Service();
                service.setId(rs.getInt(1));
                service.setName(rs.getString(2));
                service.setPrice(rs.getInt(3));
                service.setEstimatedTime(rs.getInt(4));
                list.add(service);
            }
            return list;
        } catch (SQLException e) {
            LOG.error("#getAllService: can not select all services", e);
            throw new ServiceDaoException("can not get services", e);
        } finally {
            close(rs);
            close(stmt);
            close(con);
        }
    }

    private void close(AutoCloseable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (Exception e) {
                LOG.error("#close: can not close resource[" + resource.toString() + "]");
            }
        }
    }

    @Override
    public void setConnectionBuilder(ConnectionBuilder connectionBuilder) {
        MySQLService.connectionBuilder = connectionBuilder;
    }
}
