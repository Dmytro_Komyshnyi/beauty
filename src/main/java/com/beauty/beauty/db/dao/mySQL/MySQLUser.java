package com.beauty.beauty.db.dao.mySQL;

import com.beauty.beauty.db.bean.TimeSlotBean;
import com.beauty.beauty.db.dao.ConnectionBuilder;
import com.beauty.beauty.db.dao.UserDAO;
import com.beauty.beauty.db.dao.UserDaoException;
import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.db.entity.User;
import com.beauty.beauty.db.entity.WorkDate;
import com.beauty.beauty.db.entity.Worker;
import com.beauty.beauty.util.UserUtil;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLUser implements UserDAO {
    private static final Logger LOG = Logger.getLogger(MySQLService.class);
    private static MySQLUser dbUser;
    private ConnectionBuilder connectionBuilder;

    private MySQLUser() {
    }

    public static MySQLUser getInstance() {
        if (dbUser == null) {
            dbUser = new MySQLUser();
        }
        return dbUser;
    }

    public void setConnectionBuilder(ConnectionBuilder connectionBuilder) {
        this.connectionBuilder = connectionBuilder;
    }

    /**
     * adds working hours to the user on this date
     * @param id user's id
     * @param date date
     * @throws UserDaoException
     */
    @Override
    public void addWorkerDate(int id, Date date) throws UserDaoException {
        try (Connection conn = connectionBuilder.getConnection();
            Statement stmt = conn.createStatement()
        ) {
            stmt.execute("insert into work_date (worker_id, worker_schedule_id, `date`) " +
                    "VALUES('"+id+"', '1', '"+date+"');");
        } catch (SQLException e) {
            LOG.warn("#addWorkerDate: can not add worker date["+date+"] for worker with id["+id+"]", e);
            throw new UserDaoException("can not add worker date["+date+"] for worker with id["+id+"]", e);
        }
    }

    /**
     * @return {@link List<Worker>} list of all workers with average evaluation
     * @throws UserDaoException
     */
    @Override
    public List<Worker> getMasters() throws UserDaoException {
        try (
            Connection con = connectionBuilder.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(
                    "select u.id, u.name, u.phone, u.role_id, sum(e.evaluation) / count(u.id), u.login from users u\n" +
                            "left join appointment a on u.id = a.worker_id\n" +
                            "left join evaluations e on a.id = e.appointment_id\n" +
                            "where role_id = 3\n" +
                            "group by a.worker_id\n" +
                            "order by u.name;"))
        {
            List<Worker> list = new ArrayList<>();
            while(rs.next()) {
                Worker worker = new Worker();
                worker.setId(rs.getInt(1));
                worker.setName(rs.getString(2));
                worker.setPhone(rs.getString(3));
                worker.setRole(UserUtil.getRole(rs.getInt(4)));
                worker.setMark(rs.getDouble(5));
                worker.setLogin(rs.getString(6));
                list.add(worker);
            }
            return list;
        } catch (SQLException e) {
            LOG.error("#getMasters: can not get masters", e);
            throw new UserDaoException("can not get masters", e);
        }
    }

    /**
     * @param user user should has worker's role
     * @return return user schedule if user has worker's role
     * @throws UserDaoException Can throw UserDaoException if there are connection or db problems.
     */
    @Override
    public List<WorkDate> getSchedule(User user) throws UserDaoException {
        Date now = new Date(System.currentTimeMillis());
        try (
            Connection con = connectionBuilder.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(
                    "select date, ws.time_start, ws.time_end from work_date as wd\n" +
                    "inner join worker_schedule ws on wd.worker_schedule_id = ws.id\n" +
                    "inner join users u on wd.worker_id = u.id\n" +
                    "where u.id = "+user.getId()+" and wd.date >='"+now.toString()+"';"))
        {
            List<WorkDate> list = new ArrayList<>();
            while (rs.next()) {
                WorkDate workDate = new WorkDate();
                workDate.setDate(rs.getDate(1));
                TimeSlotBean workHours = new TimeSlotBean();
                workHours.setTimeStart(rs.getTime(2));
                workHours.setTimeEnd(rs.getTime(3));
                workDate.setWorkHours(workHours);
                list.add(workDate);
            }
        return list;
        } catch (SQLException e) {
            LOG.warn("#getSchedule: can not get List<WorkDate> for user["+user+"]", e);
            throw new UserDaoException("can not get List<WorkDate> for user["+user+"]", e);
        }
    }

    /**
     * @param id user's should belong to user with worker's role
     * @return return user schedule if user has worker's role
     * @throws UserDaoException Can throw UserDaoException if there are connection or db problems.
     */
    @Override
    public List<WorkDate> getSchedule(int id) throws UserDaoException {
        User user = new User();
        user.setId(id);
        return getSchedule(user);
    }

    /**
     * @param user user will be added to db if user has uniq login and phone.After adding a user,
     * the real id will be assigned to him.
     * @throws UserDaoException
     */
    @Override
    public void addUser(User user) throws UserDaoException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet returnKey = null;
        try {
            con = connectionBuilder.getConnection();
            stmt = con.prepareStatement(
                     "INSERT INTO users (login, password, name, role_id, phone) VALUES(?, md5(?), ?, ?, ?);",
                    Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, user.getLogin());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getName());
            stmt.setInt(4, user.getRole().roleId);
            stmt.setString(5, user.getPhone());
            stmt.executeUpdate();
            returnKey = stmt.getGeneratedKeys();
            if (returnKey.next()) {
                user.setId(returnKey.getInt(1));
            }
        } catch (SQLException e) {
            LOG.warn( "#addUser: can not add user[" + user.toString() + "]", e);
            throw new UserDaoException("can't add user", e);
        } finally {
            close(returnKey);
            close(stmt);
            close(con);
        }
    }

    @Override
    public User getUser(String login, String password) throws UserDaoException {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionBuilder.getConnection();
            stmt = con.prepareStatement(
                    "SELECT id, name, phone, role_id FROM users WHERE login = ? AND password = md5(?);");
            stmt.setString(1, login);
            stmt.setString(2, password);
            rs = stmt.executeQuery();
            if (rs.next()) {
                User user = new User();
                user.setId(rs.getInt(1));
                user.setName(rs.getString(2));
                user.setPhone(rs.getString(3));
                user.setRole(UserUtil.getRole(rs.getInt(4)));
                user.setLogin(login);
                return user;
            }
            throw new UserDaoException("can not get user");
        } catch (SQLException e) {
            LOG.warn("#getUser: can not get user login[" + login +  "] password[" + password + "]");
            throw new UserDaoException("can not get user", e);
        } finally {
            close(rs);
            close(stmt);
            close(con);
        }
    }

    @Override
    public TimeSlotBean getWorkTimeByDate(int workerId, Date date) throws UserDaoException {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionBuilder.getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(
                    "select time_start, time_end from worker_schedule\n" +
                            "inner join work_date on worker_schedule_id = id\n" +
                            "where work_date.`date` = '"+date+"' and worker_id = "+workerId+";");
            if (rs.next()) {
                TimeSlotBean slot = new TimeSlotBean();
                slot.setTimeStart(rs.getTime(1));
                slot.setTimeEnd(rs.getTime(2));
                return slot;
            }
            return null;
        } catch (SQLException e) {
            LOG.warn("#getMasterWorkTimeByDate: can not get worker time masterId["+workerId+"] date["+date+"]");
            throw new UserDaoException(
                    "can not get worker time workerId["+workerId+"] date["+date+"]");
        } finally {
            close(rs);
            close(stmt);
            close(con);
        }
    }

    @Override
    public List<User> getUsersByRole(Role role) throws UserDaoException {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionBuilder.getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT id, name, phone, login FROM users WHERE role_id = "+role.roleId+";");
            ArrayList<User> users = new ArrayList<>();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt(1));
                user.setName(rs.getString(2));
                user.setPhone(rs.getString(3));
                user.setLogin(rs.getString(4));
                user.setRole(role);
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            LOG.warn("#getUsersByRole: can not get users by role["+role+"]");
            throw new UserDaoException("can not get users by role["+role+"]");
        } finally {
            close(rs);
            close(stmt);
            close(con);
        }
    }

    @Override
    public User getUser(int id) throws UserDaoException {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = connectionBuilder.getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery(
                    "SELECT id, name, phone, role_id, login FROM users WHERE id = "+id+";");
            if (rs.next()) {
                User user = new User();
                user.setId(rs.getInt(1));
                user.setName(rs.getString(2));
                user.setPhone(rs.getString(3));
                user.setRole(UserUtil.getRole(rs.getInt(4)));
                user.setLogin(rs.getString(5));
                return user;
            }
            LOG.warn("#getUser: can not find user id[" + id +  "]");
            throw new UserDaoException("can not find user id["+id+"]");
        } catch (SQLException e) {
            LOG.warn("#getUser: can not get user id[" + id +  "]", e);
            throw new UserDaoException("can not get user id["+id+"]", e);
        } finally {
            close(rs);
            close(stmt);
            close(con);
        }
    }

    private void close (AutoCloseable resource)  {
        if (resource != null) {
            try {
                resource.close();
            } catch (Exception e) {
                LOG.error("#close: can not close resource[" + resource.toString() + "]");
            }
        }
    }
}
