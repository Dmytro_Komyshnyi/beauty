package com.beauty.beauty.db.bean;

import com.beauty.beauty.db.entity.User;

import java.util.Date;

public class Email {
    String email;
    String text;
    String subject;
    Date date;

    public static Email createEmail(User user, String subject, String text) {
        Email email = new Email();
        email.setEmail(user.getLogin());
        email.setSubject(subject);
        email.setText(text);
        email.setDate(new java.util.Date());
        return email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
