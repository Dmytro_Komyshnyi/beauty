package com.beauty.beauty.db.bean;

import java.sql.Date;

public class AvailableOrderDate {
    private Date now;
    private Date max;

    public AvailableOrderDate() {
        now = new Date(new java.util.Date().getTime());
        max = new Date(now.getTime() + 1209600000);
    }

    public Date getNow() {
        return now;
    }

    public Date getMax() {
        return max;
    }
}
