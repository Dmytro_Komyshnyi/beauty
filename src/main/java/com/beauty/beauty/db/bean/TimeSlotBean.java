package com.beauty.beauty.db.bean;

import java.sql.Time;
import java.util.Objects;

public class TimeSlotBean {

    Time timeStart;
    Time timeEnd;

    public TimeSlotBean(Time timeStart, Time timeEnd) {
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeSlotBean that = (TimeSlotBean) o;
        return Objects.equals(timeStart, that.timeStart) && Objects.equals(timeEnd, that.timeEnd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timeStart, timeEnd);
    }

    public TimeSlotBean() {
    }

    public Time getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(Time timeStart) {
        this.timeStart = timeStart;
    }

    public Time getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Time timeEnd) {
        this.timeEnd = timeEnd;
    }

    @Override
    public String toString() {
        return "TimeSlotBean{" +
                "timeStart=" + timeStart +
                ", timeEnd=" + timeEnd +
                '}';
    }
}
