package com.beauty.beauty.db.entity;

import java.sql.Date;
import java.sql.Time;

public class Appointment implements java.io.Serializable {

    private int id;
    private Date date;
    private Time timeStart;
    private Time timeEnd;
    private int price;
    private boolean accepted;
    private boolean completed;
    private boolean canceled;
    private Service service;
    private User user;
    private User worker;

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", date=" + date +
                ", timeStart=" + timeStart +
                ", timeEnd=" + timeEnd +
                ", price=" + price +
                ", accepted=" + accepted +
                ", completed=" + completed +
                ", canceled=" + canceled +
                ", service=" + service +
                ", user=" + user +
                ", worker=" + worker +
                '}';
    }


    public static Appointment createAppointment(Date date, Time start, Time end,
                                                int price, Service service, User user, User worker) {
        Appointment app = new Appointment();
        app.setDate(date);
        app.setPrice(price);
        app.setTimeStart(start);
        app.setTimeEnd(end);
        app.setService(service);
        app.setUser(user);
        app.setWorker(worker);
        return app;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Appointment that = (Appointment) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        int result = 837348723;
        return 31 * (result + id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(Time timeStart) {
        this.timeStart = timeStart;
    }

    public Time getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Time timeEnd) {
        this.timeEnd = timeEnd;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getWorker() {
        return worker;
    }

    public void setWorker(User worker) {
        this.worker = worker;
    }
}
