package com.beauty.beauty.db.entity;

public class Worker extends User{

    private double mark;

    public Worker() {
    }

    public Worker(String login, String password, Role role, String name, String phone, int mark) {
        super(login, password, role, name, phone);
        this.mark = mark;
    }

    public double getMark() {
        return mark;
    }

    public void setMark(double mark) {
        this.mark = mark;
    }
}
