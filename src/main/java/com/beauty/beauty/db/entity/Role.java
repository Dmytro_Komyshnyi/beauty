package com.beauty.beauty.db.entity;

public enum Role {
    Admin (1),
    Client (2),
    Master (3),
    Unregistered(-1);

    public final int roleId;

    private Role(int roleId) {
        this.roleId = roleId;
    }
}
