package com.beauty.beauty.db.entity;

import com.beauty.beauty.db.bean.TimeSlotBean;

import java.sql.Date;

public class WorkDate {

    private Date date;
    private TimeSlotBean workHours;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public TimeSlotBean getWorkHours() {
        return workHours;
    }

    public void setWorkHours(TimeSlotBean workHours) {
        this.workHours = workHours;
    }
}
