package com.beauty.beauty.aop;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspectLogExecutionTime {

    private static final Logger LOG = Logger.getLogger(AspectLogExecutionTime.class);

    @Around("@annotation(LogExecutionTime)")
    public Object log(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();
        Object proceed = joinPoint.proceed();
        long end = System.currentTimeMillis();
        LOG.info(joinPoint.getSignature() + " time execution: " + (end - start));
        return proceed;
    }
}
