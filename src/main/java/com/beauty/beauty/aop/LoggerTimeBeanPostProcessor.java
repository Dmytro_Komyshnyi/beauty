package com.beauty.beauty.aop;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;

@Component
@Order(0)
public class LoggerTimeBeanPostProcessor implements BeanPostProcessor {

    private static final Logger LOG = Logger.getLogger(LoggerTimeBeanPostProcessor.class);

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class<?> clazz = bean.getClass();
        if (isContainAnnotation(clazz)) {
            ProxyFactory proxyFactory = new ProxyFactory(bean);
            MethodInterceptor interceptor = (methodInvocation)->{
                long start = System.currentTimeMillis();
                Object returnValue = methodInvocation.proceed();
                long end = System.currentTimeMillis();
                logTime(clazz, methodInvocation, (end - start));
                return returnValue;
            };
            proxyFactory.addAdvice(interceptor);
            return proxyFactory.getProxy();
        }
        return bean;
    }

    private void logTime(Class<?> clazz, MethodInvocation methodInvocation, long time) {
        LOG.log(Level.INFO, clazz.getName()
                + "#"
                + methodInvocation.getMethod().getName()
                + " execution time: " + (time));
    }

    private boolean isContainAnnotation(Class clazz) {
        if (clazz != null) {
            Method[] methods = clazz.getDeclaredMethods();
            return Arrays.stream(methods).anyMatch(m -> m.isAnnotationPresent(LogExecutionTime.class));
        }
        return false;
    }
}
