package com.beauty.beauty.web.filter;

import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.util.UserUtil;
import org.springframework.stereotype.Service;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;

public class FilterService {

    public static boolean checkAccess(ServletRequest servletRequest, Role... accessRoles) {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpSession session = httpServletRequest.getSession();
        if (session != null) {
            Role role = UserUtil.getUserFromSession(session).getRole();
            Role res = Arrays.stream(accessRoles).filter(r -> r == role).findAny().orElse(null);
            if (res != null) {
                return true;
            }
        }
        return false;
    }
}
