package com.beauty.beauty.web.filter;

import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.db.entity.User;
import com.beauty.beauty.util.UserUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginedUserFilter implements Filter{

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //do nothing
    }

    @Override
    public void destroy() {
        //do nothing
    }

    /**
     * prohibits access if the current user's doesn't have
     * unregistered role and redirect to logout page
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpSession session = httpServletRequest.getSession();
        if (session != null) {
            User user = UserUtil.getUserFromSession(session);
            if (user != null && Role.Unregistered != user.getRole()) {
            httpServletRequest.getRequestDispatcher("/logout")
                    .forward(httpServletRequest, servletResponse);
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
