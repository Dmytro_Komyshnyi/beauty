package com.beauty.beauty.web.filter;

import com.beauty.beauty.db.entity.Role;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RegisteredUserFilter implements Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain)
            throws IOException, ServletException {
        if (FilterService.checkAccess(req, Role.Admin, Role.Master, Role.Client)) {
            filterChain.doFilter(req, resp);
            return;
        }
        HttpServletResponse httpServletResponse = (HttpServletResponse) resp;
        httpServletResponse.sendError(404);
    }

    @Override
    public void init(FilterConfig filterConfig) {
        //do nothing
    }

    @Override
    public void destroy() {
        //do nothing
    }
}
