package com.beauty.beauty.web.controller;

import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.service.AppointmentService;
import com.beauty.beauty.web.command.CommandGetSchedule;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/master")
public class MasterController {

    private static final Logger LOG = Logger.getLogger(MasterController.class);
    @Qualifier("commandGetAppointmentMaster")
    private CommandGetSchedule commandGetSchedule;
    private AppointmentService appointmentService;



    public MasterController(CommandGetSchedule commandGetSchedule,
                            AppointmentService appointmentService) {
        this.commandGetSchedule = commandGetSchedule;
        this.appointmentService = appointmentService;
    }

    @GetMapping("appointments")
    public void getAppointments(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        appointmentService.getAppointments(req, resp, Role.Master,
                5, "pageAppointments_ms",
                "appointments_ms", "availablePage_ms",
                "/master_client");
    }

    @PostMapping("cancel_appointment")
    public void cancelAppointment(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        appointmentService.cancel(req, resp, Role.Master, "/master_client");
    }

    @PostMapping("complete_appointment")
    public void completeAppointment(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        appointmentService.complete(req, resp);
    }

    @GetMapping("schedule")
    public void getSchedule(HttpServletRequest req, HttpServletResponse resp) {
        try {
            commandGetSchedule.execute(req, resp);
        } catch (ServletException | IOException e) {
            LOG.warn("#getSchedule: can not get schedule");
        }
    }
}
