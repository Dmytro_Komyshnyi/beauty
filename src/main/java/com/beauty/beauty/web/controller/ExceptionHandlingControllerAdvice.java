package com.beauty.beauty.web.controller;

import org.apache.log4j.Logger;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;


@ControllerAdvice
public class ExceptionHandlingControllerAdvice {

    private static final Logger log = Logger.getLogger(ExceptionHandlingControllerAdvice.class);

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception exception) throws Exception {
        if (AnnotationUtils.findAnnotation(exception.getClass(),
                ResponseStatus.class) != null)
            throw exception;
        log.error("Request: " + req.getRequestURI() + " throw " + exception);
        ModelAndView view = new ModelAndView();
        view.setViewName("/error/errorPage500");
        return view;
    }
}