package com.beauty.beauty.web.controller;

import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.service.AppointmentService;
import com.beauty.beauty.web.command.CommandEvaluate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/client")
public class ClientController {

    private AppointmentService appointmentService;
    private CommandEvaluate commandEvaluate;

    @Autowired
    public ClientController(AppointmentService appointmentService,
                            CommandEvaluate commandEvaluate) {
        this.appointmentService = appointmentService;
        this.commandEvaluate = commandEvaluate;
    }

    @PostMapping("/add_appointment")
    public void addAppointment(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        appointmentService.add(req, resp);
    }

    @PostMapping("/cancel_appointment")
    public void cancelAppointment(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        appointmentService.cancel(req, resp, Role.Master, "/user_client");

    }

    @GetMapping("/appointments")
    public void getAppointments(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        appointmentService.getAppointments(req, resp, Role.Client,
                5, "pageAppointments_cl",
                "appointments_cl", "availablePage_cl",
                "/user_client");
    }

    @PostMapping("/evaluate")
    public void evaluateAppointment(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        commandEvaluate.execute(req, resp);
    }

}
