package com.beauty.beauty.web.controller;

import com.beauty.beauty.service.LogInService;
import com.beauty.beauty.util.CaptchaVerifyUtil;
import com.beauty.beauty.web.command.CommandRegistration;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/login")
public class LoginController {

    private static final Logger LOG = Logger.getLogger(LoginController.class);
    private CommandRegistration commandRegistration;
    private LogInService logInService;

    @Autowired
    public LoginController(CommandRegistration commandRegistration, LogInService logInService) {
        this.commandRegistration = commandRegistration;
        this.logInService = logInService;
    }

    @PostMapping("/sign_in")
    public void login(HttpServletRequest req, HttpServletResponse resp,
                      @RequestParam("login") String login,
                      @RequestParam("password") String password,
                      @RequestParam("g-recaptcha-response") String recaptcha) {
        try {
            if (CaptchaVerifyUtil.verify(recaptcha)) {
                logInService.authorization(req, resp, password, login);
            } else {
                resp.sendRedirect("/sign_in?info=not valid reCaptcha");
            }
        } catch (IOException e) {
            LOG.warn("#login: can not login user");
        }
    }

    @PostMapping("/registration")
    public void register(HttpServletRequest req, HttpServletResponse resp) {
        try {
            commandRegistration.execute(req, resp);
        } catch (ServletException | IOException e) {
            LOG.warn("#register: can not register user");
        }
    }

    @GetMapping("/logout")
    public void logout(HttpServletRequest req, HttpServletResponse resp) {
        try {
            logInService.logout(req, resp);
        } catch (IOException e) {
            LOG.error("#logout: can not logout user");
        }
    }
}