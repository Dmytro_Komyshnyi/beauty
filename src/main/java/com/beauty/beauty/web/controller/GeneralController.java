package com.beauty.beauty.web.controller;

import com.beauty.beauty.web.command.CommandGetServiceAndMaster;
import com.beauty.beauty.web.command.CommandGetTimeSlot;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class GeneralController {

    private CommandGetServiceAndMaster commandGetServiceAndMaster;
    private CommandGetTimeSlot commandGetTimeSlot;
    private static final String RU = "ru";
    private static final String EN = "en";

    @Autowired
    public GeneralController(CommandGetServiceAndMaster commandGetServiceAndMaster,
                             CommandGetTimeSlot commandGetTimeSlot) {
        this.commandGetServiceAndMaster = commandGetServiceAndMaster;
        this.commandGetTimeSlot = commandGetTimeSlot;
    }

    @GetMapping("/services_masters")
    public void getServicesAndMasters(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        commandGetServiceAndMaster.execute(req, resp);
    }

    @PostMapping("/time_slots")
    public void getTimeSlots(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        commandGetTimeSlot.execute(req, resp);
    }

    @GetMapping("/locale")
    public void changeLocale(HttpServletRequest req, HttpServletResponse resp) {
        String lang = req.getParameter("lang");
        if (RU.equals(lang)) {
            req.getSession().setAttribute("locale", RU);
        } else if (EN.equals(lang)) {
            req.getSession().setAttribute("locale", EN);
        }
    }

    @GetMapping("/test")
    public void test() {
        throw new RuntimeException();
    }
}
