package com.beauty.beauty.web.controller;

import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.service.AppointmentService;
import com.beauty.beauty.web.command.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private static final Logger LOG = Logger.getLogger(AdminController.class);
    private CommandGetAdminFreeSLots commandGetAdminFreeSLots;
    private CommandRegisterMaster commandRegisterMaster;
    private CommandGetWorkers commandGetWorkers;
    private CommandGetWorkerSchedule commandGetWorkerSchedule;
    private CommandAddWorkerDate commandAddWorkerDate;
    private AppointmentService appointmentService;

    @Autowired
    public AdminController(CommandRegisterMaster commandRegisterMaster,
                           CommandGetAdminFreeSLots commandGetAdminFreeSLots,
                           CommandGetWorkers commandGetWorkers,
                           CommandGetWorkerSchedule commandGetWorkerSchedule,
                           CommandAddWorkerDate commandAddWorkerDate,
                           AppointmentService appointmentService) {
        this.commandRegisterMaster = commandRegisterMaster;
        this.commandGetAdminFreeSLots = commandGetAdminFreeSLots;
        this.commandGetWorkers = commandGetWorkers;
        this.commandGetWorkerSchedule = commandGetWorkerSchedule;
        this.commandAddWorkerDate = commandAddWorkerDate;
        this.appointmentService = appointmentService;
    }

    @GetMapping("/appointment")
    public void getAppointments(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        appointmentService.getAdminAppointment(req, resp, 5);
    }

    @PostMapping("/cancel_appointment")
    public void cancelAppointment(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        appointmentService.cancel(req, resp, Role.Admin, "admin_client");
    }

    @PostMapping("/accept_appointment")
    public void acceptAppointment(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        appointmentService.accept(req, resp);
    }

    @PostMapping("/free_slots")
    public void getFreeSlots(HttpServletRequest req, HttpServletResponse resp) {
        try {
            commandGetAdminFreeSLots.execute(req, resp);
        } catch (ServletException | IOException e) {
            LOG.warn("#getFreeSlots: can not get free slots", e);
        }
    }

    @PostMapping("change_time")
    public void changeAppointmentTime(HttpServletRequest req, HttpServletResponse resp) throws IOException {
            appointmentService.changeTime(req, resp);
    }

    @PostMapping("register_master")
    public void registerMaster(HttpServletRequest req, HttpServletResponse resp) {
        try {
            commandRegisterMaster.execute(req, resp);
        } catch (ServletException | IOException e) {
            LOG.warn("#registerMaster: can not register master", e);
        }
    }

    @GetMapping("workers")
    public void getWorkers(HttpServletRequest req, HttpServletResponse resp) {
        try {
            commandGetWorkers.execute(req, resp);
        } catch (ServletException | IOException e) {
            LOG.warn("#getWorkers: can not get workers", e);
        }
    }

    @PostMapping("workers_schedule")
    public void getWorkerSchedule(HttpServletRequest req, HttpServletResponse resp) {
        try {
            commandGetWorkerSchedule.execute(req, resp);
        } catch (ServletException | IOException e) {
            LOG.warn("#getWorkerSchedule: can not get worker schedule", e);
        }
    }

    @PostMapping("add_worker_date")
    public void addWorkerDate(HttpServletRequest req, HttpServletResponse resp) {
        try {
            commandAddWorkerDate.execute(req, resp);
        } catch (ServletException | IOException e) {
            LOG.warn("#addWorkerDate: can not add worker date", e);
        }
    }
}

