package com.beauty.beauty.web.command;

import com.beauty.beauty.db.dao.UserDAO;
import com.beauty.beauty.db.dao.UserDaoException;
import com.beauty.beauty.db.entity.Worker;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Gets users with role id equals to 3(master)
 */
@Service
public class CommandGetWorkers implements Command {

    private static final Logger LOG = Logger.getLogger(CommandGetWorkers.class);
    private static final String ATTR_WORKERS = "workers";
    private static final String SUCCESS_REDIRECT = "/workers";
    private UserDAO USER_DAO;

    @Autowired
    public CommandGetWorkers(UserDAO USER_DAO) {
        this.USER_DAO = USER_DAO;
    }

    /**
     * Set workers to session
     */
    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HttpSession session = req.getSession();
            List<Worker> workers = USER_DAO.getMasters();
            session.setAttribute(ATTR_WORKERS, workers);
            resp.sendRedirect(SUCCESS_REDIRECT);
        } catch (UserDaoException e) {
            resp.sendError(400);
            LOG.warn("#execute can not get workers", e);
        }
    }
}
