package com.beauty.beauty.web.command;

import com.beauty.beauty.db.dao.AppointmentDAO;
import com.beauty.beauty.db.dao.AppointmentDaoException;
import com.beauty.beauty.db.entity.Evaluation;
import com.beauty.beauty.db.entity.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Service
public class CommandEvaluate implements Command {

    private static final Logger LOG = Logger.getLogger(CommandEvaluate.class);
    private static final String PARAM_APPOINTMENT_ID = "evaluation_id";
    private static final String PARAM_MARK = "mark";
    private static final String LOGIN_REDIRECT = "/beauty_war_exploded/sign_in";
    private AppointmentDAO appointmentDAO;

    @Autowired
    public CommandEvaluate(AppointmentDAO appointmentDAO) {
        this.appointmentDAO = appointmentDAO;
    }

    /**
     * Update evaluation if user has evaluation which belongs him
     */
    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int app_id = Integer.parseInt(req.getParameter(PARAM_APPOINTMENT_ID));
            int mark = Integer.parseInt(req.getParameter(PARAM_MARK));
            HttpSession session = req.getSession();
            Object ob = session.getAttribute("current_user");
            if (ob != null && ob.getClass() == User.class) {
                User user = (User) ob;
                Evaluation evaluation = appointmentDAO.getEvaluation(app_id);
                if (evaluation.getAppointment().getUser().equals(user)) {
                    evaluation.setEvaluation(mark);
                    appointmentDAO.updateEvaluation(evaluation);
                    resp.sendRedirect("/beauty_war_exploded");
                    return;
                }
                resp.sendError(400);
                return;
            }
            resp.sendRedirect(LOGIN_REDIRECT);
        } catch (NumberFormatException e) {
            LOG.warn("#execute: illegal user's params appointment id["
                    +req.getParameter(PARAM_APPOINTMENT_ID)+"] mark["+req.getParameter(PARAM_MARK)+"]");
            resp.sendError(400);
        } catch (AppointmentDaoException e) {
            resp.sendError(400);
            LOG.warn("#execute: can not ");
        }
    }
}
