package com.beauty.beauty.web.command;

import com.beauty.beauty.db.dao.UserDAO;
import com.beauty.beauty.db.dao.UserDaoException;
import com.beauty.beauty.db.entity.WorkDate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

@Service
public class CommandAddWorkerDate implements Command {

    private static final String PARAM_USER_DATE = "user_date";
    private static final String ATTR_WORKER_ID = "worker_id";
    private static final String ATTR_SCHEDULE = "schedule";
    private static final String REDIRECT_PAGE = "/worker_info";
    private static final String REDIRECT_PAGE_NOT_WORKER_ID = "/workers";
    public static final Logger LOG = Logger.getLogger(CommandAddWorkerDate.class);
    private UserDAO userDAO;

    @Autowired
    public CommandAddWorkerDate(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    /**
     * Gets worker's id from session by attribute {@link CommandAddWorkerDate#ATTR_WORKER_ID} and set him new work hours
     * in date from request
     */
    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Object obId = session.getAttribute(ATTR_WORKER_ID);
        Date date = null;
        try {
            if (obId != null && obId.getClass() == Integer.class) {
                int id = (Integer) obId;
                String strDate = req.getParameter(PARAM_USER_DATE);
                date = Date.valueOf(strDate);
                userDAO.addWorkerDate(id, date);
                List<WorkDate> schedule = userDAO.getSchedule(id);
                session.setAttribute(ATTR_SCHEDULE, schedule);
                resp.sendRedirect(REDIRECT_PAGE);
            } else {
                resp.sendRedirect(REDIRECT_PAGE_NOT_WORKER_ID);
            }
        } catch (UserDaoException e) {
            LOG.warn("#execute can not add date for user with id ["+obId+"]", e);
            resp.sendError(400);
        }catch (NumberFormatException e) {
            LOG.warn("#execute illegal id ["+obId+"]", e);
            resp.sendError(400);
        }
        catch (IllegalArgumentException e) {
            LOG.warn("#execute illegal date ["+date+"]");
            resp.sendError(400);
        }
    }
}
