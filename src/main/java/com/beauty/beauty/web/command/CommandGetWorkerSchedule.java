package com.beauty.beauty.web.command;

import com.beauty.beauty.db.dao.UserDAO;
import com.beauty.beauty.db.dao.UserDaoException;
import com.beauty.beauty.db.entity.WorkDate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Service
public class CommandGetWorkerSchedule implements Command {

    private static final String REDIRECT_PAGE = "/worker_info";
    private static final Logger LOG = Logger.getLogger(CommandGetWorkerSchedule.class);
    private static final String ATTR_SCHEDULE = "schedule";
    private static final String ATTR_WORKER_ID = "worker_id";
    private UserDAO userDAO;

    @Autowired
    public CommandGetWorkerSchedule(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    /**
     * Sets worker's schedule to user's session and redirect to page {@link CommandGetWorkerSchedule#REDIRECT_PAGE}
     */
    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = -1;
        try {
            id = Integer.parseInt(req.getParameter(ATTR_WORKER_ID));
            List<WorkDate> schedule = userDAO.getSchedule(id);
            HttpSession session = req.getSession();
            session.setAttribute(ATTR_SCHEDULE, schedule);
            session.setAttribute(ATTR_WORKER_ID, id);
            resp.sendRedirect(REDIRECT_PAGE);
        } catch (UserDaoException e) {
            LOG.warn("#execute: can not get schedule id["+id+"]");
            resp.sendError(400);
        } catch (NumberFormatException e) {
            LOG.warn("#execute: can not parse user's id["+req.getParameter(ATTR_WORKER_ID)+"]");
        }
    }
}
