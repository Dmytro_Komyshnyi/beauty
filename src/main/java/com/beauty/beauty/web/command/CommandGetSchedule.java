package com.beauty.beauty.web.command;

import com.beauty.beauty.db.dao.UserDAO;
import com.beauty.beauty.db.dao.UserDaoException;
import com.beauty.beauty.db.entity.User;
import com.beauty.beauty.db.entity.WorkDate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Service
public class CommandGetSchedule implements Command {

    private UserDAO userDAO;
    private static final Logger LOG = Logger.getLogger(CommandGetSchedule.class);
    private static final String ATTR_CURRENT_USER = "current_user";
    private static final String ATTR_SCHEDULE = "schedule";

    @Autowired
    public CommandGetSchedule(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    /**
     * Gives personal schedule for the current user with role master
     */
    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute(ATTR_CURRENT_USER);
        if (user != null) {
            try {
                List<WorkDate> date = userDAO.getSchedule(user);
                session.setAttribute(ATTR_SCHEDULE, date);
                resp.sendRedirect("/master_schedule");
            } catch (UserDaoException e) {
                LOG.error("can not get schedule for user["+user+"]");
                resp.sendError(400);
            }
        }
    }
}
