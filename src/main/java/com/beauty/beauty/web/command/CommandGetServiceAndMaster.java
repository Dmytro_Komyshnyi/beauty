package com.beauty.beauty.web.command;

import com.beauty.beauty.db.dao.ServiceDAO;
import com.beauty.beauty.db.dao.ServiceDaoException;
import com.beauty.beauty.db.dao.UserDAO;
import com.beauty.beauty.db.dao.UserDaoException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Service
public class CommandGetServiceAndMaster implements Command {

    Logger LOG = Logger.getLogger(CommandGetServiceAndMaster.class);
    private ServiceDAO serviceDAO;
    private UserDAO userDAO;

    @Autowired
    public CommandGetServiceAndMaster(ServiceDAO serviceDAO, UserDAO userDAO) {
        this.serviceDAO = serviceDAO;
        this.userDAO = userDAO;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HttpSession session = req.getSession();
            session.setAttribute("services", serviceDAO.getAllService());
            session.setAttribute("masters", userDAO.getMasters());
            resp.sendRedirect("/services?sort=name&ask=true");
        } catch (ServiceDaoException e) {
            LOG.error("#execute can not invoke ServiceDao#getAllService", e);
            resp.sendError(500);
        } catch (UserDaoException e) {
            LOG.error("#execute can not invoke UserDAO#getMasters", e);
            resp.sendError(500);
        }
    }
}
