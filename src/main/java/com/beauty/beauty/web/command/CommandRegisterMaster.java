package com.beauty.beauty.web.command;

import com.beauty.beauty.db.dao.UserDAO;
import com.beauty.beauty.db.dao.UserDaoException;
import com.beauty.beauty.db.dao.mySQL.MySQLUser;
import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.db.entity.User;
import com.beauty.beauty.service.LogInService;
import com.beauty.beauty.service.ServletServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public class CommandRegisterMaster implements Command {

    private static final Logger LOG = Logger.getLogger(CommandRegistration.class);
    private static final UserDAO userDAO = MySQLUser.getInstance();
    private LogInService logInService;

    @Autowired
    public CommandRegisterMaster(LogInService logInService) {
        this.logInService = logInService;
    }

    /**
     * Create new user with master role {@link Role#Master}
     */
    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            User user = logInService.createUser(req, Role.Master);
            userDAO.addUser(user);
            req.getSession().setAttribute(INFO_MESSAGE, "Success");
            resp.sendRedirect("/register_master?info=Added new master");
        } catch (ServletServiceException e) {
            LOG.warn("#execute: did not registers new master", e);
            resp.sendRedirect("/register_master?info=Something went wrong");
        } catch (UserDaoException e) {
            LOG.warn("#execute: did not registers new master", e);
            resp.sendRedirect("/register_master?info=This user already exist");
        }
    }
}
