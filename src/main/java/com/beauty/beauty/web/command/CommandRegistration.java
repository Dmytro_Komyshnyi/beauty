package com.beauty.beauty.web.command;

import com.beauty.beauty.db.dao.UserDAO;
import com.beauty.beauty.db.dao.UserDaoException;
import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.db.entity.User;
import com.beauty.beauty.service.LogInService;
import com.beauty.beauty.service.ServletServiceException;
import com.beauty.beauty.util.CaptchaVerifyUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Registration a new user with client role
 */
@Service
public class CommandRegistration implements Command {

    private static final Logger LOG = Logger.getLogger(CommandRegistration.class);
    private static final String ATTR_RECAPTCHA_RESP = "g-recaptcha-response";
    private LogInService logInService;
    private UserDAO userDAO;

    @Autowired
    public CommandRegistration(LogInService logInService, UserDAO userDAO) {
        this.logInService = logInService;
        this.userDAO = userDAO;
    }

    /**
     * Create a new user with client role {@link Role#Client} and put it to the user's session
     * check date and use reCaptcha
     */
    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String gRecaptchaResponse = req.getParameter(ATTR_RECAPTCHA_RESP);
        boolean isValid = CaptchaVerifyUtil.verify(gRecaptchaResponse);
        if (isValid) {
            registration(req, resp);
        } else {
            resp.sendRedirect("/login?"+INFO_MESSAGE + "=not valid reCaptcha");
        }
    }

    private void registration(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        try {
            User user = logInService.createUser(req, Role.Client);
            userDAO.addUser(user);
            session.setAttribute("current_user", user);
            resp.sendRedirect("/");
        } catch (ServletServiceException e) {
            resp.sendRedirect("/login?info="+"");
            LOG.warn("#execute: did not registers new user", e);
        } catch (UserDaoException e) {
            LOG.warn("#execute: did not registers new user", e);
            resp.sendRedirect("/login");
        }
    }
}
