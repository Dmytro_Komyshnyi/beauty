package com.beauty.beauty.web.command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract interface Command {
    String INFO_MESSAGE = "info";
    public abstract void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException;
}
