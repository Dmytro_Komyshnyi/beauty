package com.beauty.beauty.web.command;

import com.beauty.beauty.db.bean.TimeSlotBean;
import com.beauty.beauty.service.ServletServiceException;
import com.beauty.beauty.service.TimeService;
import com.beauty.beauty.util.Util;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

@Service
public class CommandGetTimeSlot implements Command {

    private static final Logger LOG = Logger.getLogger(CommandGetTimeSlot.class);
    private static final String ILLEGAL_DATE_MESSAGE = "Choose available date";
    private static final String ILLEGAL_SERVICE_MESSAGE = "Choose service";
    private static final String ILLEGAL_MASTER_MESSAGE = "Choose master";
    private TimeService timeService;

    @Autowired
    public CommandGetTimeSlot(TimeService timeService) {
        this.timeService = timeService;
    }

    /**
     * This method send to user free time slots for service and date which were taken from the session.
     * Should send parameters: the date(param "user_date"),
     * master's id(param "master_id") and service's name(param "service_name")
     */
    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            String serviceName = req.getParameter("service_name");
            String strMasterId = req.getParameter("master_id");
            String strDate = req.getParameter("user_date");
            if (validateParams(resp, serviceName, strMasterId, strDate)) {
                return;
            }
            Date date = Date.valueOf(strDate);
            if (!Util.isToday(date)) {
                resp.sendRedirect("/services?" + INFO_MESSAGE + "=" + ILLEGAL_DATE_MESSAGE);
                return;
            }
            insertTimeSlot(req, serviceName, strMasterId, date);
            resp.sendRedirect("/choose_time");
        } catch (NumberFormatException e) {
            LOG.warn("#execute: illegal user format number", e);
            resp.sendError(400);
        } catch (IllegalArgumentException e) {
            LOG.warn("#execute: illegal user date", e);
            resp.sendError(400);
        } catch (ServletServiceException e) {
            LOG.warn("#execute: user illegal data", e);
            resp.sendError(400);
        }
    }

    private void insertTimeSlot(HttpServletRequest req, String serviceName, String strMasterId, Date date)
            throws ServletServiceException {
        int masterId = Integer.parseInt(strMasterId);
        List<TimeSlotBean> list = timeService.getFreeTimeSlots(date, masterId, serviceName);
        req.getSession().setAttribute("time_slots", list);
        HttpSession session = req.getSession();
        session.setAttribute("master_id", masterId);
        session.setAttribute("user_service_name", serviceName);
        session.setAttribute("user_date", date);
    }

    private boolean validateParams(HttpServletResponse resp, String serviceName, String strMasterId, String strDate)
            throws IOException {
        return (!(isNotNull(strMasterId, resp, ILLEGAL_MASTER_MESSAGE)
                        && isNotNull(serviceName, resp, ILLEGAL_SERVICE_MESSAGE)
                        && isNotNull(strDate, resp, ILLEGAL_SERVICE_MESSAGE)));
    }

    private boolean isNotNull(Object ob, HttpServletResponse resp, String userMessage) throws IOException {
        if (ob == null) {
            resp.sendRedirect("/services?info=" + userMessage);
            return false;
        }
        return true;
    }
}
