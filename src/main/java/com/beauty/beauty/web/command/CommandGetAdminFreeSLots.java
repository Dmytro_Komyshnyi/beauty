package com.beauty.beauty.web.command;

import com.beauty.beauty.db.bean.TimeSlotBean;
import com.beauty.beauty.db.dao.AppointmentDAO;
import com.beauty.beauty.db.dao.AppointmentDaoException;
import com.beauty.beauty.db.entity.Appointment;
import com.beauty.beauty.service.ServletServiceException;
import com.beauty.beauty.service.TimeService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Service
public class CommandGetAdminFreeSLots implements Command {

    private AppointmentDAO appointmentDAO;
    private TimeService timeService;
    private static final Logger LOG = Logger.getLogger(CommandGetAdminFreeSLots.class);
    private static final String PARAM_APPOINTMENT_ID = "appointment_id";
    private static final String ATTR_APPOINTMENT_ID = "change_appointment_id";
    private static final String ATTR_TIME_SLOTS = "time_slots";

    @Autowired
    public CommandGetAdminFreeSLots(AppointmentDAO appointmentDAO, TimeService timeService) {
        this.appointmentDAO = appointmentDAO;
        this.timeService = timeService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int appId = Integer.parseInt(req.getParameter(PARAM_APPOINTMENT_ID));
            Appointment app = appointmentDAO.getAppointment(appId);
            List<TimeSlotBean> slots = timeService.getFreeTimeSlots(
                    app.getDate(), app.getWorker().getId(),app.getService().getName(),
                    new TimeSlotBean(app.getTimeStart(), app.getTimeEnd()));
            HttpSession session = req.getSession();
            session.setAttribute(ATTR_APPOINTMENT_ID, appId);
            session.setAttribute(ATTR_TIME_SLOTS, slots);
            resp.sendRedirect("/change_time");
        } catch (NumberFormatException | ServletServiceException | AppointmentDaoException e) {
            LOG.warn("Illegal appointment id["+req.getParameter(PARAM_APPOINTMENT_ID), e);
            resp.sendError(400);
        }
    }
}
