package com.beauty.beauty.web.listener;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

@Component
public class ContextListener implements ServletContextListener {

    private static final Logger LOG = Logger.getLogger(ContextListener.class);

    /**
     * initialize Log4J
     */
    @Override
    public void contextInitialized(ServletContextEvent event) {
        try {
            ServletContext servletContext = event.getServletContext();
            PropertyConfigurator.configure(servletContext.getRealPath(
                    "WEB-INF/log4j.properties"));
            LOG.info("#initLog4J: initialize Log4J");
        } catch (Exception ex) {
            LOG.error("#initLog4J: can not initialize Log4J");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        //do nothing
    }
}
