package com.beauty.beauty.web.tag;

import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class MoneyFormat extends SimpleTagSupport {

    String value;

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public void doTag() throws IOException {
        String money = value.substring(0, value.length() - 2) + '.' + value.substring(value.length() - 2);
        getJspContext().getOut().println(money);
    }
}
