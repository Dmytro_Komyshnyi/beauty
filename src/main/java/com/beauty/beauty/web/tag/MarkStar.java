package com.beauty.beauty.web.tag;

import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class MarkStar extends SimpleTagSupport {

    private static final String STAR = "\u2605";
    private static final String EMPTY_STAR = "\u2606";

    /**
     * number of star
     * Round rules
     * 0.5 -> 1: 0.4 -> 0
     */
    double mark;

    /**
     * print mark times star into JSP if mark less then ten print empty star
     * but not more then ten signs
     */
    @Override
    public void doTag() throws IOException {
        long counter = Math.round(mark);
        for (int i = 0; i < 10; i++) {
            if (counter > 1) {
                getJspContext().getOut().println(STAR);
                counter--;
            } else {
                getJspContext().getOut().println(EMPTY_STAR);
            }
        }
    }

    public double getMark() {
        return mark;
    }

    public void setMark(double mark) {
        this.mark = mark;
    }
}
