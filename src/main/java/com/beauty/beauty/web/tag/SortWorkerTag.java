package com.beauty.beauty.web.tag;

import com.beauty.beauty.db.entity.Worker;

import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortWorkerTag extends SimpleTagSupport {

    private List<Worker> workerList;
    private String attrName;
    private String typeSort;
    private boolean ask = true;
    private static final String MARK_TYPE = "mark";
    private static final String NAME_TYPE = "name";


    /**
     * Sort {@link List<Worker>}
     */
    @Override
    public void doTag() {
        List<Worker> list = null;
        if (MARK_TYPE.equals(typeSort)) {
            if (ask) {
                list = workerList.stream().sorted(Comparator.comparing(Worker::getMark))
                        .collect(Collectors.toList());
            } else {
                list = workerList.stream().sorted(Comparator.comparing(Worker::getMark).reversed())
                        .collect(Collectors.toList());
            }
        } else {
            if (ask) {
                list = workerList.stream().sorted(Comparator.comparing(Worker::getName))
                        .collect(Collectors.toList());
            } else {
                list = workerList.stream().sorted(Comparator.comparing(Worker::getName).reversed())
                        .collect(Collectors.toList());
            }
        }
        getJspContext().setAttribute(attrName, list);
    }


    /**
     * @return {@link SortWorkerTag#typeSort}
     */
    public String getTypeSort() {
        return typeSort;
    }

    /**
     * @param typeSort should be {@link SortWorkerTag#MARK_TYPE} or {@link SortWorkerTag#NAME_TYPE}
     */
    public void setTypeSort(String typeSort) {
        this.typeSort = typeSort;
    }

    /**
     * @return {@link SortWorkerTag#attrName} attribute name with sorted list
     */
    public String getAttrName() {
        return attrName;
    }

    /**
     * @param attrName the name of the attribute in which the sorted list will be put
     */
    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    /**
     * @return {@link SortWorkerTag#workerList} sorted list
     */
    public List<Worker> getWorkerList() {
        return workerList;
    }

    public void setWorkerList(List<Worker> workerList) {
        this.workerList = workerList;
    }

    /**
     * @return {@link SortWorkerTag#ask} sort order selection
     */
    public boolean isAsk() {
        return ask;
    }

    /**
     * @param ask sort order selection
     */
    public void setAsk(boolean ask) {
        this.ask = ask;
    }
}
