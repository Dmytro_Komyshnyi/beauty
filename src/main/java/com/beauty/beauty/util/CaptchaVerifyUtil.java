package com.beauty.beauty.util;

import org.apache.log4j.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.net.ssl.HttpsURLConnection;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

/**
 * Util class for validation user's request by reCaptcha.v2.
 */
public class CaptchaVerifyUtil {

    public static final String SITE_VERIFY_URL =
            "https://www.google.com/recaptcha/api/siteverify";
    private static final Logger LOG = Logger.getLogger(CaptchaVerifyUtil.class);
    //Google keys
    /**
     * Site key for reCaptcha.v2
     */
    public static final String SITE_KEY ="6LeQTW4aAAAAABof7U_4ECv1zBK35hyz8gfTU5e1";

    /**
     * Secret key for reCaptcha.v2
     */
    private static final String SECRET_KEY ="6LeQTW4aAAAAALjT8GCheUv4UhnoxyI4-HlS8rbv";

    /**
     * Validate user's response by Google recaptcha.v2 API.
     * @param gRecaptchaResponse user's response for validation by google recaptcha v2.
     * @return return result of validation. If mark better then 0.5 return true otherwise return false.
     */
    public static boolean verify(String gRecaptchaResponse) {
        if (gRecaptchaResponse == null || gRecaptchaResponse.length() == 0) {
            return false;
        }
        try {
            HttpsURLConnection conn = (HttpsURLConnection) new URL(SITE_VERIFY_URL).openConnection();
            conn.setRequestMethod("POST");
            String postParams = "secret=" + SECRET_KEY
                    + "&response=" + gRecaptchaResponse;
            conn.setDoOutput(true);
            OutputStream outStream = conn.getOutputStream();
            outStream.write(postParams.getBytes());
            outStream.flush();
            outStream.close();
            InputStream is = conn.getInputStream();
            JsonReader jsonReader = Json.createReader(is);
            JsonObject jsonObject = jsonReader.readObject();
            jsonReader.close();
            return jsonObject.getBoolean("success");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
