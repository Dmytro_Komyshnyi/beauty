package com.beauty.beauty.util;

import javax.servlet.http.HttpSession;
import java.sql.Date;

public class Util {

    private Util() {
    }


    /**
     * checks the object for null and for casting to the string
     */
    public static boolean hasCastToString(Object... ob) {
        for (Object o : ob) {
            if (o == null || o.getClass() != String.class || ((String) o).isEmpty()) {
                return false;
            }
        }
        return true;
    }


    /**
     * @param attribute this attribute will be deleted from session
     * @param session delete attribute from this session
     */
    public static void cleanSession(HttpSession session, String... attribute) {
        for (String str : attribute) {
            session.removeAttribute(str);
        }
    }

    /**
     * Check date today or less then today plus 14 days
     * @param date not null
     */
    public static boolean isToday(Date date) {
        Date nowWithHours = new Date(System.currentTimeMillis());
        Date now = Date.valueOf(nowWithHours.toString());
        Date max = new Date(now.getTime() + 1209600000);
        return !(now.getTime() > date.getTime() || date.getTime() > max.getTime());
    }
}
