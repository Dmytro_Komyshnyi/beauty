package com.beauty.beauty.util;

import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.db.entity.User;

import javax.servlet.http.HttpSession;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserUtil {

    private static final Pattern PASSWORD_PATTERN = Pattern.compile("^([\\w@\\.]{8,40})$");
    private static final Pattern LOGIN_PATTERN = Pattern.compile("^([\\w]+@[\\w]+\\.[\\w]+)$");
    private static final Pattern PHONE_PATTERN = Pattern.compile("^\\+?([\\d]{10,12}$)");
    private static final String ATTR_USER = "current_user";


    public static Role getRole (int roleId) {
        switch (roleId) {
            case 1: return Role.Admin;
            case 2: return Role.Client;
            case 3: return Role.Master;
            case -1: return Role.Unregistered;
        }
        throw new IllegalArgumentException("Role with this id does not exist");
    }

    /**
     * @param password not null
     * @return validate phone by regex ^([\\w@\\.]{8,40})$
     */
    public static boolean hasValidPassword(String password) {
        if (password.length() <= 40 && password.length() >= 8) {
            Matcher matcher = PASSWORD_PATTERN.matcher(password);
            return matcher.matches();
        }
        return false;
    }

    /**
     * @param login not null
     * @return validate phone by regex ^([\w]+@[\w]+\.[\w]+)$
     */
    public static boolean hasValidLogin(String login) {
        Matcher matcher = LOGIN_PATTERN.matcher(login);
        return matcher.matches();
    }


    /**
     * @param phone not null
     * @return validate phone by regex ^\+?([\d]{10,12}$)
     */
    public static boolean hasValidPhone(String phone) {
        Matcher matcher = PHONE_PATTERN.matcher(phone);
        return matcher.matches();
    }

    /**
     * @param session user's session
     * @return user from session otherwise create
     * new user with Unregistered role and put it in session
     */
    public static User getUserFromSession(HttpSession session) {
        Object ob = session.getAttribute(ATTR_USER);
        if (ob != null && ob.getClass() == User.class) {
            return (User) ob;
        }
        User user =  new User(null, null, Role.Unregistered, null, null);
        session.setAttribute(ATTR_USER, user);
        return user;
    }
}
