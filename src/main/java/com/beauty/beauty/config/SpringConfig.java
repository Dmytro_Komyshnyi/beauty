package com.beauty.beauty.config;

import com.beauty.beauty.db.dao.AppointmentDAO;
import com.beauty.beauty.db.dao.ServiceDAO;
import com.beauty.beauty.db.dao.UserDAO;
import com.beauty.beauty.db.dao.mySQL.DAOFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.naming.NamingException;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.beauty.beauty")
@EnableAspectJAutoProxy
public class SpringConfig implements WebMvcConfigurer {

    private static ApplicationContext context;

    @Autowired
    public SpringConfig(ApplicationContext applicationContext) {
        context = applicationContext;
    }

    public static ApplicationContext getContext() {
        return context;
    }

    @Bean
    public InternalResourceViewResolver resolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean
    public UserDAO userDAO() throws NamingException {
        return DAOFactory.buildUserDAO();
    }

    @Bean
    public AppointmentDAO appointmentDAO() throws NamingException {
        return DAOFactory.buildAppointmentDAO();
    }

    @Bean
    public ServiceDAO serviceDAO() throws NamingException {
        return DAOFactory.buildServiceDAO();
    }

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        registry.viewResolver(resolver());
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/style/**")
                .addResourceLocations("/WEB-INF/style/");
    }


}
