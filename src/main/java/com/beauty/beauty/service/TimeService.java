package com.beauty.beauty.service;

import com.beauty.beauty.db.bean.TimeSlotBean;

import java.sql.Date;
import java.util.List;

public interface TimeService {

    List<TimeSlotBean> getFreeTimeSlots(
            Date date, int masterId, String serviceName, TimeSlotBean freeSlot) throws ServletServiceException;
    List<TimeSlotBean> getFreeTimeSlots(
            Date date, int masterId, String serviceName) throws ServletServiceException;
    String parseTime(String strTime);
    public boolean validateTime(TimeSlotBean userTime, List<TimeSlotBean> busySlots, TimeSlotBean workTime);
}
