package com.beauty.beauty.service;

import org.springframework.stereotype.Service;

@Service
public class PaginationServiceImpl implements PaginationService {

    /**
     * compute number of pages
     * @param rows number of rows
     * @param pageSize rows per page
     * @return number of pages
     */
    @Override
    public int calcAvailablePage(int rows, int pageSize) {
        float availablePage = (float) rows / pageSize;
        if (availablePage % 1 != 0) {
            availablePage++;
        }
        return (int) availablePage;
    }

    /**
     * @param strPageNum string should contains only number and page
     * @param availablePage number of available pages
     * @return if strPage exist return strPage another return last page or first page
     */
    @Override
    public int findPage (String strPageNum, int availablePage) {
        try {
            if (availablePage <= 0) {
                return 1;
            }
            int page = Integer.parseInt(strPageNum);
            if (page >= 1 && page <= availablePage) {
                return page;
            }
            if (page > availablePage) {
                return availablePage;
            }
            return 1;
        } catch (NumberFormatException e) {
            return 1;
        }
    }
}
