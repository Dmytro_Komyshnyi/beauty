package com.beauty.beauty.service;

public interface PaginationService {
    int findPage (String strPageNum, int availablePage);
    int calcAvailablePage(int rows, int pageSize);
}
