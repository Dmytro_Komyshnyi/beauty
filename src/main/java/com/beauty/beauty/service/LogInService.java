package com.beauty.beauty.service;

import com.beauty.beauty.aop.LogExecutionTime;
import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.db.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface LogInService {
    @LogExecutionTime
    User createUser(HttpServletRequest req, Role role) throws ServletServiceException;
    @LogExecutionTime
    void logout(HttpServletRequest req, HttpServletResponse resp) throws IOException;
    @LogExecutionTime
    void authorization(HttpServletRequest req, HttpServletResponse resp, String login, String password) throws IOException;
}
