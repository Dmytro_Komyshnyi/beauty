package com.beauty.beauty.service;

import com.beauty.beauty.db.bean.TimeSlotBean;
import com.beauty.beauty.db.dao.*;
import com.beauty.beauty.db.dao.mySQL.MySQLService;
import com.beauty.beauty.db.dao.mySQL.MySQLUser;
import com.beauty.beauty.db.entity.Appointment;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

@Service
public class ServletServiceImpl implements ServletService {

    private static final Logger LOG = Logger.getLogger(ServletServiceImpl.class);
    private static final ReentrantLock REENTRANT_LOCK = new ReentrantLock();
    private static ServiceDAO SERVICE_DAO = MySQLService.getInstance();
    private static UserDAO USER_DAO = MySQLUser.getInstance();
    @Autowired
    private AppointmentDAO appointmentDAO;
    @Autowired
    private TimeService timeService;

    /**
     * Update appointment only one quarry can change table in the same time
     * @param app should contains real id
     */
    @Override
    public boolean updateAppointment(Appointment app) {
        if ((app.getDate().getTime() + app.getTimeStart().getTime()) < System.currentTimeMillis()) {
            return false;
        }
        TimeSlotBean userTime = new TimeSlotBean(app.getTimeStart(), app.getTimeEnd());
        try {
            TimeSlotBean workTime =
                    USER_DAO.getWorkTimeByDate(app.getWorker().getId(), app.getDate());
            REENTRANT_LOCK.lock();
            List<TimeSlotBean> timesSlots = appointmentDAO.
                    getBusyTimeSlotOrderTimeStart(app.getDate(), app.getWorker().getId());
            if (timeService.validateTime(userTime, timesSlots, workTime)) {
                appointmentDAO.updateAppointment(app);
                return true;
            }
        } catch (AppointmentDaoException e) {
            LOG.error("#updateAppointment: can not update appointment["+app+"]");
        } catch (UserDaoException e) {
            LOG.warn("#updateAppointment: invoke getWorkTimeByDate workerId["+app.getWorker().getId()+
                    " date["+app.getDate()+"]");
        } finally {
            if (REENTRANT_LOCK.isLocked()) {
                REENTRANT_LOCK.unlock();
            }
        }
        return false;
    }

    /**
     * Update appointment only one quarry can change table in the same time
     */
    @Override
    public boolean addAppointment(Appointment app) {
        if ((app.getDate().getTime() + app.getTimeStart().getTime() + 2 * 60 * 60_000) < System.currentTimeMillis()) {
            return false;
        }
        TimeSlotBean userTime = new TimeSlotBean(app.getTimeStart(), app.getTimeEnd());
            try {
                TimeSlotBean workTime =
                        USER_DAO.getWorkTimeByDate(app.getWorker().getId(), app.getDate());
                REENTRANT_LOCK.lock();
                List<TimeSlotBean> timesSlots = appointmentDAO.
                        getBusyTimeSlotOrderTimeStart(app.getDate(), app.getWorker().getId());
                if (timeService.validateTime(userTime, timesSlots, workTime)) {
                    appointmentDAO.addAppointment(app);
                    return true;
                }
            } catch (AppointmentDaoException e) {
                LOG.warn("#addAppointment: can not add appointment");
            } catch (UserDaoException e) {
                LOG.warn("#addAppointment: invoke getWorkTimeByDate workerId["+app.getWorker().getId()+
                        " date["+app.getDate()+"]");
            } finally {
                if (REENTRANT_LOCK.isLocked()) {
                    REENTRANT_LOCK.unlock();
                }
            }
        return false;
    }

    @Override
    public void removeAppointmentFromSession(HttpServletRequest req, int id) {
        HttpSession session = req.getSession();
        Object obList = session.getAttribute("appointments");
        if (obList instanceof List) {
            List list = (List) obList;
            Appointment app = new Appointment();
            app.setId(id);
            list.remove(app);
        }
    }

    @Override
    public void changeAppointmentCanceledStatusSession(HttpServletRequest req, int id, String attr) {
        HttpSession session = req.getSession();
        Object obList = session.getAttribute(attr);
        if (obList instanceof List) {
            List list = (List) obList;
            Appointment app = new Appointment();
            app.setId(id);
            app = (Appointment) list.get(list.indexOf(app));
            app.setCanceled(true);
            session.setAttribute(attr, list);
        }
    }
}
