package com.beauty.beauty.service;

import com.beauty.beauty.db.bean.Email;
import com.beauty.beauty.db.dao.*;
import com.beauty.beauty.db.entity.Appointment;
import com.beauty.beauty.db.entity.Evaluation;
import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.db.entity.User;
import com.beauty.beauty.util.Util;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    private static final Logger LOG = Logger.getLogger(AppointmentServiceImpl.class);
    private static final String MESSAGE_NULL_TIME = "Input time";
    private static final String ATTR_USER_DATE = "user_date";
    private static final String ATTR_SERVICE_NAME = "user_service_name";
    private static final String ATTR_MASTER_ID = "master_id";
    private static final String ATTR_CURRENT_USER = "current_user";
    private static final String ATTR_APPOINTMENT_ID = "change_appointment_id";
    private static final String ATTR_USER_TIME = "user_time";
    private static final String ATTR_APPOINTMENTS = "appointments";
    private static final String ILLEGAL_ATTRIBUTE_REDIRECT = "/services_masters";
    private static final String MESSAGE_ILLEGAL_TIME = "Input correct time";
    private static final String PARAM_APPOINTMENT_ID = "appointment_id";
    private static final String MESSAGE_TEXT = "Please rate us: http://localhost:8080/rate_us?evaluation_id=";
    private static final String MESSAGE_SUBJECT = "Rate Us";
    private static final String COMPLETE_REDIRECT_PAGE = "/master_client";

    private ServletService servletService;
    private TimeService timeService;
    private UserDAO userDAO;
    private ServiceDAO serviceDAO;
    private AppointmentDAO appointmentDAO;
    private EmailService emailService;
    private PaginationService paginationService;

    @Autowired
    public AppointmentServiceImpl(ServletService servletService,
                                  TimeService timeService,
                                  UserDAO userDAO,
                                  ServiceDAO serviceDAO,
                                  AppointmentDAO appointmentDAO,
                                  EmailService emailService,
                                  PaginationService paginationService) {
        this.servletService = servletService;
        this.timeService = timeService;
        this.userDAO = userDAO;
        this.serviceDAO = serviceDAO;
        this.appointmentDAO = appointmentDAO;
        this.emailService = emailService;
        this.paginationService = paginationService;
    }

    @Override
    public void add(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            HttpSession session = req.getSession();
            User user = (User) session.getAttribute(ATTR_CURRENT_USER);
            Date date = (Date) session.getAttribute(ATTR_USER_DATE);
            Integer masterId = (Integer) session.getAttribute(ATTR_MASTER_ID);
            String userTime = req.getParameter(ATTR_USER_TIME);
            if (!isValid(resp, user, date, masterId, userTime)) {
                return;
            }
            userTime = timeService.parseTime(userTime);
            User worker = userDAO.getUser(masterId);
            if (worker.getRole() != Role.Master) {
                resp.sendRedirect(ILLEGAL_ATTRIBUTE_REDIRECT);
                return;
            }
            createAppointmentAndRedirect(resp, session, date, user, worker, userTime);
        } catch (ServiceDaoException e) {
            LOG.warn("can not get service by name", e);
            resp.sendRedirect(ILLEGAL_ATTRIBUTE_REDIRECT);
        } catch (UserDaoException e) {
            LOG.warn("can not get master by id", e);
            resp.sendRedirect(ILLEGAL_ATTRIBUTE_REDIRECT);
        } catch (IllegalArgumentException e) {
            LOG.warn("user input incorrect time", e);
            resp.sendRedirect("/choose_time?info=" + MESSAGE_ILLEGAL_TIME);
        }
    }

    @Override
    public void accept(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String appointmentId = req.getParameter("appointment_id");
        try {
            if (appointmentId != null) {
                int id = Integer.parseInt(appointmentId);
                appointmentDAO.changeAcceptStatus(id, true);
                servletService.removeAppointmentFromSession(req, id);
                req.getRequestDispatcher("/admin_client").forward(req, resp);
            }
        } catch (NumberFormatException e) {
            LOG.warn("#execute invoke Integer#parse strNum["+appointmentId+"]", e);
            resp.sendError(400);
        } catch (AppointmentDaoException e) {
            LOG.warn("#execute invoke AppointmentDAO#changeCancelStatus id["+appointmentId+"] accept[true]", e);
            resp.sendError(400);
        }
    }

    @Override
    public void cancel(HttpServletRequest req, HttpServletResponse resp, Role role, String redirect) throws IOException {
        String appointmentId = req.getParameter(PARAM_APPOINTMENT_ID);
        try {
            HttpSession session = req.getSession();
            User user = (User) session.getAttribute(ATTR_CURRENT_USER);
            if (isValidSessionAttribute(appointmentId, user)) {
                int id = Integer.parseInt(appointmentId);
                cancelAppointmentByRole(role, user, id, req);
                resp.sendRedirect(redirect);
            }
        } catch (NumberFormatException e) {
            LOG.warn("#execute invoke Integer#parse strNum[" + appointmentId + "]", e);
            resp.sendError(400);
        } catch (AppointmentDaoException e) {
            LOG.warn("#execute invoke AppointmentDAO#changeCancelStatus id[" + appointmentId + "] cancel[true]", e);
            resp.sendError(400);
        }
    }

    @Override
    public void complete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            HttpSession session = req.getSession();
            User master = (User) session.getAttribute(ATTR_CURRENT_USER);
            int appId = Integer.parseInt(req.getParameter("appointment_id"));
            appointmentDAO.changeCompletedStatus(master.getId(), appId, true);
            Appointment app = appointmentDAO.getAppointment(appId);
            Evaluation evaluation = new Evaluation();
            evaluation.setAppointment(app);
            appointmentDAO.addEvaluation(evaluation);
            sendMessage(app);
            Object obAppointments = session.getAttribute("appointments_ms");
            if (obAppointments instanceof List) {
                List<Appointment> list = (List<Appointment>) obAppointments;
                list.get(list.indexOf(app)).setCompleted(true);
                session.setAttribute("appointments_ms", list);
            }
            resp.sendRedirect(COMPLETE_REDIRECT_PAGE);
        } catch (NumberFormatException e) {
            resp.sendError(400);
            LOG.warn("#execute: Illegal appointment id["+req.getParameter("appointment_id")+"]", e);
        } catch (AppointmentDaoException e) {
            resp.sendError(400);
            LOG.warn("#execute: failed to update appointment with id["
                    +req.getParameter("appointment_id")+"] successfully", e);
        }
    }

    @Override
    public void changeTime(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            HttpSession session = req.getSession();
            Object idOb = session.getAttribute(ATTR_APPOINTMENT_ID);
            if (idOb != null && idOb.getClass() == Integer.class) {
                int id = (Integer) idOb;
                Appointment app = appointmentDAO.getAppointment(id);
                String strTime = timeService.parseTime(req.getParameter(ATTR_USER_TIME));
                Time time = Time.valueOf(strTime);
                app.setTimeStart(time);
                app.setTimeEnd(new Time(time.getTime() + app.getService().getEstimatedTime() * 60_000L));
                servletService.updateAppointment(app);
                resp.sendRedirect("/admin_client");
            } else {
                resp.sendError(400);
            }
        } catch (AppointmentDaoException e) {
            LOG.warn("#execute: can not change time", e);
            resp.sendError(400);
        }
    }

    @Override
    public void getAdminAppointment(HttpServletRequest req, HttpServletResponse resp, int pageSize) throws IOException {
        String strPageNum = req.getParameter("pageAppointment");
        try {
            int availablePage = paginationService.calcAvailablePage(
                    appointmentDAO.getNumberOfActualAppointment(), pageSize);
            HttpSession session = req.getSession();
            int page = paginationService.findPage(strPageNum, availablePage);
            List<Appointment> appointments = appointmentDAO.getActualAppointments(
                    (page - 1) * pageSize, pageSize);
            session.setAttribute(ATTR_APPOINTMENTS, appointments);
            session.setAttribute("pageAppointment", page);
            session.setAttribute("availablePage", availablePage);
            resp.sendRedirect("/admin_client");
        } catch (AppointmentDaoException e) {
            resp.sendError(400);
            LOG.warn("#execute can not get appointments", e);
        } catch (NumberFormatException e) {
            resp.sendError(400);
            LOG.warn("#execute can not get user appointments illegal user date pageAppointment["
                    +strPageNum+"]", e);
        }
    }

    @Override
    public void getAppointments(HttpServletRequest req, HttpServletResponse resp,
                                Role role, int pageSize, String attrPageAppointment,
                                String attrAppointments, String attrAvailablePage,
                                String redirectPage) throws IOException {
        String strPageNum = req.getParameter(attrPageAppointment);
        try {
            User user = (User) req.getSession().getAttribute("current_user");
            int availablePage = calcAvailablePage(user, role, pageSize);
            int page = paginationService.findPage(strPageNum, availablePage);
            List<Appointment> appointments = selectRole(page, user, role, pageSize);
            HttpSession session = req.getSession();
            session.setAttribute(attrAppointments, appointments);
            session.setAttribute(attrPageAppointment, page);
            session.setAttribute(attrAvailablePage, availablePage);
            resp.sendRedirect(redirectPage);
        } catch (AppointmentDaoException e) {
            resp.sendError(400);
            LOG.warn("#execute can not get appointments", e);
        } catch (NumberFormatException e) {
            resp.sendError(400);
            LOG.warn("#execute can not get user appointments illegal user date pageAppointment["
                    +strPageNum+"]", e);
        }
    }

    @Override
    public void evaluate() {

    }

    private int calcAvailablePage(User user, Role userRole, int pageSize) throws AppointmentDaoException {
        if (Role.Client == userRole) {
            return paginationService.calcAvailablePage(appointmentDAO.getNumberOfUserAppointment(user), pageSize);
        } else {
            return paginationService.calcAvailablePage(appointmentDAO.getNumberOfMasterAppointment(user), pageSize);
        }
    }

    private void sendMessage(Appointment app) {
        try {
            emailService.addEmail(Email.createEmail(
                    app.getUser(), MESSAGE_SUBJECT, MESSAGE_TEXT + app.getId()));
        } catch (IOException e) {
            LOG.error("#sendMessage: can not send message", e);
        }
    }

    private boolean isValidSessionAttribute(String appointmentId, User user) {
        return appointmentId != null && user != null && user.getClass() == User.class;
    }

    private List<Appointment> selectRole(int page, User user, Role role, int pageSize) throws AppointmentDaoException {
        if (Role.Client == role) {
            return appointmentDAO.getAppointments((page - 1) * pageSize, pageSize, user);
        } else {
            return appointmentDAO.getMasterAppointment((page - 1) * pageSize, pageSize, user);
        }
    }

    private void cancelAppointmentByRole(Role role, User user, int id, HttpServletRequest req) throws AppointmentDaoException {
        if (role == Role.Client) {
            appointmentDAO.changeCancelStatus(user.getId(), id, true);
            servletService.changeAppointmentCanceledStatusSession(req, id, "appointments_cl");
        } else if (role == Role.Master) {
            appointmentDAO.changeCancelStatus(id, true);
            servletService.changeAppointmentCanceledStatusSession(req, id, "appointments_ms");
        } else if (role == Role.Admin) {
            appointmentDAO.changeCancelStatus(id, true);
            servletService.removeAppointmentFromSession(req, id);
        }
    }

    private boolean isValid(HttpServletResponse resp, User user, Date date,
                            Integer masterId, String userTime) throws IOException {
        if (user == null || user.getRole() == Role.Unregistered) {
            resp.sendRedirect("/login");
            return false;
        }
        if (date == null || masterId == null) {
            resp.sendRedirect(ILLEGAL_ATTRIBUTE_REDIRECT);
            return false;
        }
        if (userTime == null) {
            resp.sendRedirect("/choose_time?info=" + MESSAGE_NULL_TIME);
            return false;
        }
        return true;
    }

    private void createAppointmentAndRedirect(HttpServletResponse resp, HttpSession session,
                                              Date date, User user, User worker, String userTime) throws ServiceDaoException, IOException {
        Time start = Time.valueOf(userTime);
        com.beauty.beauty.db.entity.Service service = serviceDAO.getService((String) session.getAttribute(ATTR_SERVICE_NAME));
        Time end = new Time(start.getTime() + service.getEstimatedTime() * 60_000L);
        Appointment app = Appointment.createAppointment(
                date, start, end, service.getPrice(), service, user, worker);
        if (servletService.addAppointment(app)) {
            Util.cleanSession(session, ATTR_USER_TIME, ATTR_MASTER_ID, ATTR_SERVICE_NAME);
            resp.sendRedirect("/success_appointment?info=Success: appointment: " + app.getId());
        } else {
            resp.sendRedirect("choose_time?info=" + MESSAGE_ILLEGAL_TIME);
        }
    }
}
