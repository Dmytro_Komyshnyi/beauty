package com.beauty.beauty.service;

import com.beauty.beauty.db.entity.Appointment;

import javax.servlet.http.HttpServletRequest;

public interface ServletService {

    public boolean updateAppointment(Appointment app);
    public boolean addAppointment(Appointment app);
    public void removeAppointmentFromSession(HttpServletRequest req, int id);
    public void changeAppointmentCanceledStatusSession(HttpServletRequest req, int id, String attr);

}
