package com.beauty.beauty.service;

import com.beauty.beauty.db.bean.Email;

import java.io.IOException;

public interface EmailService {

    public void start() throws IOException;
    public void addEmail(Email email) throws IOException;
    public void shutDown() throws IOException;
}
