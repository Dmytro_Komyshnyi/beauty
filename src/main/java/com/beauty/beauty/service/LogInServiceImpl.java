package com.beauty.beauty.service;

import com.beauty.beauty.aop.LogExecutionTime;
import com.beauty.beauty.db.dao.UserDAO;
import com.beauty.beauty.db.dao.UserDaoException;
import com.beauty.beauty.db.entity.Role;
import com.beauty.beauty.db.entity.User;
import com.beauty.beauty.util.UserUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

//TODO replace redirect
@Service
public class LogInServiceImpl implements LogInService {

    private static final Logger LOG = Logger.getLogger(LogInServiceImpl.class);
    private static final String MESSAGE_LOGIN = "Incorrect login";
    private static final String MESSAGE_PASSWORD = "Incorrect password";
    private static final String MESSAGE_PHONE = "Incorrect phone";
    private static final String MESSAGE_NAME = "Incorrect user name";
    private static final String INFO_MESSAGE = "info";
    private static final String ATTR_LOGIN = "login";
    private static final String ATTR_PASSWORD = "password";
    private static final String ATTR_FULL_NAME = "fullname";
    private static final String ATTR_PHONE = "phone";
    private static final String ATTR_USER = "current_user";
    private static final String LOGIN_PAGE = "/login";
    private UserDAO userDAO;

    @Autowired
    public LogInServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public User createUser(HttpServletRequest req, Role role) throws ServletServiceException {
        String loginParam = req.getParameter(ATTR_LOGIN);
        String passwordParam = req.getParameter(ATTR_PASSWORD);
        String nameParam = req.getParameter(ATTR_FULL_NAME);
        String phoneParam = req.getParameter(ATTR_PHONE);
        if (loginParam != null && passwordParam != null
                && nameParam != null && phoneParam != null) {
            validationParams(req, loginParam, passwordParam, phoneParam, nameParam);
            return new User(loginParam, passwordParam, role, nameParam, phoneParam);
        } else {
            req.getSession().setAttribute(INFO_MESSAGE, "Fill all fields");
            throw new ServletServiceException("didn't fill all fields");
        }
    }

    @Override
    public void logout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            User user = new User();
            user.setRole(Role.Unregistered);
            HttpSession session = req.getSession();
            session.invalidate();
            req.getSession().setAttribute(ATTR_USER, user);
            resp.sendRedirect(LOGIN_PAGE);
        } catch (IOException e) {
            LOG.error("can not redirect to " + LOGIN_PAGE + " page");
            throw e;
        }
    }

    @LogExecutionTime
    @Override
    public void authorization(HttpServletRequest req, HttpServletResponse resp,
                              String password, String login) throws IOException {
        if (password == null || login == null) {
            resp.sendRedirect("/sign_in?"+INFO_MESSAGE+"=fill all gaps");
            return;
        }
        if (UserUtil.hasValidLogin(login) && UserUtil.hasValidPassword(password)) {
            try {
                User user = userDAO.getUser(login, password);
                req.getSession().setAttribute("current_user", user);
                resp.sendRedirect("/");
            } catch (UserDaoException e) {
                LOG.warn("#execute: can not get user login["+login+"]" + " password ["+password+"]", e);
                resp.sendRedirect("/sign_in?"+INFO_MESSAGE + "=incorrect login or password");
            }
        }
    }

    private void validationParams(HttpServletRequest req, String login,
                                  String password, String phone, String name) throws ServletServiceException {
        if (!UserUtil.hasValidLogin(login)) {
            req.getSession().setAttribute(INFO_MESSAGE, MESSAGE_LOGIN);
            throw new ServletServiceException("#validationParams: " + MESSAGE_LOGIN);
        }
        if (!UserUtil.hasValidPassword(password)) {
            req.getSession().setAttribute(INFO_MESSAGE, MESSAGE_PASSWORD);
            throw new ServletServiceException("#validationParams: " + MESSAGE_PASSWORD);
        }
        if (!UserUtil.hasValidPhone(phone)) {
            req.getSession().setAttribute(INFO_MESSAGE, MESSAGE_PHONE);
            throw new ServletServiceException("#validationParams: " + MESSAGE_PHONE);
        }
        if (name.length() < 2 || name.length() > 146) {
            name = name.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
            req.getSession().setAttribute(INFO_MESSAGE, MESSAGE_NAME);
            throw new ServletServiceException("#validationParams: " + MESSAGE_NAME);
        }
    }
}
