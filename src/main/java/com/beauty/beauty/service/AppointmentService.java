package com.beauty.beauty.service;

import com.beauty.beauty.db.entity.Role;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface AppointmentService {

    void add(HttpServletRequest req, HttpServletResponse resp) throws IOException;

    void accept(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException;

    void cancel(HttpServletRequest req, HttpServletResponse resp, Role role, String redirect) throws IOException;

    void complete(HttpServletRequest req, HttpServletResponse resp) throws IOException;

    void changeTime(HttpServletRequest req, HttpServletResponse resp) throws IOException;

    void getAdminAppointment(HttpServletRequest req, HttpServletResponse resp, int pageSize) throws IOException;

    void getAppointments(HttpServletRequest req, HttpServletResponse resp,
                         Role role, int pageSize, String attrPageAppointment,
                         String attrAppointments, String attrAvailablePage,
                         String redirectPage) throws IOException;

    void evaluate();
}
