package com.beauty.beauty.service;

import com.beauty.beauty.db.bean.TimeSlotBean;
import com.beauty.beauty.db.dao.*;
import com.beauty.beauty.db.dao.mySQL.MySQLService;
import com.beauty.beauty.db.dao.mySQL.MySQLUser;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

@Service
public class TimeServiceImpl implements TimeService {

    private static final Logger LOG = Logger.getLogger(TimeServiceImpl.class);
    private static ServiceDAO SERVICE_DAO = MySQLService.getInstance();
    private static UserDAO USER_DAO = MySQLUser.getInstance();
    @Autowired
    private AppointmentDAO appointmentDAO;

    public TimeServiceImpl(AppointmentDAO appointmentDAO) {
        this.appointmentDAO = appointmentDAO;
    }

    public TimeServiceImpl() {
    }

    /**
     * Method finds free slots between master's work time start and work time end
     * and takes into account busy slots and work hours. Finds service's estimated time by service name.
     * Finds work hours and busy slots time by master's id and date.
     * Gives free master's free slots in date.
     * FreeSlot should already be added to DB.
     * @param date not null
     * @param masterId non negative number and non zero
     * @param serviceName not null and not empty
     * @param freeSlot real busy slot which can be free
     * @return free slots plus user free slot
     */
    @Override
    public List<TimeSlotBean> getFreeTimeSlots
            (Date date, int masterId, String serviceName, TimeSlotBean freeSlot) throws ServletServiceException {
        try {
            int serviceTime = SERVICE_DAO.getEstimatedTime(serviceName);
            List<TimeSlotBean> busySlots = appointmentDAO.getBusyTimeSlotOrderTimeStart(date, masterId);
            TimeSlotBean workTime = USER_DAO.getWorkTimeByDate(masterId, date);
            if (workTime == null) {
                return null;
            }
            busySlots.remove(freeSlot);
            return calcFreeSlots(workTime, busySlots, serviceTime);
        } catch (AppointmentDaoException e) {
            LOG.warn("#getFreeTimeSlot invoked getBusyTimeSlotByDateAndMaster " +
                    "date["+date+"] masterId["+masterId+"]", e);
            throw new ServletServiceException("can not get busy slots from db by master id["+masterId
                    +"]" + " date[" +date+ "]", e);
        } catch (UserDaoException e) {
            LOG.warn("#getFreeTimeSlot invoked getMasterWorkTimeByDate " +
                    "date["+date+"] masterId["+masterId+"]", e);
            throw new ServletServiceException("can not get master from db by master id[" +masterId+"]", e);
        } catch (ServiceDaoException e) {
            LOG.warn("#getFreeTimeSlot invoked ServiceDAO#getEstimatedTime " +
                    "serviceName["+serviceName+"]", e);
            throw new ServletServiceException("can not get service from db by serviceName["+serviceName+"]", e);
        }
    }

    /**
     * Method finds free slots between master's work time start and work time end
     * and takes into account busy slots and work hours. Finds service's estimated time by service name.
     * Finds work hours and busy slots time by master's id and date.
     * Gives free master's free slots in date.
     * @param date not null
     * @param masterId non negative number and non zero
     * @param serviceName not null and not empty
     * @return free slots
     */
    @Override
    public List<TimeSlotBean> getFreeTimeSlots(Date date, int masterId, String serviceName) throws ServletServiceException {
        try {
            int serviceTime = SERVICE_DAO.getEstimatedTime(serviceName);
            List<TimeSlotBean> busySlots = appointmentDAO.getBusyTimeSlotOrderTimeStart(date, masterId);
            TimeSlotBean workTime = USER_DAO.getWorkTimeByDate(masterId, date);
            if (workTime == null) {
                return null;
            }
            if (busySlots.isEmpty()) {
                workTime.setTimeEnd(
                        new Time(workTime.getTimeEnd().getTime() - (long) serviceTime * 60_000));
                busySlots.add(workTime);
                return busySlots;
            }
            return calcFreeSlots(workTime, busySlots, serviceTime);
        } catch (AppointmentDaoException e) {
            LOG.warn("#getFreeTimeSlot invoked getBusyTimeSlotByDateAndMaster " +
                    "date["+date+"] masterId["+masterId+"]", e);
            throw new ServletServiceException("can not get busy slots from db by master id["+masterId
                    +"]" + " date[" +date+ "]", e);
        } catch (UserDaoException e) {
            LOG.warn("#getFreeTimeSlot invoked getMasterWorkTimeByDate " +
                    "date["+date+"] masterId["+masterId+"]", e);
            throw new ServletServiceException("can not get master from db by master id[" +masterId+"]", e);
        } catch (ServiceDaoException e) {
            LOG.warn("#getFreeTimeSlot invoked ServiceDAO#getEstimatedTime serviceName["+serviceName+"]", e);
            throw new ServletServiceException("can not get service from db by serviceName["+serviceName+"]", e);
        }
    }

    /**
     * Method finds free slots between work time start and work time end and takes into account busy slots.
     * Each free slot can be assigned to service with this estimated time.
     * @param workTime not null
     * @param busySlots {@link List<TimeSlotBean>} not null and can be empty
     * @param serviceTime non negative number, estimated time of service in minutes
     * @return free slots
     */
    private List<TimeSlotBean> calcFreeSlots(TimeSlotBean workTime, List<TimeSlotBean> busySlots, int serviceTime) {
        serviceTime = serviceTime * 60_000;
        TimeSlotBean lastSlot = new TimeSlotBean();
        lastSlot.setTimeStart(workTime.getTimeEnd());
        busySlots.add(lastSlot);
        List<TimeSlotBean> freeSlots = new ArrayList<>();
        if ((busySlots.get(0).getTimeStart().getTime() - workTime.getTimeStart().getTime()) >= serviceTime) {
            TimeSlotBean firstSlot = new TimeSlotBean();
            firstSlot.setTimeStart(new Time(workTime.getTimeStart().getTime()));
            firstSlot.setTimeEnd(new Time(busySlots.get(0).getTimeStart().getTime() - serviceTime));
            freeSlots.add(firstSlot);
        }
        for (int i = 1; i < busySlots.size(); i++) {
            TimeSlotBean currentSlot = busySlots.get(i);
            TimeSlotBean previousSlot = busySlots.get(i - 1);
            if (currentSlot.getTimeStart().getTime() - previousSlot.getTimeEnd().getTime() >= serviceTime) {
                TimeSlotBean freeSlot = new TimeSlotBean();
                freeSlot.setTimeStart(previousSlot.getTimeEnd());
                freeSlot.setTimeEnd(new Time(currentSlot.getTimeStart().getTime() - serviceTime));
                freeSlots.add(freeSlot);
            }
        }
        return freeSlots;
    }


    /**
     * @param strTime should contains time format [h]h:[h]m[:ss]
     */
    @Override
    public String parseTime(String strTime) {
        if (strTime.matches("([\\d]{1,2}:[\\d]{2}:[\\d]{2})")) {
            return strTime;
        }
        return strTime + ":00";
    }

    /**
     * Validate userTime. UserTime cannot occupy a slot in whole or
     * in part or be more or less then workTime
     * @param userTime time slot for validation
     * @param busySlots slots that cannot be occupy
     * @param workTime work hours
     * @return result of validation
     */
    @Override
    public boolean validateTime(TimeSlotBean userTime, List<TimeSlotBean> busySlots, TimeSlotBean workTime) {
        if (userTime.getTimeStart().getTime() > userTime.getTimeEnd().getTime()
                || (userTime.getTimeStart().getTime() < workTime.getTimeStart().getTime()
                || userTime.getTimeEnd().getTime() > workTime.getTimeEnd().getTime())) {
            return false;
        }
        for (TimeSlotBean slot : busySlots) {
            if ((userTime.getTimeStart().getTime() > slot.getTimeStart().getTime()
                    && userTime.getTimeStart().getTime() < slot.getTimeEnd().getTime())
                    || ((userTime.getTimeEnd().getTime() > slot.getTimeStart().getTime()
                    && userTime.getTimeEnd().getTime() < slot.getTimeEnd().getTime()))) {
                return false;
            }
        }
        return true;
    }
}
