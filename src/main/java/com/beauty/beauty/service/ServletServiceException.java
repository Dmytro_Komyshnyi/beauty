package com.beauty.beauty.service;

public class ServletServiceException extends Exception {

    public ServletServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServletServiceException(String message) {
        super(message);
    }
}
