package com.beauty.beauty.service;

import com.beauty.beauty.db.bean.Email;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Service
public class EmailServiceImpl implements EmailService {

    private static final Logger LOG = Logger.getLogger(EmailServiceImpl.class.getName());
    private static final String PROP_PATH = "email.properties";
    private static final String PROP_EMAIL = "mail.smtps.user";
    private static final String PROP_TIME = "mail.time.start";
    private static final String PROP_DELAY = "mail.delay";
    private static final String PROP_PASSWORD = "mail.smtps.password";
    private String email = "mail.smtps.password";
    private String password = "mail.smtps.password";
    private Session mailSession;
    private final List<Email> emails = new ArrayList<>();
    private ScheduledExecutorService executor;
    private Time startTime;
    private long delay;

    public EmailServiceImpl() throws IOException {
        buildExecutor();
        start();
    }

    /**
     * Create ScheduledExecutorService and read properties from properties file.
     * mail.core.pool.size number of threads
     * mail.delay send email with delay
     * mail.time.start time first start
     * mail.smtps.password
     * mail.smtps.user
     * @throws IOException when can not read properties file [email.properties]
     */
    private void buildExecutor() throws IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try (InputStream input = classLoader.getResourceAsStream(PROP_PATH)) {
            Properties prop = new Properties();
            prop.load(input);
            startTime = Time.valueOf(prop.getProperty(PROP_TIME));
            delay = Long.parseLong(prop.getProperty(PROP_DELAY));
            password = prop.getProperty(PROP_PASSWORD);
            email = prop.getProperty(PROP_EMAIL);
            mailSession = Session.getDefaultInstance(prop);
            executor = new ScheduledThreadPoolExecutor(1);
        } catch (IOException e) {
            LOG.error("#ctor: can not read properties file["+PROP_PATH+"]", e);
            throw e;
        }

    }

    /**
     * @param email this email will be send next time
     */
    @Override
    public void addEmail(Email email) {
        emails.add(email);
    }

    /**
     * Start EmailService
     */
    public void start() {
        String strTime = new SimpleDateFormat("HH:mm:ss").format(new Date());
        Time nowTime = Time.valueOf(strTime);
        long initialDelay;
        if (startTime.getTime() < nowTime.getTime()) {
            initialDelay = 24*60*60_000 - nowTime.getTime() + startTime.getTime();
        } else {
            initialDelay = startTime.getTime() - nowTime.getTime();
        }
        executor.scheduleWithFixedDelay(this::sendEmail, initialDelay, delay, TimeUnit.MILLISECONDS);
    }

    /**
     * Builds messages from list of emails which was added by method add Email
     * and then sends them
     */
    private void sendEmail() {
        if (!emails.isEmpty()) {
            try(Transport tp = mailSession.getTransport()) {
                List<MimeMessage> messages = buildMessage();
                tp.connect(null, password);
                for (MimeMessage msg : messages) {
                    tp.sendMessage(msg, msg.getAllRecipients());
                }
            } catch (MessagingException e) {
                e.printStackTrace();
                LOG.error("#sendEmail: incorrect from email["+email+"]");
            } catch (Exception e) {
                LOG.error("#sendEmail: smtp server denies access", e);
            }
        }
    }

    /**
     * build message for sending
     */
    private List<MimeMessage> buildMessage() {
        Date now = new Date();
        List<MimeMessage> messages = new ArrayList<>();
        List<Email> removed = new ArrayList<>();
        for (Email email : emails) {
            if (email.getDate().before(now)) {
                MimeMessage msg = new MimeMessage(mailSession);
                try {
                    msg.setFrom(new InternetAddress(this.email));
                    msg.setSubject(email.getSubject());
                    msg.setText(email.getText());
                    msg.addRecipient(Message.RecipientType.TO, new InternetAddress(email.getEmail()));
                    messages.add(msg);
                } catch (MessagingException e) {
                    LOG.warn("can not set message for email["+email.getEmail()+"]");
                }
                removed.add(email);
            }
        }
        for (Email email : removed) {
            emails.remove(email);
        }
        return messages;
    }

    /**
     * shutdown service and create new instance
     */
    public void rebuild() throws IOException {
        shutDown();
        buildExecutor();
    }

    /**
     * stop executing
     */
    @PreDestroy
    public void shutDown() {
        if (!executor.isShutdown()) {
            executor.shutdown();
        }
    }

    /**
     * Returns true if this service has been shut down.
     * @return true if this service has been shut down
     */
    public boolean isShutdown() {
        return executor.isShutdown();
    }
}
