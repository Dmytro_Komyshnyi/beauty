<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create Master</title>
</head>
<body>
<%@ include file="/header.jspf" %>
<div class="container">
    <div class="mt-5 py-5 text-white">
            <div>
                <header>
                    <h1 class="user__title">New Master</h1>
                </header>
                <form action="/admin/register_master" method="post" class="form">
                    <div class="form__group">
                        <input name = 'login'  type = 'email' placeholder="email" class="form__input"/>
                    </div>
                    <div class="form__group">
                        <input name = 'password'  type = 'password' placeholder="password" class="form__input"/>
                    </div>
                    <div class="form__group">
                        <input name = 'fullname'  type = 'text' placeholder="fullname" class="form__input"/>
                    </div>
                    <div class="form__group">
                        <input name = 'phone' type = 'text' placeholder="phone" class="form__input"/>
                    </div>
                    <input class="btn" type="submit" value="Register master">
                </form>
            </div>
            <div>
                <c:if test="${info != null}">
                    <c:out value="${info}"/>
                </c:if>
            </div>
    </div>
</div>
</body>
</html>
