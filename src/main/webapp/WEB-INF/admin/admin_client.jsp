<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="m" uri="/WEB-INF/tld/moneyFormat.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="select_service" uri="/WEB-INF/tld/moneyFormat.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/table_style.css">
    <title>Admin client</title>
</head>
<body>
<%@ include file="/header.jspf" %>
<div class="container">
    <div class="mt-5 py-5">
        <h1>Admin client</h1>
        <c:if test="${pageAppointment == null}">
            <c:set var = "pageAppointment" scope = "session" value = "${0}"/>
        </c:if>
        <div class="table-wrapper">
        <p>Appointments</p>
        <table class="fl-table">
            <thead>
            <tr>
                <td>Accept</td>
                <td>Cancel</td>
                <td>Change time</td>
                <td><fmt:message key="client.id"/></td>
                <td><fmt:message key="client.client.name"/></td>
                <td><fmt:message key="client.client.phone"/></td>
                <td><fmt:message key="client.client.email"/></td>
                <td><fmt:message key="client.worker.name"/></td>
                <td><fmt:message key="client.worker.phone"/></td>
                <td><fmt:message key="client.service"/></td>
                <td><fmt:message key="client.date"/></td>
                <td><fmt:message key="client.time.start"/></td>
                <td><fmt:message key="client.time.end"/></td>
                <td><fmt:message key="client.price"/></td>
            </tr>
            </thead>
            <tbody>
                <c:if test="${appointments != null}">
                <c:forEach var="w" items="${appointments}">
                    <tr>
                        <c:choose>
                            <c:when test="${w.canceled || w.completed}">
                                <td>
                                    <button type="button" disabled>accept</button>
                                </td>
                                <td>
                                    <button type="button" disabled>cancel</button>
                                </td>
                                <td>
                                    <button type="button" disabled>change</button>
                                </td>
                            </c:when>
                            <c:otherwise>
                                <td>
                                    <form action="/admin/accept_appointment" method="post">
                                        <input type="hidden" name="appointment_id" value="${w.id}">
                                        <input type="submit" value="accept">
                                    </form>
                                </td>
                                <td>
                                    <form action="/admin/cancel_appointment" method="post">
                                        <input type="hidden" name="appointment_id" value="${w.id}">
                                        <input type="submit" value="cancel">
                                    </form>
                                </td>
                                <td>
                                    <form action="/admin/free_slots" method="post">
                                        <input type="hidden" name="appointment_id" value="${w.id}">
                                        <input type="submit" value="change">
                                    </form>
                                </td>
                            </c:otherwise>
                        </c:choose>
                        <td><c:out value="${w.id}"/></td>
                        <td><c:out value="${w.user.name}"/></td>
                        <td><c:out value="${w.user.phone}"/></td>
                        <td><c:out value="${w.user.login}"/></td>
                        <td><c:out value="${w.worker.name}"/></td>
                        <td><c:out value="${w.worker.phone}"/></td>
                        <td><c:out value="${w.service.name}"/></td>
                        <td><c:out value="${w.date}"/></td>
                        <td><c:out value="${w.timeStart}"/></td>
                        <td><c:out value="${w.timeEnd}"/></td>
                        <td><m:money value="${w.price}"/></td>
                    </tr>
                </c:forEach>
            </tbody>
            </table>
        </div>
        </c:if>
        <div class="pagination">
            <form action="/admin/appointment" method="get">
                <input type="hidden" name = "pageAppointment" value="${pageAppointment + 1}"/>
                <input type="submit" value="next">
            </form>
                <c:choose>
                    <c:when test="${availablePage != null || availablePage > 0}">
                        <c:forEach var="i" begin="1" end="${availablePage}">
                            <c:choose>
                                <c:when test="${pageAppointment == i}">
                                    <a href="/admin/appointment?pageAppointment=${i}"/>(<c:out value="${i}"/>)</a>
                                </c:when>
                                <c:otherwise>
                                    <a href="/admin/appointment?pageAppointment=${i}"/><c:out value="${i}"/></a>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <a href="/admin/appointment?pageAppointment=1"/>(1)</a>
                    </c:otherwise>
                </c:choose>
            <form action="/admin/appointment" method="get">
                <input type="hidden" name = "pageAppointment" value="${pageAppointment - 1}"/>
                <input type="submit" value="previous">
            </form>
        </div>
    </div>
</div>
</body>
</html>
