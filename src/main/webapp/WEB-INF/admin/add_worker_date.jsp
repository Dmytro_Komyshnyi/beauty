<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/table_style.css">
    <title>Schedule</title>
</head>
<body>
<%@ include file="/header.jspf" %>
<div class="container">
    <div class="mt-5 py-5">
        <div class="table-wrapper">
            <p>Appointments</p>
            <table class="fl-table">
                <thead>
                <tr>
                    <td><fmt:message key="master.date"/>Date</td>
                    <td><fmt:message key="master.start"/>Date</td>
                    <td><fmt:message key="master.end"/>Date</td>
                </tr>
                </thead>
                <c:if test="${schedule != null}">
                    <tbody>
                    <c:forEach var="day" items="${schedule}">
                        <tr>
                            <td>
                                <c:out value="${day.date}"/>
                            </td>
                            <td>
                                <c:out value="${day.workHours.timeStart}"/>
                            </td>
                            <td>
                                <c:out value="${day.workHours.timeEnd}"/>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </c:if>
            </table>
            <jsp:useBean id="date" class="com.beauty.beauty.db.bean.AvailableOrderDate"/>
            <form action="/admin/add_worker_date/" method="post">
                <input type="date" value="${date.now}" name="user_date" min="${date.now}" max="${date.max}">
                <input type="submit" value="Add new">
            </form>
        </div>
    </div>
</div>
</body>
</html>
