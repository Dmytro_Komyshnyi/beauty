<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/table_style.css">
    <title>Workers</title>
</head>
<body>
<%@ include file="/header.jspf" %>
<div class="container">
    <div class="mt-5 py-5">
        <h1><fmt:message key="admin_worekers.workers"/></h1>
        <c:if test="${pageWorkers == null}">
            <c:set var = "pageWorkers" scope = "session" value = "${0}"/>
        </c:if>
        <div class="table-wrapper">
            <p>Appointments</p>
            <table class="fl-table">
                <thead>
                <tr>
                    <td>Add work date</td>
                    <td>id</td>
                    <td>name</td>
                    <td>login</td>
                    <td>phone</td>
                </tr>
                </thead>
                <tbody>
                <c:if test="${workers != null}">
                <c:forEach var="w" items="${workers}">
                    <tr>
                        <td>
                            <form action="/admin/workers_schedule" method="post">
                                <input type="hidden" name="worker_id" value="${w.id}">
                                <input type="submit" value="Show more">
                            </form>
                        </td>
                        <td><c:out value="${w.id}"/></td>
                        <td><c:out value="${w.name}"/></td>
                        <td><c:out value="${w.login}"/></td>
                        <td><c:out value="${w.phone}"/></td>
                    </tr>
                </c:forEach>
                </c:if>
                </tbody>
            </table>
        </div>
        <form action="/admin/workers" method="get">
            <input type="submit" value="Reload">
        </form>
    </div>
</div>
</body>
</html>
