<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/table_style.css">
    <title>${current_user.name} schedule</title>
</head>
<body>
<%@ include file="/header.jspf" %>
<div class="container">
    <div class="mt-5 py-5 text-white">
        <div class="table-wrapper">
            <p>Appointments</p>
            <table class="fl-table">
                <thead>
                <tr>
                    <td><fmt:message key="master.date"/>Date</td>
                    <td><fmt:message key="master.start"/>Date</td>
                    <td><fmt:message key="master.end"/>Date</td>
                </tr>
                </thead>
                <c:if test="${schedule != null}">
                    <tbody>
                        <c:forEach var="day" items="${schedule}">
                            <tr>
                                <td>
                                    <c:out value="${day.date}"/>
                                </td>
                                <td>
                                    <c:out value="${day.workHours.timeStart}"/>
                                </td>
                                <td>
                                    <c:out value="${day.workHours.timeEnd}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>
        </div>
    </div>
</div>
</body>
</html>
