<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="m" uri="/WEB-INF/tld/moneyFormat.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/table_style.css">
    <title>Master client ${current_user.name}</title>
</head>
<body>
<%@ include file="/header.jspf"%>
<div class="container">
    <div class="mt-5 py-5">
        <h1>Master ${current_user.name}</h1>
        <button onclick="location.href = '/master/schedule';">Schedule</button>
        <c:if test="${pageAppointment_ms == null}">
            <c:set var = "pageAppointment_ms" scope = "session" value = "${0}"/>
        </c:if>
        <div class="table-wrapper">
            <p>Appointments</p>
            <table class="fl-table">
                <thead>
                <tr>
                    <td>Complete</td>
                    <td>Cancel</td>
                    <td><fmt:message key="client.id"/></td>
                    <td><fmt:message key="client.client.name"/></td>
                    <td><fmt:message key="client.service"/></td>
                    <td><fmt:message key="client.date"/></td>
                    <td><fmt:message key="client.time.start"/></td>
                    <td><fmt:message key="client.time.end"/></td>
                    <td><fmt:message key="client.price"/></td>
                    <td><fmt:message key="client.canceled"/></td>
                    <td><fmt:message key="client.completed"/></td>
                </tr>
                </thead>
                <tbody>
                <c:if test="${appointments_ms != null}">
                <c:forEach var="w" items="${appointments_ms}">
                    <tr>
                            <c:choose>
                                <c:when test="${w.canceled || w.completed}">
                                    <td>
                                        <button type="button" disabled>complete</button>
                                    </td>
                                    <td>
                                        <button type="button" disabled>cancel</button>
                                    </td>
                                </c:when>
                                <c:otherwise>
                                    <td>
                                        <form action="/master/complete_appointment" method="post">
                                            <input type="hidden" name="appointment_id" value="${w.id}">
                                            <input type="submit" value="complete">
                                        </form>
                                    </td>
                                    <td>
                                        <form action="/master/cancel_appointment" method="post">
                                            <input type="hidden" name="req_command" value="req_cancel_appointment_ms">
                                            <input type="hidden" name="appointment_id" value="${w.id}">
                                            <input type="submit" value="cancel">
                                        </form>
                                    </td>
                                </c:otherwise>
                            </c:choose>
                        <td><c:out value="${w.id}"/></td>
                        <td><c:out value="${w.user.name}"/></td>
                        <td><c:out value="${w.service.name}"/></td>
                        <td><c:out value="${w.date}"/></td>
                        <td><c:out value="${w.timeStart}"/></td>
                        <td><c:out value="${w.timeEnd}"/></td>
                        <td><m:money value="${w.price}"/></td>
                        <td><c:out value="${w.canceled}"/></td>
                        <td><c:out value="${w.completed}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        </c:if>
        <div>
            <form action="/master/appointments" method="get">
                <input type="hidden" name = "pageAppointment_ms" value="${pageAppointment_ms + 1}"/>
                <input type="submit" value="next">
            </form>
            <form action="/master/appointments" method="get">
                <input type="hidden" name = "pageAppointment_ms" value="${pageAppointment_ms - 1}"/>
                <input type="submit" value="previous">
            </form>
        </div>
    </div>
</div>
</body>
</html>