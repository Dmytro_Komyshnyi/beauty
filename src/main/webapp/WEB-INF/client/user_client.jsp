<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="m" uri="/WEB-INF/tld/moneyFormat.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/table_style.css">
    <title>${current_user.name}</title>
</head>
<body>
<%@ include file="/header.jspf" %>
<div class="container">
    <div class="mt-5 py-5 text-white">
        <h1>Client</h1>
        <c:if test="${pageAppointment_cl == null}">
            <c:set var = "pageAppointment_cl" scope = "session" value = "${0}"/>
        </c:if>
        <div class="table-wrapper">
            <p>Appointments</p>
            <table class="fl-table">
                <thead>
                <tr>
                    <td><fmt:message key="client.cancel"/></td>
                    <td><fmt:message key="client.id"/></td>
                    <td><fmt:message key="client.worker.name"/></td>
                    <td><fmt:message key="client.service"/></td>
                    <td><fmt:message key="client.date"/></td>
                    <td><fmt:message key="client.time.start"/></td>
                    <td><fmt:message key="client.time.end"/></td>
                    <td><fmt:message key="client.price"/></td>
                    <td><fmt:message key="client.canceled"/></td>
                    <td><fmt:message key="client.completed"/></td>
                </tr>
                </thead>
                <tbody>
                <c:if test="${appointments_cl != null}">
                <c:forEach var="w" items="${appointments_cl}">
                    <tr>
                        <td>
                            <c:choose>
                                <c:when test="${!w.canceled && !w.completed}">
                                    <form action="/client/cancel_appointment" method="post">
                                        <input type="hidden" name="appointment_id" value="${w.id}">
                                        <input type="submit" value="cancel">
                                    </form>
                                </c:when>
                                <c:otherwise>
                                    <button type="button" disabled>cancel</button>
                                </c:otherwise>
                           </c:choose>
                        </td>
                        <td><c:out value="${w.id}"/></td>
                        <td><c:out value="${w.worker.name}"/></td>
                        <td><c:out value="${w.service.name}"/></td>
                        <td><c:out value="${w.date}"/></td>
                        <td><c:out value="${w.timeStart}"/></td>
                        <td><c:out value="${w.timeEnd}"/></td>
                        <td><m:money value="${w.price}"/></td>
                        <td><c:out value="${w.canceled}"/></td>
                        <td><c:out value="${w.completed}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        </c:if>
        <div class="pagination">
            <form action="/client/appointments" method="get">
                <input type="hidden" name = "pageAppointment_cl" value="${pageAppointment_cl + 1}"/>
                <input type="submit" value="next">
            </form>
            <c:choose>
                <c:when test="${availablePage_cl != null || availablePage_cl > 0}">
                    <c:forEach var="i" begin="1" end="${availablePage_cl}">
                        <c:choose>
                            <c:when test="${pageAppointment_cl == i}">
                                <a href="/beauty_war_exploded/controller?req_command=get_appointments_cl&pageAppointment_cl=${i}"/>(<c:out value="${i}"/>)</a>
                            </c:when>
                            <c:otherwise>
                                <a href="/beauty_war_exploded/controller?req_command=get_appointments_cl&pageAppointment=${i}"/><c:out value="${i}"/></a>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <a href="/beauty_war_exploded/controller?req_command=get_appointments_cl&pageAppointment=1"/>(1)</a>
                </c:otherwise>
            </c:choose>
            <form action="/client/appointments" method="get">
                <input type="hidden" name = "pageAppointment_cl" value="${pageAppointment_cl - 1}"/>
                <input type="submit" value="previous">
            </form>
        </div>
    </div>
</div>
</body>
</html>
