<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Rate us</title>
</head>
<body>
<%@ include file="/header.jspf" %>
<div class="container">
    <div class="mt-5 py-5 text-white">
            <c:if test="${param.evaluation_id != null}">
                <form action="/client/evaluate" method="post">
                    <input name="mark" type="radio" value="1">1</br>
                    <input name="mark" type="radio" value="2">2</br>
                    <input name="mark" type="radio" value="3">3</br>
                    <input name="mark" type="radio" value="4">4</br>
                    <input name="mark" type="radio" value="5">5</br>
                    <input name="mark" type="radio" value="6">6</br>
                    <input name="mark" type="radio" value="7">7</br>
                    <input name="mark" type="radio" value="8">8</br>
                    <input name="mark" type="radio" value="9">9</br>
                    <input name="mark" type="radio" value="10">10</br>
                    <input value="rate" type="submit">
                    <input type="submit" value="rate">
                </form>
            </c:if>
    </div>
</div>
</body>
</html>
