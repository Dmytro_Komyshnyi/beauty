<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        .captcha_center {
            text-align: center;
        }
        .g-recaptcha {
            display: inline-block;
        }
    </style>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/login.css">
    <title>Sign in</title>
</head>
<body>
<fmt:message key="login.login" var="login"/>
<fmt:message key="login.password" var="password"/>
<fmt:message key="login.sign.in.submit" var="submit"/>
<div class="user">
    <header class="user__header">
        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3219/logo.svg"/>
        <h1 class="user__title"><fmt:message key="login.sign.in"/></h1>
    </header>
    <form action="${pageContext.request.contextPath}/login/sign_in" method="post" class="form">
<%--        <input type="hidden" name = "req_command" value="login"/>--%>
        <div class="form__group">
            <input type="email" name = "login" placeholder="${login}" class="form__input"/>
        </div>
        <div class="form__group">
            <input name = 'password'  type = 'password' placeholder="${password}" class="form__input"/>
        </div>
        <input class="btn" type="submit" value="${submit}">
        <div class="captcha_center">
            <div class="g-recaptcha"
                 data-sitekey="6LeQTW4aAAAAABof7U_4ECv1zBK35hyz8gfTU5e1"
                 style="padding-top: 10px;">
            </div>
        </div>
    </form>
    <c:if test="${param.info != null}">
        <div>
            <c:out value="${param.info}"/>
        </div>
    </c:if>
</div>
</body>
</html>
