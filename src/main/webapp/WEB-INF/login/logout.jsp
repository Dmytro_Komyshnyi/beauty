<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/login.css">
    <title>Logout</title>
</head>
<body>
    <fmt:message key="login.logout" var="submit"/>
<div class="user">
    <header class="user__header">
        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3219/logo.svg"/>
        <h1 class="user__title"><c:out value="${submit}"/></h1>
    </header>
    <form action="/login/logout" method="get" class="form">
        <input type="hidden" name="logout" value="true">
        <input class="btn" type="submit" value="${submit}">
    </form>
    <c:if test="${param.info != null}">
        <div>
            <c:out value="${param.info}"/>
        </div>
    </c:if>
</div>
</body>
</html>
