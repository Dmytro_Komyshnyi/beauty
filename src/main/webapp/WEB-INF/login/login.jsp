<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/login.css">
    <script src='https://www.google.com/recaptcha/api.js' type="text/javascript">
        document.getElementById("btn_has_account").onclick = ()=> {
            location.href = "/sign_in";
        };
    </script>
    <title>Sign in</title>
    <style>
        .captcha_center {
            text-align: center;
        }
        .g-recaptcha {
            display: inline-block;
        }
    </style>
</head>
<body>
    <fmt:message key="login.login" var="login"/>
    <fmt:message key="login.password" var="password"/>
    <fmt:message key="login.name" var="name"/>
    <fmt:message key="login.phone" var="phone"/>
    <fmt:message key="login.submit" var="submit"/>
    <fmt:message key="login.has.account" var="has_account"/>
    <div class="user">
        <header class="user__header">
            <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3219/logo.svg"/>
            <h1 class="user__title"><fmt:message key="login.registration"/></h1>
        </header>
        <form action="/login/registration" method="post" class="form">
<%--            <input name = 'req_command' value = 'registration' type = 'hidden'/>--%>
            <div class="form__group">
                <input name = 'login'  type = 'email' placeholder="${login}" class="form__input"/>
            </div>
            <div class="form__group">
                <input name = 'password'  type = 'password' placeholder="${password}" class="form__input"/>
            </div>
            <div class="form__group">
                <input name = 'fullname'  type = 'text' placeholder="${name}" class="form__input"/>
            </div>
            <div class="form__group">
                <input name = 'phone' type = 'text' placeholder="${phone}" class="form__input"/>
            </div>
            <input class="btn" type="submit" value="${submit}">
            <div class="captcha_center">
                <div class="g-recaptcha"
                     data-sitekey="6LeQTW4aAAAAABof7U_4ECv1zBK35hyz8gfTU5e1"
                     style="padding-top: 10px;">
                </div>
            </div>
        </form>
        <form class = form>
            <button id="btn_has_account" class="btn" onclick="redirectToSignIn()">${has_account}</button>
        </form>
        <c:if test="${info != null}">
            <div>
                <c:out value="${info}"/>
            </div>
        </c:if>
    </div>
</body>
</html>
