<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@600;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/error_style.css">
</head>
<body>
<div class="mainbox">
    <div class="err">400</div>
    <div class="msg">Bad request<p>Let's go <a href="/">home</a> and try from there.</p></div>
</div>
</body>
</html>