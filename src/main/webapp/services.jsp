<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="m" uri="/WEB-INF/tld/moneyFormat.tld" %>
<%@ taglib prefix="pm" uri="/WEB-INF/tld/printMark.tld" %>
<%@ taglib prefix="sw" uri="/WEB-INF/tld/sortWorker.tld" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/table_style.css">
    <title>Services</title>
</head>
<%@ include file="header.jspf" %>
<body>
<div class = "container">
    <div class="mt-5 py-5">
        <form action="/services_masters" method="get">
            <input type="submit" value="Reload"/>
        </form>
        <c:if test="${services != null || masters != null}">
            <c:if test="${param.ask == null}">
                <c:set value="${true}" var="ask" scope="request"/>
            </c:if>
            <sw:sortWorker workerList="${masters}" attrName="sortMasters" typeSort="${param.sort}" ask="${param.ask}"/>
            <form action="services" method="get">
                <select name="sort">
                    <option value="name">Alphabet</option>
                    <option value="mark">Mark</option>
                </select>
                <select name="ask">
                    <option value="true">ASK</option>
                    <option value="false">DECS</option>
                </select>
                <input type="submit" value="sort"/>
            </form>
            <form action="/time_slots" method="post">
                <div class="table-wrapper">
                <p>Masters</p>
                    <table class="fl-table">
                        <thead>
                        <tr>
                            <td><fmt:message key="services.table.choose"/></td>
                            <td><fmt:message key="services.table.name"/></td>
                            <td><fmt:message key="services.table.mark"/></td>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="master" items="${sortMasters}">
                            <tr>
                                <td><input type="radio" name="master_id" value="${master.id}"/></td>
                                <td>${master.name}</td>
                                <td><pm:printMark mark="${master.mark}"/></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="table-wrapper">
                    <p>Services</p>
                    <table class="fl-table">
                        <thead>
                        <tr>
                            <td>Choose one</td>
                            <td>Name</td>
                            <td>Time</td>
                            <td>Price</td>
                        </tr>
                        </thead>
                        <c:forEach var="service" items="${services}">
                            <tr>
                                <td><input type="radio" name="service_name" value="${service.name}"/></td>
                                <td>${service.name}</td>
                                <td>${service.estimatedTime}min</td>
                                <td><m:money value="${service.price}"/></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <jsp:useBean id="date" class="com.beauty.beauty.db.bean.AvailableOrderDate" />
                <input type="date" value="${date.now}" name="user_date" min="${date.now}" max="${date.max}">
                <input type="submit" value="Choose"/>
            </form>
        </c:if>
        <c:if test="${param.info != null}">
            <div class = "info">
                <c:out value="${param.info}"/>
            </div>
        </c:if>
    </div>
</div>
</body>
</html>
