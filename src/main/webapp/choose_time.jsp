<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <link rel="stylesheet" href="/css/style/table_style.css">
    <title>Choose time</title>
</head>
<body>
<%@ include file="/header.jspf" %>
<div class="container">
    <div class="mt-5 py-5">
        <h1>Choose Time</h1>
            <div class="table-wrapper">
            <p>Time Slots</p>
            <table class="fl-table">
                <thead>
                    <tr>
                        <td>Choose one</td>
                        <td>from</td>
                        <td>to</td>
                    </tr>
                </thead>
                <tbody>
                <c:if test="${time_slots != null}">
                    <c:forEach var="slot" items="${time_slots}">
                        <tr>
                            <td>
                                <form method="post" action="/client/add_appointment">
                                    <input required type="time" name="user_time" value="${slot.timeStart}" min="${slot.timeStart}" max="${slot.timeEnd}">
                                    <input type="submit" value="Add appointment">
                                </form>
                            </td>
                            <td><c:out value="${slot.timeStart}"/></td>
                            <td><c:out value="${slot.timeEnd}"/></td>
                        </tr>
                    </c:forEach>
                </c:if>
                </tbody>
            </table>
            </div>
        <c:if test="${param.info != null}">
            <div class = "info">
                <c:out value="${param.info}"/>
            </div>
        </c:if>
    </div>
</div>
</body>
</html>
