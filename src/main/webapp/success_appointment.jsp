<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>Success</title>
</head>
<body>
<%@include file="/header.jspf" %>
<div class="container">
    <div class="mt-5 py-5 text-white">
        <c:if test="${param.info != null}">
            <c:out value="${param.info}"/>
        </c:if>
    </div>
</div>
</body>
</html>
