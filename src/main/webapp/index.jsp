<%@ taglib prefix="m" uri="/WEB-INF/tld/moneyFormat.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Beauty</title>
</head>
<body>
<%@ include file="header.jspf" %>
<div class="container">
   <div class="mt-5 py-5 text-white">
       <c:choose>
           <c:when test="${locale != null && locale == 'ru'}">
               <h1 class="display-4"><fmt:message key="salon.name"/></h1>
               Мы лучшие в мире !!!
           </c:when>
           <c:otherwise>
               <h1 class="display-4"><fmt:message key="salon.name"/></h1>
               We are the best in the whole world !!!
           </c:otherwise>
       </c:choose>
   </div>
</div>
</body>
</html>